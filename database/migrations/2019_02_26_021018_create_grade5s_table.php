<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGrade5sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grade5s', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('lagro_student_id')->unsigned();
          $table->string('subject_code')->nullable();
          $table->string('subject_name')->nullable();
          $table->string('grade')->nullable();
          $table->string('remark')->nullable();
          $table->string('instructor')->nullable();
          $table->foreign('lagro_student_id')->references('id')->on('lagro_students');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grade5s');
    }
}
