<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLagroStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lagro_students', function (Blueprint $table) {
          // Student's Info
          $table->increments('id');
          $table->string('firstname');
          $table->string('lastname');
          $table->string('middlename');
          $table->string('studentid')->unique();
          $table->string('email')->unique();
          $table->timestamp('email_verified_at')->nullable();
          $table->string('password');
          $table->string('course')->nullable();
          $table->string('major')->nullable();
          $table->string('student_status')->nullable();
          $table->string('section')->nullable();
          $table->string('gender')->nullable();
          $table->string('status')->nullable();
          $table->string('citizenship')->nullable();
          $table->string('birthplace')->nullable();
          $table->string('religion')->nullable();
          $table->string('date_of_birth')->nullable();

          // Address
          $table->string('street_number')->nullable();
          $table->string('street')->nullable();
          $table->string('subdivision_village_building')->nullable();
          $table->string('barangay')->nullable();
          $table->string('city')->nullable();
          $table->string('province')->nullable();
          $table->string('zip_code')->nullable();

          // Contact details
          $table->string('telephone')->nullable();
          $table->string('mobile')->nullable();

          // Current or Last School Attended
          $table->string('school_type')->nullable();
          $table->string('school_name')->nullable();
          $table->string('grad_date')->nullable();
          $table->string('school_year')->nullable();
          $table->string('year_grade')->nullable();
          $table->string('term')->nullable();

          // Parents / Guardian's Information
          // Father's Info
          $table->string('f_firstname')->nullable();
          $table->string('f_lastname')->nullable();
          $table->string('f_initial')->nullable();
          $table->string('f_suffix')->nullable();
          $table->string('f_mobile')->nullable();
          $table->string('f_email')->nullable();
          $table->string('f_occupation')->nullable();
          // Mother's Info
          $table->string('m_firstname')->nullable();
          $table->string('m_lastname')->nullable();
          $table->string('m_initial')->nullable();
          $table->string('m_suffix')->nullable();
          $table->string('m_mobile')->nullable();
          $table->string('m_email')->nullable();
          $table->string('m_occupation')->nullable();
          // Guardian's Info
          $table->string('g_firstname')->nullable();
          $table->string('g_lastname')->nullable();
          $table->string('g_initial')->nullable();
          $table->string('g_suffix')->nullable();
          $table->string('g_mobile')->nullable();
          $table->string('g_email')->nullable();
          $table->string('g_occupation')->nullable();
          $table->string('g_relationship')->nullable();
          /*
           *
           * 1st Year First Semester
           *
           */

          $table->rememberToken();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lagro_students');
    }
}
