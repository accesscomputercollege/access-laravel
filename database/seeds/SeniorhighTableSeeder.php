<?php

use Illuminate\Database\Seeder;

class SeniorhighTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('seniorhigh')->insert([
        [
          'id' => 1,
          'course_name' => 'Senior High School',
          'description' => 'Senior high school (SHS) refers to Grades 11 and 12, the last two years of the K-12 program that DepEd has been implementing since 2012. ... High school in the old system consisted of First Year to Fourth Year. What corresponds to those four years today are Grades 7 to 10, also known as junior high school (JHS).',
        ],
      ]);
    }
}
