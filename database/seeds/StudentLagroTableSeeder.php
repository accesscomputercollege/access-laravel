<?php

use Illuminate\Database\Seeder;
use App\StudentLagro;

class StudentLagroTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i=0; $i < 1000; $i++) {
          StudentLagro::create([
            'firstname' => $faker->firstNameMale,
            'lastname' => $faker->lastName,
            'middlename' => $faker->firstNameFemale,
            'mobile' => $faker->phoneNumber,
            'student_email' => $faker->email,
          ]);
        }
    }
}
