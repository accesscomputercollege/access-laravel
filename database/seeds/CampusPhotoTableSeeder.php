<?php

use Illuminate\Database\Seeder;

class CampusPhotoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('photos')->insert([
        [
          'id' => 1,
          'file' => 'balagtas_branch.jpg'
        ],
        [
          'id' => 2,
          'file' => 'camarin_branch.jpg'
        ],
        [
          'id' => 3,
          'file' => 'cubao_branch.jpg'
        ],
        [
          'id' => 4,
          'file' => 'lagro_branch.jpg'
        ],
        [
          'id' => 5,
          'file' => 'marilao_branch.jpg'
        ],
        [
          'id' => 6,
          'file' => 'meycauayan_branch.jpg'
        ],
        [
          'id' => 7,
          'file' => 'monumento_branch.jpg'
        ],
        [
          'id' => 8,
          'file' => 'marikina_branch.png'
        ],
        [
          'id' => 9,
          'file' => 'pasig_sannicolas_branch.jpg'
        ],
        [
          'id' => 10,
          'file' => 'recto_branch.png'
        ],
        [
          'id' => 11,
          'file' => 'zabarte_branch.jpg'
        ],
        [
          'id' => 12,
          'file' => 'pasig_kapasigan_branch.png'
        ],
        [
          'id' => 13,
          'file' => 'noimage.jpg'
        ],
        [
          'id' => 14,
          'file' => 'noimage.jpg'
        ],
        /**
         * NEWS
         */
         [
           'id' => 15,
           'file' => 'news1.jpg'
         ],
         [
           'id' => 16,
           'file' => 'news2.jpg'
         ],
         [
           'id' => 17,
           'file' => 'news3.jpg'
         ],
         [
           'id' => 18,
           'file' => 'news4.jpg'
         ],
         /**
          * EVENTS
          */
          [
            'id' => 19,
            'file' => 'event1.jpg'
          ],
          [
            'id' => 20,
            'file' => 'event2.jpg'
          ],
          [
            'id' => 21,
            'file' => 'event3.jpg'
          ],
          [
            'id' => 22,
            'file' => 'event4.jpg'
          ],
          [
            'id' => 23,
            'file' => 'event5.jpg'
          ],
      ]);
    }
}
