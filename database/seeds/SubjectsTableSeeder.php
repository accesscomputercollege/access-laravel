<?php

use Illuminate\Database\Seeder;

class SubjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('subjects')->insert([
        [
          'id' => '1',
          'subject' => 'Inquire about your campuses, programs, and tuition fees'
        ],
        [
          'id' => '2',
          'subject' => 'Request for my academic records (TOR, Diploma, etc.)'
        ],
        [
          'id' => '3',
          'subject' => 'Request for academic verification'
        ],
        [
          'id' => '4',
          'subject' => 'Make suggestions'
        ],
        [
          'id' => '5',
          'subject' => 'Make a complaint'
        ],
        [
          'id' => '6',
          'subject' => 'Ask something else'
        ],
      ]);
    }
}
