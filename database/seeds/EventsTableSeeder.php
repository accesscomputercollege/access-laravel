<?php

use Illuminate\Database\Seeder;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('events')->insert([
        [
          'id' => 1,
          'title'          => 'Battle of the Band',
          'detail'         => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
          'startdate'      => '2019-05-03',
          'start_time'     => '09:30:00',
          'end_time'       => '16:00:00',
          'note'           => 'To all students of ACCESS COMPUTER COLLEGES only',
          'photo_id'       => ''
        ],
        [
          'id' => 2,
          'title'          => 'Education gives us a knowledge of the world around us and changes',
          'detail'         => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
          'startdate'      => '2019-01-01',
          'start_time'     => '09:30:00',
          'end_time'       => '16:00:00',
          'note'           => 'To all students of ACCESS COMPUTER COLLEGES only',
          'photo_id'       => ''
        ],
        [
          'id' => 3,
          'title'          => 'Battle of the Band',
          'detail'         => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
          'startdate'      => '2019-07-07',
          'start_time'     => '09:30:00',
          'end_time'       => '16:00:00',
          'note'           => 'To all students of ACCESS COMPUTER COLLEGES only',
          'photo_id'       => ''
        ],
        [
          'id' => 4,
          'title'          => 'Some say education is the process of gaining information is nation.',
          'detail'         => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
          'startdate'      => '2019-04-04',
          'start_time'     => '09:30:00',
          'end_time'       => '16:00:00',
          'note'           => 'To all students of ACCESS COMPUTER COLLEGES only',
          'photo_id'       => ''
        ],
        [
          'id' => 5,
          'title'          => 'Battle of the Band',
          'detail'         => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
          'startdate'      => '2019-08-08',
          'start_time'     => '09:30:00',
          'end_time'       => '16:00:00',
          'note'           => 'To all students of ACCESS COMPUTER COLLEGES only',
          'photo_id'       => ''
        ],
        [
          'id' => 6,
          'title'          => 'Sport Fest 2019',
          'detail'         => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
          'startdate'      => '2019-09-09',
          'start_time'     => '09:30:00',
          'end_time'       => '16:00:00',
          'note'           => 'To all students of ACCESS COMPUTER COLLEGES only',
          'photo_id'       => ''
        ],
        [
          'id' => 7,
          'title'          => 'Battle of the Band',
          'detail'         => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
          'startdate'      => '2019-05-05',
          'start_time'     => '09:30:00',
          'end_time'       => '16:00:00',
          'note'           => 'To all students of ACCESS COMPUTER COLLEGES only',
          'photo_id'       => ''
        ],
        [
          'id' => 8,
          'title'          => 'Sport Fest 2019',
          'detail'         => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
          'startdate'      => '2019-07-07',
          'start_time'     => '09:30:00',
          'end_time'       => '16:00:00',
          'note'           => 'To all students of ACCESS COMPUTER COLLEGES only',
          'photo_id'       => ''
        ],
        [
          'id' => 9,
          'title'          => 'Battle of the Band',
          'detail'         => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
          'startdate'      => '2019-02-02',
          'start_time'     => '09:30:00',
          'end_time'       => '16:00:00',
          'note'           => 'To all students of ACCESS COMPUTER COLLEGES only',
          'photo_id'       => ''
        ],
      ]);
    }
}
