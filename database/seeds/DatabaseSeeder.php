<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
          CampusPhotoTableSeeder::class,
          CategoriesTableSeeder::class,
          SubjectsTableSeeder::class,
          // NewsTableSeeder::class,
          EventsTableSeeder::class,
          AnnouncementsTableSeeder::class,
          VocationalTableSeeder::class,
          CollegiateTableSeeder::class,
          ShortcoursesTableSeeder::class,
          SeniorhighTableSeeder::class,
      ]);
    }
}
