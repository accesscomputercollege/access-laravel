<?php

use Illuminate\Database\Seeder;

class ShortcoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('shortcourse')->insert([
        [
          'id' => 1,
          'course_name' => 'Computer Systems Servicing',
          'description' => 'This course is designed to develop knowledge, skills, and attitudes of a Computer Service Technician in accordance with industry standards. It covers basic and common competencies such as installing, maintaining, configuring, and diagnosing computer systems and networks.',
          'career_list' => '<ul>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                            </ul>'
        ],
        [
          'id' => 2,
          'course_name' => 'Electronic Products Assembly and Servicing',
          'description' => 'The Electronic Products Assembly and Servicing (EPAS) NC II Qualification consists of competencies that a person must possess to assemble electronic products, prepare printed circuit boards (PCB) modules and to install and service consumer and industrial electronic products and systems.',
          'career_list' => '<ul>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                            </ul>'
        ],
        [
          'id' => 3,
          'course_name' => 'Visual Graphic Design',
          'description' => 'Graphic designers create visual concepts, by hand or using computer software, to communicate ideas that inspire, inform, or captivate consumers. They develop the overall layout and production design for advertisements, brochures, magazines, and corporate reports.',
          'career_list' => '<ul>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                            </ul>'
        ],
        [
          'id' => 4,
          'course_name' => 'Bookkeeping',
          'description' => 'The creation of financial transactions includes posting information to accounting journals or accounting software from such source documents as invoices to customers, cash receipts, and supplier invoices. The bookkeeper also reconciles accounts to ensure their accuracy.',
          'career_list' => '<ul>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                            </ul>'
        ],
        [
          'id' => 5,
          'course_name' => 'Housekeeping',
          'description' => 'Housekeeping NC II is a technical-vocational program is designed to prepare students for housekeeping jobs in hotels, motels, and resorts. The program equips students with skills in housekeeping services, guest rooms preparations, laundering and cleaning.',
          'career_list' => '<ul>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                            </ul>'
        ],
        [
          'id' => 6,
          'course_name' => 'Food and Beverage Services',
          'description' => 'Food and Beverage Services NC II is a technical-vocational program that trains students on the preparation of food plans and meals for restaurants, hotels, canteens, banquets and functions, and basically any establishment that serves food to a large number of people.',
          'career_list' => '<ul>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                            </ul>'
        ],
        [
          'id' => 7,
          'course_name' => 'Bartending',
          'description' => 'This course offers training in bar operations and procedures common in full service bars, along with techniques of pouring and mixing drinks. The course objective is employment as a bartender in a full-service bar.',
          'career_list' => '<ul>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                            </ul>'
        ],
        [
          'id' => 8,
          'course_name' => 'Cookery',
          'description' => 'This course introduces the basic principles of sanitation and safety relative to the hospitality industry. Topics include personal hygiene, sanitation and safety regulations, use and care of equipment, the principles of food-borne illness, and other related topics.',
          'career_list' => '<ul>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                            </ul>'
        ],
      ]);
    }
}
