<?php

use Illuminate\Database\Seeder;

class NewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('news')->insert([
        [
          'id' => 1,
          'title'           => 'Some say education is the process of gaining information is nation.',
          'campus'          => 'All Campuses',
          'detail_brief'    => 'Belis nisl adipiscing sapien sed malesu diame lacus eget erat Cras mollis scele.',
          'detail_extended' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
          'photo_id'        => '15',
          'created_at'      => '2018-12-29 22:47:52',
        ],
        [
          'id' => 2,
          'title'           => 'Education gives us a knowledge of the world around us and changes',
          'campus'          => 'All Campuses',
          'detail_brief'    => 'Belis nisl adipiscing sapien sed malesu diame lacus eget erat Cras mollis scele.',
          'detail_extended' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
          'photo_id'        => '16',
          'created_at'      => '2018-12-29 22:47:52',
        ],
        [
          'id' => 3,
          'title'           => 'One thing I wish I can do is, to provide education for all child left behind',
          'campus'          => 'All Campuses',
          'detail_brief'    => 'Belis nisl adipiscing sapien sed malesu diame lacus eget erat Cras mollis scele.',
          'detail_extended' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
          'photo_id'        => '17',
          'created_at'      => '2018-12-29 22:47:52',
        ],
        [
          'id' => 4,
          'title'           => 'Some say education is the process of gaining information is nation.',
          'campus'          => 'All Campuses',
          'detail_brief'    => 'Belis nisl adipiscing sapien sed malesu diame lacus eget erat Cras mollis scele.',
          'detail_extended' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
          'photo_id'        => '18',
          'created_at'      => '2018-12-29 22:47:52',
        ],
        [
          'id' => 5,
          'title'           => 'One thing I wish I can do is, to provide education for all child left behind',
          'campus'          => 'All Campuses',
          'detail_brief'    => 'Belis nisl adipiscing sapien sed malesu diame lacus eget erat Cras mollis scele.',
          'detail_extended' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
          'photo_id'        => '15',
          'created_at'      => '2018-12-29 22:47:52',
        ],
        [
          'id' => 6,
          'title'           => 'Some say education is the process of gaining information is nation.',
          'campus'          => 'All Campuses',
          'detail_brief'    => 'Belis nisl adipiscing sapien sed malesu diame lacus eget erat Cras mollis scele.',
          'detail_extended' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
          'photo_id'        => '15',
          'created_at'      => '2018-12-29 22:47:52',
        ],
        [
          'id' => 7,
          'title'           => 'One thing I wish I can do is, to provide education for all child left behind',
          'campus'          => 'All Campuses',
          'detail_brief'    => 'Belis nisl adipiscing sapien sed malesu diame lacus eget erat Cras mollis scele.',
          'detail_extended' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
          'photo_id'        => '15',
          'created_at'      => '2018-12-29 22:47:52',
        ],
        [
          'id' => 8,
          'title'           => 'Some say education is the process of gaining information is nation.',
          'campus'          => 'Lagro',
          'detail_brief'    => 'Belis nisl adipiscing sapien sed malesu diame lacus eget erat Cras mollis scele.',
          'detail_extended' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
          'photo_id'        => '15',
          'created_at'      => '2018-12-29 22:47:52',
        ],
        [
          'id' => 9,
          'title'           => 'One thing I wish I can do is, to provide education for all child left behind',
          'campus'          => 'Zabarte',
          'detail_brief'    => 'Belis nisl adipiscing sapien sed malesu diame lacus eget erat Cras mollis scele.',
          'detail_extended' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
          'photo_id'        => '15',
          'created_at'      => '2018-12-29 22:47:52',
        ],
      ]);
    }
}
