<?php

use Illuminate\Database\Seeder;

class VocationalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('vocational')->insert([
        [
          'id' => 1,
          'course_code' => 'CS',
          'course_name' => 'Computer Science (2-yrs)',
          'description' => '<p>
                              Principal areas of study within Computer Science include artificial intelligence, computer systems and networks, security, database systems, human computer interaction, vision and graphics, numerical analysis, programming languages, software engineering, bioinformatics and theory of computing.
                            </p>',
          'career_list' => '<ul>
                              <li>Software Developer</li>
                              <li>Data Administrator</li>
                              <li>Computer Hardware Engineer</li>
                              <li>Computer System Analyst</li>
                              <li>Computer Network Architect</li>
                              <li>Web Developer</li>
                              <li>Information Security Analist</li>
                              <li>Computer Programer</li>
                            </ul>'
        ],
        [
          'id' => 2,
          'course_code' => 'ComSec',
          'course_name' => 'Computer Secretarial (2-yrs)',
          'description' => '<p>
                              Secretarial studies programs are also called secretarial science, administrative assistant, general office assistant or executive assistant programs. ... While general programs provide students with a variety of basic office skills, specialized programs include courses geared toward the medical or legal field.
                            </p>',
          'career_list' => '<ul>
                              <li>Admin Assistant</li>
                              <li>Plantsite  Office Secretary</li>
                              <li>Executive Secretary</li>
                              <li>Office Secretary</li>
                              <li>Urgent Sales Secretary</li>
                            </ul>'
        ],
        [
          'id' => 3,
          'course_code' => 'ComTech',
          'course_name' => 'Computer Technician (2-yrs)',
          'description' => '<p>
                              Undergraduate Computer Technician Degree. Undergraduate degree programs are usually intended for students wishing to troubleshoot computer systems, evaluate software or design computer networks at the entry level. ... Coursework typically focuses on the fundamentals of computer technology.
                            </p>',
          'career_list' => '<ul>
                              <li>IT Support Technician</li>
                              <li>Computer Technician</li>
                              <li>Laser jet printer technician</li>
                              <li>Printer Technician</li>
                              <li>IT Field Support/CCTV Technician</li>
                              <li>IT Service Center Technician</li>
                              <li>IT Help Desk Technician</li>
                              <li>Copier Technician</li>
                              <li>Service Desk Technician</li>
                            </ul>'
        ],
        [
          'id' => 4,
          'course_code' => 'ET',
          'course_name' => 'Electronics Technician (2-yrs)',
          'description' => '<p>
                              Electronics Technician Course and Class Descriptions. An electronics technician works with a variety of hand tools to repair and maintain electronic equipment. Courses for electronics technicians are generally completed as part of a postsecondary non-degree award program.
                            </p>',
          'career_list' => '<ul>
                              <li>Technician</li>
                              <li>Electrical technician</li>
                              <li>Electrician</li>
                              <li>Low voltage technician</li>
                              <li>Electrical technician (mining)</li>
                              <li>Medium voltage technician</li>
                              <li>Service technician</li>
                              <li>CCTV operator</li>
                            </ul>'
        ],
        [
          'id' => 5,
          'course_code' => 'HRS',
          'course_name' => 'Hotel and Restaurant Services (2-yrs)',
          'description' => '<p>
                              The two-year Hospitality and Restaurant Services program is designed to jumpstart the students career in the hospitality industry. Students will be prepared to earn national certificates on cookery, food and beverage, bartending, and local guiding services.
                            </p>',
          'career_list' => '<ul>
                              <li>Kitchen steward</li>
                              <li>Line cook</li>
                              <li>On call dinning staff</li>
                              <li>Internship for food and beverage department.</li>
                              <li>Full time cook</li>
                              <li>Executive chef</li>
                              <li>Demi chef</li>
                              <li>Sous chef</li>
                              <li>Waitress</li>
                              <li>Food and beverage manage/staff</li>
                              <li>Head chef</li>
                              <li>Assistant cook</li>
                              <li>Restaurant team lead</li>
                            </ul>'
        ],
      ]);
    }
}
