<?php

use Illuminate\Database\Seeder;

class CollegiateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('collegiate')->insert([
        [
          'id' => 1,
          'course_code' => 'BSCE',
          'course_name' => 'Bachelor of Science in Computer Engineering (5-yrs)',
          'description' => '<p>
                              The BS Computer Engineering course is designed to give strong foundations in physics, mathematics, electronics, programming methodology, data communication and computer organization to hone students in dealing with the theory and design of algorithms, hardware, software and electronic systems. The program encompasses the study of hardware design, data storage, computer architecture, assembly languages, and design of computers for engineering, information retrieval, and scientific researches.
                            </p>
                            <p>
                              Graduates of the course can choose to enter careers that involve the designs of computer hardware and software systems in various areas such as: computer graphics, computer-aided design, multimedia systems, databases, parallel computation, distributed computation, artificial intelligence, optical computing, large-scale-integration design, and fabrication as computer engineers.
                            </p>',
          'career_list' => '<ul>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                            </ul>'
        ],
        [
          'id' => 2,
          'course_code' => 'BSTM',
          'course_name' => 'Bachelor of Science in Tourism Management (4-yrs)',
          'description' => '<p>
                              The BS Tourism Management course provides students with focused skills on tourism in areas besides hospitality. In depth understanding and mastery of skills applied in various sectors that include Tourism Services, Attractions, Events, Seminars & Conferences, Transportation, Travel/Trade/Business and Adventure/Recreation are included in the program.
                            </p>
                            <p>
                              Graduates of the course can choose to enter careers that involve organization and execution of events for various industries requiring travels, seminars/conventions and special events as junior middle manager positions.
                            </p>',
          'career_list' => '<ul>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                            </ul>'
        ],
        [
          'id' => 3,
          'course_code' => 'BSBA',
          'course_name' => 'Bachelor of Science in Business Administration (4-yrs)',
          'description' => '<p>
                              The BS in Office Administration is designed for students who desire to enter the business world as office managers or administrative assistants. Emphasis is placed on office procedures, communications, office automation and general office management and coordination skills.
                            </p>
                            <p>
                              Graduates of the course can choose to enter careers that involve managing small to medium scale offices of any business field and serve as support to managerial and supervisory tasks as Administrative support staff.
                            </p>',
          'career_list' => '<ul>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                            </ul>'
        ],
        [
          'id' => 4,
          'course_code' => 'BSIT',
          'course_name' => 'Bachelor of Science in Information Technology (4-yrs)',
          'description' => '<p>
                              The BS information Technology course prepares students to become professionals in the digital age imparting comprehensive know-how of the leading information and communication technologies, computer programs and graphics through the application of industry standardized competencies.
                            </p>
                            <p>
                              Graduates of the course can choose to enter careers that involve analysis of design through the use of modern tools to administer computer networks, web pages and other peripherals for any industry utilizing computer technology for information required by the different functional units of a company as I.T. Consultants.
                            </p>',
          'career_list' => '<ul>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                            </ul>'
        ],
        [
          'id' => 5,
          'course_code' => 'BSCS',
          'course_name' => 'Bachelor of Science in Computer Science (4-yrs)',
          'description' => '<p>
                              The BS Computer Science course is designed for students seeking an integrated project-based curriculum that focuses on the computer skills highly valued in today’s industry. Projects and coursework are designed to provide strong foundations in technical skills, standards, and fundamentals such as programming, data structures and algorithms, and computer architecture as well as advanced study in operating systems, compilers and artificial intelligence.
                            </p>
                            <p>
                              Graduates of the course can choose to enter careers that involve computer programming, software designing, and computer systems creation in any industry utilizing computer applications in aiding daily operational functions as computer scientists.
                            </p>',
          'career_list' => '<ul>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                            </ul>'
        ],
        [
          'id' => 6,
          'course_code' => 'BSHRM',
          'course_name' => 'Bachelor of Science in Hotel and Restaurant Management (4-yrs)',
          'description' => '<p>
                              The BS Hotel and Restaurant Management course provides students with a broad base of management, technical skills and competencies needed to become industry leaders in the competitive hotel and restaurant business environment. Graduates of this degree can choose to enter careers in lodging, restaurant, travel, attractions, meeting and convention, and food service industries as junior and middle managers.
                            </p>',
          'career_list' => '<ul>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                            </ul>'
        ],
        [
          'id' => 7,
          'course_code' => 'BSOA',
          'course_name' => 'Bachelor of Science in Office Administration (4-yrs)',
          'description' => '<p>
                              The BS in Office Administration is designed for students who desire to enter the business world as office managers or administrative assistants. Emphasis is placed on office procedures, communications, office automation and general office management and coordination skills.
                            </p>
                            <p>
                              Graduates of the course can choose to enter careers that involve managing small to medium scale offices of any business field and serve as support to managerial and supervisory tasks as Administrative support staff.
                            </p>',
          'career_list' => '<ul>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                            </ul>'
        ],
        [
          'id' => 8,
          'course_code' => 'BTTEd',
          'course_name' => 'Bachelor of Technical Teacher Education',
          'description' => '<ul>
                              <li>Major in Electronics Technology (4-yrs)</li>
                              <li>Major in Food Service Management (4-yrs)</li>
                            </ul>',
          'career_list' => '<ul>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                              <li>Test career list</li>
                            </ul>'
        ],
      ]);
    }
}
