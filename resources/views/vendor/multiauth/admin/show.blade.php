@extends('multiauth::layouts.admin')

@section('title')
  Access | Admin Management
@endsection

@section('main-title')
  Admin Management
@endsection

@section('breadcrumb')
  <ol class="breadcrumb float-right">
      <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
      <li class="breadcrumb-item active">Admin Management</li>
  </ol>
@endsection

@section('main-content')
  @include('multiauth::message')
  <div class="row">
      <div class="col">
        <div class="card mb-3">
          <div class="card-header">
            <span class="float-right">
                <a href="{{route('admin.register')}}" class="btn btn-sm btn-success"><i class="fas fa-plus"></i> New {{ ucfirst(config('multiauth.prefix')) }}</a>
            </span>
            <h3><i class="fa fa-table"></i> Admin List</h3>

          </div>

          <div class="card-body">
            <div class="table-responsive">
            <table id="example1" class="table table-bordered table-hover display">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Role</th>
                  <th>Email Address</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($admins as $admin)
                  <tr>
                    <td><a href="{{route('admin.edit',[$admin->id])}}">{{ $admin->name }}</a></td>
                    <td>
                      @foreach ($admin->roles as $role)
                        <p class="badge badge-warning">{{ $role->name }}</p>
                      @endforeach
                    </td>
                    <td>{{ $admin->email }}</td>
                    <td>
                      <div class="text-center">
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-dark btn-sm mb-2" data-toggle="modal" data-target="#delete{{ $admin->id }}">
                          <i class="far fa-trash-alt"></i>
                        </button>
                        <!-- Modal -->
                        <div class="modal fade" id="delete{{ $admin->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Admin Management</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                Are you sure to delete <b>{{ $admin->name }}</b> from admin records?
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <a href="#" class="btn btn-danger" onclick="event.preventDefault(); document.getElementById('delete-form-{{ $admin->id }}').submit();">Delete</a>
                                <form id="delete-form-{{ $admin->id }}" action="{{ route('admin.delete',[$admin->id]) }}" method="POST" style="display: none;">
                                    @csrf @method('delete')
                                </form>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
            </div>

          </div>
        </div><!-- end card-->
      </div>
  </div>

{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    {{ ucfirst(config('multiauth.prefix')) }} List
                    <span class="float-right">
                        <a href="{{route('admin.register')}}" class="btn btn-sm btn-success">New {{ ucfirst(config('multiauth.prefix')) }}</a>
                    </span>
                </div>
                <div class="card-body">
    @include('multiauth::message')
                    <ul class="list-group">
                        @foreach ($admins as $admin)
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            {{ $admin->name }}
                            <span class="badge">
                                    @foreach ($admin->roles as $role)
                                        <span class="badge-warning badge-pill ml-2">
                                            {{ $role->name }}
                                        </span> @endforeach
                            </span>
                            <div class="float-right">
                                <a href="#" class="btn btn-sm btn-secondary mr-3" onclick="event.preventDefault(); document.getElementById('delete-form-{{ $admin->id }}').submit();">Delete</a>
                                <form id="delete-form-{{ $admin->id }}" action="{{ route('admin.delete',[$admin->id]) }}" method="POST" style="display: none;">
                                    @csrf @method('delete')
                                </form>

                                <a href="{{route('admin.edit',[$admin->id])}}" class="btn btn-sm btn-primary mr-3">Edit</a>
                            </div>
                        </li>
                        @endforeach @if($admins->count()==0)
                        <p>No {{ config('multiauth.prefix') }} created Yet, only super {{ config('multiauth.prefix') }} is available.</p>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div> --}}
@endsection
