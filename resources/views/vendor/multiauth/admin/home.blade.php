@extends('multiauth::layouts.admin')

@section('title')
  Access | Admin Home
@endsection

@section('main-title')
  Home
@endsection

@section('breadcrumb')
  <ol class="breadcrumb float-right">
      <li class="breadcrumb-item active">Home</li>
  </ol>
@endsection

@section('main-content')
<div class="row d-flex justify-content-center">
    <div class="col-md-8 ">
        <div class="card">
            <div class="card-header">{{ ucfirst(config('multiauth.prefix')) }} Dashboard</div>

            <div class="card-body">
                @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
                @endif You are logged in to {{ config('multiauth.prefix') }} side!
                <div class="greet mt-4 text-center">
                  <h3>Welcome</h3>
                  <p>{{ auth('admin')->user()->name }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
