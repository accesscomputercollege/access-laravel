@extends('multiauth::layouts.admin')

@section('title')
  Access | Admin Dashboard
@endsection

@section('main-title')
  Dashboard
@endsection

@section('breadcrumb')
  <ol class="breadcrumb float-right">
      <li class="breadcrumb-item active">Dashboard</li>
  </ol>
@endsection

@section('main-content')
  @admin('publisher')
  <div class="row">
    <div class="col">
        <div class="card-box noradius noborder bg-warning">
            <i class="fas fa-user-graduate float-right text-white"></i>
            <h6 class="text-white text-uppercase m-b-20">Lagro Students</h6>
            <h3 class="m-b-20 text-white counter">{{ $lagro->count() }}</h3>
            <span class="text-white">_</span>
        </div>
    </div>
  </div>
  <div class="row">
      <div class="col-xs-12 col-md-4 col-lg-4 col-xl-4">
          <div class="card-box noradius noborder bg-info">
              <i class="fas fa-bullhorn float-right text-white"></i>
              <h6 class="text-white text-uppercase m-b-20">Announcements</h6>
              <h3 class="m-b-20 text-white counter">{{ $campuses->count() }}</h3>
              <span class="text-white">_</span>
          </div>
      </div>

      <div class="col-xs-12 col-md-4 col-lg-4 col-xl-4">
          <div class="card-box noradius noborder bg-danger">
              <i class="fa fa-bell-o float-right text-white"></i>
              <h6 class="text-white text-uppercase m-b-20">All Events</h6>
              <h3 class="m-b-20 text-white counter">{{ $events->count() }}</h3>
              <span class="text-white">_</span>
          </div>
      </div>
      <div class="col-xs-12 col-md-4 col-lg-4 col-xl-4">
          <div class="card-box noradius noborder bg-primary">
              <i class="fa fa-newspaper float-right text-white"></i>
              <h6 class="text-white text-uppercase m-b-20">All News</h6>
              <h3 class="m-b-20 text-white counter">{{ $events->count() }}</h3>
              <span class="text-white">_</span>
          </div>
      </div>
  </div>
  @endadmin

  @admin('super', 'administrator')
    <div class="row">
        <div class="col-xs-12 col-md-4 col-lg-4 col-xl-4">
            <div class="card-box noradius noborder bg-warning">
                <i class="fas fa-user-graduate float-right text-white"></i>
                <h6 class="text-white text-uppercase m-b-20">Students</h6>
                <h3 class="m-b-20 text-white counter">{{ $lagro->count() }}</h3>
                <span class="text-white">_</span>
            </div>
        </div>

        <div class="col-xs-12 col-md-4 col-lg-4 col-xl-4">
            <div class="card-box noradius noborder bg-info">
                <i class="fas fa-school float-right text-white"></i>
                <h6 class="text-white text-uppercase m-b-20">Campuses</h6>
                <h3 class="m-b-20 text-white counter">{{ $campuses->count() }}</h3>
                <span class="text-white">_</span>
            </div>
        </div>

        <div class="col-xs-12 col-md-4 col-lg-4 col-xl-4">
            <div class="card-box noradius noborder bg-danger">
                <i class="fa fa-bell-o float-right text-white"></i>
                <h6 class="text-white text-uppercase m-b-20">Events</h6>
                <h3 class="m-b-20 text-white counter">{{ $events->count() }}</h3>
                <span class="text-white">_</span>
            </div>
        </div>
    </div>

    <div class="row">
      <div class="col-md-6">
        <div class="card-box noradius noborder bg-default">
            <i class="far fa-newspaper float-right text-white"></i>
            <h6 class="text-white text-uppercase m-b-20">School News</h6>
            <h3 class="m-b-20 text-white counter">{{ $news->count() }}</h3>
            <span class="text-white">_</span>
        </div>
      </div>
      <div class="col-md-6">
        <div class="card-box noradius noborder bg-default">
            <i class="fas fa-bullhorn float-right text-white"></i>
            <h6 class="text-white text-uppercase m-b-20">School Announcement</h6>
            <h3 class="m-b-20 text-white counter">{{ $announcements->count() }}</h3>
            <span class="text-white">_</span>
        </div>
      </div>
    </div>

    <h5>Programs</h5>
    <div class="row">
      <div class="col-sm-12 col-md-6 col-lg-6">
        <div class="card-box noradius noborder bg-primary">
            <i class="fas fa-graduation-cap float-right text-white"></i>
            <h6 class="text-white text-uppercase m-b-20">Vocational</h6>
            <h3 class="m-b-20 text-white counter">{{ $vocationals->count() }}</h3>
            <span class="text-white">_</span>
        </div>
      </div>
      <div class="col-sm-12 col-md-6 col-lg-6">
        <div class="card-box noradius noborder bg-info">
            <i class="fas fa-graduation-cap float-right text-white"></i>
            <h6 class="text-white text-uppercase m-b-20">Collegiate</h6>
            <h3 class="m-b-20 text-white counter">{{ $colleges->count() }}</h3>
            <span class="text-white">_</span>
        </div>
      </div>
      <div class="col-sm-12 col-md-6 col-lg-6">
        <div class="card-box noradius noborder bg-danger">
            <i class="fas fa-graduation-cap float-right text-white"></i>
            <h6 class="text-white text-uppercase m-b-20">Short Training</h6>
            <h3 class="m-b-20 text-white counter">{{ $shortcourses->count() }}</h3>
            <span class="text-white">_</span>
        </div>
      </div>
      <div class="col-sm-12 col-md-6 col-lg-6">
        <div class="card-box noradius noborder bg-danger">
            <i class="fas fa-graduation-cap float-right text-white"></i>
            <h6 class="text-white text-uppercase m-b-20">Senior High</h6>
            <h3 class="m-b-20 text-white counter">{{ $seniorhigh->count() }}</h3>
            <span class="text-white">_</span>
        </div>
      </div>
    </div>
  @endadmin
@endsection
