@extends('multiauth::layouts.admin')

@section('title')
  Access | Event Management
@endsection

@section('main-title')
  School Event Management
@endsection

@section('breadcrumb')
  <ol class="breadcrumb float-right">
      <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
      <li class="breadcrumb-item"><a href="{{ route('events.index') }}">School Event Management</a></li>
      <li class="breadcrumb-item active">Edit</li>
  </ol>
@endsection

@section('main-content')
  <div class="container">
      <div class="row justify-content-center">
          <div class="col">
              <div class="card">
                  <div class="card-header">Edit this Announcement</div>
                  <div class="card-body">
                    @include('multiauth::message')
                    {!!Form::open(['action' => ['Admin\EventsController@update', $event->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                        <div class="row mb-3">
                          <div class="col-md-4">
                            <img class="w-100 img-thumbnail" src="{{ $event->photo ? $event->photo->file : '' }}">
                          </div>
                          <div class="col-md-8">
                            <div class="form-group">
                                {{Form::label('title', 'Event Title')}}
                                {{Form::text('title', $event->title, ['class' => 'form-control', 'placeholder' => 'Title'])}}
                            </div>
                            <div class="form-group">
                                {{Form::label('photo_id', 'Event Image')}} <br>
                                {{Form::file('photo_id')}}
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                            {{Form::label('date', 'Start Date')}}
                            {{Form::date('startdate', $event->startdate, ['class' => 'form-control'])}}
                        </div>
                        <div class="form-row">
                          <div class="col">
                            <div class="form-group">
                                {{Form::label('start_time', 'Start Time')}} (Optional)
                                {{Form::time('start_time', $event->start_time, ['class' => 'form-control'])}}
                            </div>
                          </div>
                          <div class="col">
                            <div class="form-group">
                                {{Form::label('end_time', 'End Time')}} (Optional)
                                {{Form::time('end_time', $event->end_time, ['class' => 'form-control'])}}
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                            {{Form::label('detail', 'Content Brief')}}
                            {{Form::textarea('detail', $event->detail, ['id' => 'content_brief-ckeditor', 'class' => 'form-control'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('note', 'Important to note')}}
                            {{Form::textarea('note', $event->note, ['id' => 'content_extended-ckeditor', 'class' => 'form-control'])}}
                        </div>
                        {{Form::hidden('_method','PUT')}}
                        {{Form::submit('Save changes', ['class'=>'btn btn-sm btn-primary'])}}
                        <a href="/admin/events" class="btn btn-danger btn-sm float-right">Back</a>
                    {!! Form::close() !!}
                  </div>
              </div>
          </div>
      </div>
  </div>
@endsection

@section('script')
  <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
  <script type="text/javascript">
      CKEDITOR.replace( 'content_brief-ckeditor' );
      CKEDITOR.replace( 'content_extended-ckeditor' );
  </script>
@endsection
