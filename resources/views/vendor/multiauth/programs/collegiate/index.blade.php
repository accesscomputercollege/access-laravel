@extends('multiauth::layouts.admin')

@section('title')
  Access | Collegiate Programs
@endsection

@section('main-title')
  Collegiate Programs
@endsection

@section('breadcrumb')
  <ol class="breadcrumb float-right">
      <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
      <li class="breadcrumb-item active">Collegiate Programs</li>
  </ol>
@endsection

@section('main-content')
  @include('multiauth::message')
  <div class="row">
      <div class="col">
        <div class="card mb-3">
          <div class="card-header">
            <span class="float-right">
                <a href="/admin/collegiate/create" class="btn btn-sm btn-success"><i class="fas fa-plus"></i> New Collegiate Program</a>
            </span>
            <h3><i class="fa fa-table"></i> All Collegiate Programs</h3>
          </div>
          <div class="card-body">
            <div class="table-responsive">
            <table id="example1" class="table table-bordered">
              <thead>
                <tr>
                  <th>Course code</th>
                  <th>Name</th>
                  <th>Program type</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @if (count($collegiate) > 0)
                  @foreach ($collegiate as $college)
                  <tr>
                    <td><a href="/admin/collegiate/{{$college->id}}/edit">{{ $college->course_code }}</a></td>
                    <td>{{ $college->course_name }}</td>
                    <td>Collegiate</td>
                    <td>
                      <div class="text-center">
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-dark btn-sm mb-2" data-toggle="modal" data-target="#delete{{ $college->id }}">
                          <i class="far fa-trash-alt"></i>
                        </button>
                        <!-- Modal -->
                        <div class="modal fade" id="delete{{ $college->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Collegiate Program</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                Are you sure to delete this <b>{{ $college->course_name }}</b> Course?
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                {!!Form::open(['action' => ['Admin\CollegiateController@destroy', $college->id], 'method' => 'POST'])!!}
                                    {{Form::hidden('_method', 'DELETE')}}
                                    {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
                                {!!Form::close()!!}
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                @endforeach
              @endif
              </tbody>
            </table>
            </div>
          </div>
        </div><!-- end card-->
      </div>
  </div>
@endsection
