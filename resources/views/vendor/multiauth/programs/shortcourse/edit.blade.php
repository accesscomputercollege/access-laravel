@extends('multiauth::layouts.admin')

@section('title')
  Access | Short Training Programs
@endsection

@section('main-title')
  Short Training Programs
@endsection

@section('breadcrumb')
  <ol class="breadcrumb float-right">
      <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
      <li class="breadcrumb-item"><a href="{{ route('shortcourse.index') }}">Short Training Programs</a></li>
      <li class="breadcrumb-item active">Edit</li>
  </ol>
@endsection

@section('main-content')
  <div class="container">
      <div class="row justify-content-center">
          <div class="col">
              <div class="card">
                  <div class="card-header">Edit Short Training Programs</div>
                  <div class="card-body">
                    @include('multiauth::message')
                    {!! Form::open(['action' => ['Admin\ShortcourseController@update', $shortcourse->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                        <div class="form-group">
                          <label>Course Name</label>
                          <input type="text" name="course_name" class="form-control" value="{{ $shortcourse->course_name }}">
                        </div>
                        <div class="form-group">
                          <label>Course Description</label>
                          <textarea name="description" class="form-control" id="content_brief-ckeditor" rows="5">{{ $shortcourse->description }}</textarea>
                        </div>
                        <div class="form-group">
                          <label>Course Career Opportunities (Bullet type)</label>
                          <textarea name="career_list" class="form-control" id="content_extended-ckeditor" rows="5">{{ $shortcourse->career_list }}</textarea>
                        </div>
                        <div class="form-group">
                          <label for="campuses">Campus with this course</label>
                          <select name="campuses[]" class="form-control select2" multiple="multiple">
                            @foreach ($campuses->all() as $key => $campus)
                              @if ($key > 0)
                                <option value="{{ $campus->id }}"
                                    @if (in_array($campus->id,$shortcourse->campuses->pluck('id')->toArray()))
                                        selected
                                    @endif >{{ $campus->branch_name }}
                                </option>
                              @endif
                            @endforeach
                          </select>
                        </div>
                        {{Form::hidden('_method','PUT')}}
                        {{Form::submit('Save changes', ['class'=>'btn btn-sm btn-primary mt-5'])}}
                        <a href="/admin/shortcourse" class="btn btn-danger btn-sm float-right mt-5">Back</a>
                    {!! Form::close() !!}
                  </div>
              </div>
          </div>
      </div>
  </div>
@endsection

@section('script')
  <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
  <script type="text/javascript">
      CKEDITOR.replace( 'content_brief-ckeditor' );
      CKEDITOR.replace( 'content_extended-ckeditor' );
  </script>
@endsection
