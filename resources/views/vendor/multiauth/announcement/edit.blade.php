@extends('multiauth::layouts.admin')

@section('title')
  Access | Announcements
@endsection

@section('main-title')
  Announcements
@endsection

@section('breadcrumb')
  <ol class="breadcrumb float-right">
      <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
      <li class="breadcrumb-item"><a href="{{ route('admin.announcements') }}">Announcements</a></li>
      <li class="breadcrumb-item active">Edit</li>
  </ol>
@endsection

@section('main-content')
  <div class="container">
      <div class="row justify-content-center">
          <div class="col">
              <div class="card">
                  <div class="card-header">Edit this Announcement</div>

                  <div class="card-body">
                    @include('multiauth::message')
                      <form action="{{ route('admin.announcement.update', $announcement->id) }}" method="post">
                          @csrf @method('patch')
                          <div class="form-group">
                              <label for="role">Announcement Title</label>
                              <input type="text" value="{{ $announcement->title }}" name="title" class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}" id="announcement">
                          </div>
                          <div class="form-group">
                              <label class="mr-sm-2 sr-only" for="inlineFormCustomSelect">Campuses</label>
                              <select name="category_id" class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                                <option value="{{ $announcement->category->id }}" selected>
                                   {{$announcement->category->branch_name}}
                                </option>
                                @foreach ($categories as $category)
                                  <option value="{{ $category->id }}">{{$category->branch_name}}</option>
                                @endforeach
                              </select>
                          </div>
                          <div class="form-group">
                              <label for="role">Content Brief</label>
                              <textarea name="content_brief" id="content_brief-ckeditor" class="form-control {{ $errors->has('content_brief') ? ' is-invalid' : '' }}">{{ $announcement->content_brief }}</textarea>
                          </div>
                          <div class="form-group">
                              <label for="role">Content Extended</label>
                              <textarea name="content_extended" id="content_extended-ckeditor" class="form-control {{ $errors->has('content_extended') ? ' is-invalid' : '' }}">{{ $announcement->content_extended }}</textarea>
                          </div>
                          <button type="submit" class="btn btn-primary btn-sm">Save Changes</button>
                          <a href="{{ route('admin.announcements') }}" class="btn btn-danger btn-sm float-right">Back</a>
                      </form>
                  </div>
              </div>
          </div>
      </div>
  </div>
@endsection

@section('script')
  <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
  <script type="text/javascript">
      CKEDITOR.replace( 'content_brief-ckeditor' );
      CKEDITOR.replace( 'content_extended-ckeditor' );
  </script>
@endsection
