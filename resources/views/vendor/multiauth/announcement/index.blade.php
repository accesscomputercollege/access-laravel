@extends('multiauth::layouts.admin')

@section('title')
  Access | Announcements
@endsection

@section('main-title')
  Announcements
@endsection

@section('breadcrumb')
  <ol class="breadcrumb float-right">
      <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
      <li class="breadcrumb-item active">Announcements</li>
  </ol>
@endsection

@section('main-content')
  @include('multiauth::message')
  <div class="row">
      <div class="col">
        <div class="card mb-3">
          <div class="card-header">
            <span class="float-right">
                <a href="{{route('admin.announcement.create')}}" class="btn btn-sm btn-success"><i class="fas fa-plus"></i> New Announcement</a>
            </span>
            <h3><i class="fa fa-table"></i> Announcement List</h3>

          </div>

          <div class="card-body">
            <div class="table-responsive">
            <table id="example1" class="table table-bordered table-hover display">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Campus</th>
                  <th>Date post</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @if (count($announcements) > 0)
                  @foreach ($announcements as $announce)
                  <tr>
                    <td><a href="{{route('admin.announcement.edit',$announce->id)}}">{{ $announce->title }}</a></td>
                    <td>{{ $announce->category->branch_name }}</td>
                    <td>{{date('M j, Y', strtotime($announce->created_at))}}</td>
                    <td>
                      <div class="text-center">
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-dark btn-sm mb-2" data-toggle="modal" data-target="#delete{{$announce->id}}">
                          <i class="far fa-trash-alt"></i>
                        </button>
                        <!-- Modal -->
                        <div class="modal fade" id="delete{{$announce->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Announcement</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                Are you sure to delete this <b>{{ $announce->title }}</b> announcement from <b>{{ $announce->category->branch_name }}</b>?
                                <b></b>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-danger" onclick="event.preventDefault(); document.getElementById('delete-form-{{ $announce->id }}').submit();">Delete</button>
                                <form id="delete-form-{{ $announce->id }}" action="{{ route('admin.announcement.delete',$announce->id) }}" method="POST" style="display: none;">
                                  @csrf @method('delete')
                                </form>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                @endforeach
              @endif
              </tbody>
            </table>
            </div>
          </div>
        </div><!-- end card-->
      </div>
  </div>
@endsection
