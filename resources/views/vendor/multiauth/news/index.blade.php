@extends('multiauth::layouts.admin')

@section('title')
  Access | Access News
@endsection

@section('main-title')
  Access News
@endsection

@section('breadcrumb')
  <ol class="breadcrumb float-right">
      <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
      <li class="breadcrumb-item active">News Management</li>
  </ol>
@endsection

@section('main-content')
  @include('multiauth::message')
  <div class="row">
      <div class="col">
        <div class="card mb-3">
          <div class="card-header">
            <span class="float-right">
                <a href="/admin/news/create" class="btn btn-sm btn-success"><i class="fas fa-plus"></i> New News</a>
            </span>
            <h3><i class="fa fa-table"></i> News list</h3>

          </div>

          <div class="card-body">
            <div class="table-responsive">
            <table id="example1" class="table table-bordered table-hover display">
              <thead>
                <tr>
                  <th>Title</th>
                  <th>Campus</th>
                  <th>Date post</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @if (count($news) > 0)
                  @foreach ($news as $new)
                  <tr>
                    <td><a href="/admin/news/{{ $new->id }}/edit">{{ $new->title }}</a></td>
                    <td>{{ $new->campus }}</td>
                    <td>{{date('M j, Y', strtotime($new->created_at))}}</td>
                    <td>
                      <div class="text-center">
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-dark btn-sm mb-2" data-toggle="modal" data-target="#delete{{ $new->id }}">
                          <i class="far fa-trash-alt"></i>
                        </button>
                        <!-- Modal -->
                        <div class="modal fade" id="delete{{ $new->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">School news</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                Are you sure to delete this <b>{{ $new->title }}</b> news?
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                                {!!Form::open(['action' => ['Admin\NewsController@destroy', $new->id], 'method' => 'POST'])!!}
                                    {{Form::hidden('_method', 'DELETE')}}
                                    {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
                                {!!Form::close()!!}
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                @endforeach
              @endif
              </tbody>
            </table>
            </div>
          </div>
        </div><!-- end card-->
      </div>
  </div>
@endsection
