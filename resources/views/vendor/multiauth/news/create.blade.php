@extends('multiauth::layouts.admin')

@section('title')
  Access | News Management
@endsection

@section('main-title')
  Access News Management
@endsection

@section('breadcrumb')
  <ol class="breadcrumb float-right">
      <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
      <li class="breadcrumb-item"><a href="{{ route('news.index') }}">News Management</a></li>
      <li class="breadcrumb-item active">Create</li>
  </ol>
@endsection

@section('main-content')
  <div class="container">
      <div class="row justify-content-center">
          <div class="col">
              <div class="card">
                  <div class="card-header">Add News</div>
                  <div class="card-body">
                    @include('multiauth::message')
                    {!! Form::open(['action' => 'Admin\NewsController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                        <div class="form-group">
                            {{Form::label('title', 'News Title')}}
                            {{Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'Title'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('photo_id', 'News Image')}} <br>
                            {{Form::file('photo_id')}}
                        </div>
                        <div class="form-group">
                          <label>Campus</label><br>
                            <select name="campus" id="campus" class="form-control">
                              @foreach ($campuses as $campus)
                                <option value="{{ $campus->branch_name }}">{{ $campus->branch_name }}</option>
                              @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            {{Form::label('detail_brief', 'Detail Brief')}}
                            {{Form::textarea('detail_brief', '', ['id' => 'content_brief-ckeditor', 'class' => 'form-control', 'placeholder' => 'Body Text'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('detail_extended', 'Detail Extended')}}
                            {{Form::textarea('detail_extended', '', ['id' => 'content_extended-ckeditor', 'class' => 'form-control', 'placeholder' => 'Body Text'])}}
                        </div>

                        {{Form::submit('Store', ['class'=>'btn btn-sm btn-primary'])}}
                        <a href="/admin/news" class="btn btn-danger btn-sm float-right">Back</a>
                    {!! Form::close() !!}
                  </div>
              </div>
          </div>
      </div>
  </div>
@endsection

@section('script')
  <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
  <script type="text/javascript">
      CKEDITOR.replace( 'content_brief-ckeditor' );
      CKEDITOR.replace( 'content_extended-ckeditor' );
  </script>
@endsection
