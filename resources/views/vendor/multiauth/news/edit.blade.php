@extends('multiauth::layouts.admin')

@section('title')
  Access | News Management
@endsection

@section('main-title')
  Access News Management
@endsection

@section('breadcrumb')
  <ol class="breadcrumb float-right">
      <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
      <li class="breadcrumb-item"><a href="{{ route('news.index') }}">News Management</a></li>
      <li class="breadcrumb-item active">Edit</li>
  </ol>
@endsection

@section('main-content')
  <div class="container">
      <div class="row justify-content-center">
          <div class="col">
              <div class="card">
                  <div class="card-header">Edit this News</div>

                  <div class="card-body">
                    @include('multiauth::message')
                    {!!Form::open(['action' => ['Admin\NewsController@update', $new->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                        <div class="row mb-3">
                          <div class="col-md-4">
                            <img class="w-100 img-thumbnail" src="{{ $new->photo ? $new->photo->file : 'http://placehold.it/100x100' }}">
                          </div>
                          <div class="col-md-8">
                            <div class="form-group">
                                {{Form::label('title', 'Event Title')}}
                                {{Form::text('title', $new->title, ['class' => 'form-control', 'placeholder' => 'Title'])}}
                            </div>
                            <div class="form-group">
                                {{Form::label('photo_id', 'News Image')}} <br>
                                {{Form::file('photo_id')}}
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                            <label>Campuses</label><br>
                            <select name="campus" class="custom-select">
                              <option value="{{ $campus->branch_name ? $campus->branch_name : 'Select Campus' }}" selected>{{ $campus->branch_name ? $campus->branch_name : 'Select Campus' }}</option>
                              @foreach ($campuses as $campus)
                                <option value="{{ $campus->branch_name }}">{{$campus->branch_name}}</option>
                              @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            {{Form::label('detail_brief', 'Content Brief')}}
                            {{Form::textarea('detail_brief', $new->detail_brief, ['id' => 'content_brief-ckeditor', 'class' => 'form-control'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('detail_extended', 'Content Extended')}}
                            {{Form::textarea('detail_extended', $new->detail_extended, ['id' => 'content_extended-ckeditor', 'class' => 'form-control'])}}
                        </div>
                        {{Form::hidden('_method','PUT')}}
                        {{Form::submit('Store', ['class'=>'btn btn-sm btn-primary'])}}
                        <a href="/admin/news" class="btn btn-danger btn-sm float-right">Back</a>
                    {!! Form::close() !!}
                  </div>
              </div>
          </div>
      </div>
  </div>
@endsection

@section('script')
  <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
  <script type="text/javascript">
      CKEDITOR.replace( 'content_brief-ckeditor' );
      CKEDITOR.replace( 'content_extended-ckeditor' );
  </script>
@endsection
