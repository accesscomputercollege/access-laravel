<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    @include('multiauth::shared.links')
</head>

<body class="adminbody">
    <div id="main">
      <div class="adminlogin">
        @yield('content')<!-- Admin Login -->
      </div>
      @guest('admin')
        @else
          @include('multiauth::shared.header')
          @include('multiauth::shared.sidebar')
          <div class="content-page">
              <!-- Start content -->
              <div class="content">
                  <div class="container-fluid">
                      <div class="row">
                          <div class="col-xl-12">
                              <div class="breadcrumb-holder">
                                  <h1 class="main-title float-left">@yield('main-title')</h1>
                                  @yield('breadcrumb')
                                  <div class="clearfix"></div>
                              </div>
                          </div>
                      </div>
                      @yield('main-content')<!-- Admin Dasboard -->
                  </div>
              </div>
          </div>
          @include('multiauth::shared.footer')
      @endguest
    </div>

    @yield('script')
    @include('multiauth::shared.script')
    @include('multiauth::shared.script_grades')
  </body>
</html>
