@extends('multiauth::layouts.admin')

@section('title')
  Access | Campus Management
@endsection

@section('main-title')
  Campus Management
@endsection

@section('breadcrumb')
  <ol class="breadcrumb float-right">
      <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
      <li class="breadcrumb-item"><a href="{{ route('campuses.index') }}">Campus Management</a></li>
      <li class="breadcrumb-item active">Create</li>
  </ol>
@endsection

@section('main-content')
  <div class="container">
      <div class="row justify-content-center">
          <div class="col">
              <div class="card">
                  <div class="card-header">Add Campus</div>
                  <div class="card-body">
                    @include('multiauth::message')
                    {!! Form::open(['action' => 'Admin\CampusesController@store', 'method' => 'POST', 'files' => true]) !!}
                        <div class="form-group">
                            {{Form::label('branch_name', 'Campus Name')}}
                            {{Form::text('branch_name', '', ['class' => 'form-control', 'placeholder' => 'Campus Name'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('slug', 'Confirm Campus Name (url)')}}
                            {{Form::text('slug', '', ['class' => 'form-control', 'placeholder' => 'Confirm Campus Name'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('photo_id', 'Campus Image')}} <br>
                            {{Form::file('photo_id')}}
                        </div>

                        <div class="form-group mt-5">
                          <h4 class="mb-3">School Information</h4>
                          <div class="form-group">
                              {{Form::label('address', 'School Address')}}
                              {{Form::text('address', '', ['class' => 'form-control', 'placeholder' => 'Address'])}}
                          </div>
                          <div class="form-row">
                            <div class="col-md-6 mb-3">
                              {{Form::label('telephone', 'Telephone number')}}
                              {{Form::text('telephone', '', ['class' => 'form-control', 'placeholder' => 'Tel nos.'])}}
                            </div>
                            <div class="col-md-6 mb-3">
                              {{Form::label('mobile', 'Mobile number')}}
                              {{Form::text('mobile', '', ['class' => 'form-control', 'placeholder' => 'Mobile nos.'])}}
                            </div>
                          </div>
                          <div class="form-group">
                              {{Form::label('facebook', 'Facebook')}}
                              {{Form::text('facebook', '', ['class' => 'form-control', 'placeholder' => 'Facebook page'])}}
                          </div>
                          <div class="form-group">
                              {{Form::label('email', 'Campus Email Address')}}
                              {{Form::email('email', '', ['class' => 'form-control', 'placeholder' => 'Email Address'])}}
                          </div>
                          <div class="form-group">
                              {{Form::label('map', 'Location Map (Embed a map)')}}
                              <a href="https://www.google.com/maps/@14.752383,121.0270005,14z" target="_blank">Google Map</a>
                              {{Form::text('map', '', ['class' => 'form-control', 'placeholder' => 'Location Map'])}}
                              <div class="mt-2">
                                <a class="btn btn-warning btn-sm" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                  Instructions
                                </a>
                                <div class="collapse alert alert-warning mt-2" id="collapseExample">
                                  <p class="text-muted">Step 1: SEARCH GOOGLE MAP</p>
                                  <p class="text-muted">Step 2: CLICK SHARE</p>
                                  <p class="text-muted">Step 3: CLICK EMBED A MAP</p>
                                  <p class="text-muted">Step 4: COPY HTML (MEDIUM SIZE)</p>
                                  <p class="text-muted">Step 5: PASTE INTO LOCATION MAP FIELD THEN CHANGE THE WIDTH TO 100% (width="100%")</p>
                                </div>
                              </div>
                          </div>
                        </div>

                        <div class="form mt-5">
                        <h4 class="mb-4">Available Programs</h4>
                          <div class="form-group">
                            <label for="seniorhigh">Senior High School</label>
                            <select name="seniorhigh[]" class="form-control select2" multiple="multiple">
                              @foreach ($seniorhigh as $senior)
                                  <option value="{{ $senior->id }}">{{ $senior->course_name }}</option>
                              @endforeach
                            </select>
                          </div>
                          <div class="form-group">
                            <label for="vocationals">Vocational Programs</label>
                            <select name="vocationals[]" class="form-control select2" multiple="multiple">
                              @foreach ($vocationals as $vocational)
                                  <option value="{{ $vocational->id }}">{{ $vocational->course_name }}</option>
                              @endforeach
                            </select>
                          </div>
                          <div class="form-group">
                            <label for="collegiate">Collegiate Programs</label>
                            <select name="collegiate[]" class="form-control select2" multiple="multiple">
                              @foreach ($collegiate as $college)
                                  <option value="{{ $college->id }}">{{ $college->course_name }}</option>
                              @endforeach
                            </select>
                          </div>
                          <div class="form-group">
                            <label for="shortcourses">Short Training Programs</label>
                            <select name="shortcourses[]" class="form-control select2" multiple="multiple">
                              @foreach ($shortcourses as $shortcourse)
                                  <option value="{{ $shortcourse->id }}">{{ $shortcourse->course_name }}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>

                        {{Form::submit('Store', ['class'=>'btn btn-sm btn-primary'])}}
                        <a href="/admin/campuses" class="btn btn-danger btn-sm float-right">Back</a>
                    {!! Form::close() !!}
                  </div>
              </div>
          </div>
      </div>
  </div>
@endsection
