@extends('multiauth::layouts.admin')

@section('title')
  Access | Access Recto
@endsection

@section('main-title')
  Access Recto
@endsection

@section('breadcrumb')
  <ol class="breadcrumb float-right">
      <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
      <li class="breadcrumb-item active">Access Recto</li>
  </ol>
@endsection

@section('main-content')
  @include('multiauth::message')
  <div class="row">
      <div class="col">
        <div class="card mb-3">
          <div class="card-header">
            <span class="float-right">
                <a href="/admin/recto/students/create" class="btn btn-sm btn-success ml-3 mb-2"><i class="fas fa-plus"></i> Add New Student</a>
            </span>
            <h3><i class="fa fa-table"></i> Students list</h3>
          </div>
          <div class="card-body">
            <div class="table-responsive">
            <table id="example1" class="table table-bordered table-hover display">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Avatar</th>
                  <th>Student ID</th>
                  <th>Section</th>
                  <th>Email Adress</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($students as $student)
                  <tr>
                    <td class="text-capitalize"><a href="/admin/recto/students/{{ $student->id }}/edit">{{ $student->firstname }} {{ $student->lastname }} {{ $student->middlename }}</a></td>
                    <td class="text-center"><img class="rounded-circle" src="http://placehold.it/100x100" width="50"></td>
                    <td class="text-uppercase">{{ $student->studentid }}</td>
                    <td>BSIT-M81</td>
                    <td>{{ $student->email }}</td>
                    <td>
                      <div class="text-center">
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-dark btn-sm mb-2" data-toggle="modal" data-target="#delete{{ $student->id }}">
                          <i class="far fa-trash-alt"></i>
                        </button>
                        <!-- Modal -->
                        <div class="modal fade" id="delete{{ $student->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Student Management</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                Are you sure to delete <b>{{ $student->firstname }}, {{ $student->lastname }}</b> from records?
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                                {!!Form::open(['action' => ['Admin\Student\RectoController@destroy', $student->id], 'method' => 'POST'])!!}
                                    {{Form::hidden('_method', 'DELETE')}}
                                    {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
                                {!!Form::close()!!}
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
            </div>
          </div>
        </div><!-- end card-->
      </div>
  </div>
@endsection
