@extends('multiauth::layouts.admin')

@section('title')
  Access | Access Recto
@endsection

@section('main-title')
  Access Recto
@endsection

@section('breadcrumb')
  <ol class="breadcrumb float-right">
      <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
      <li class="breadcrumb-item"><a href="/admin/recto/students">Student Management</a></li>
      <li class="breadcrumb-item active">Edit</li>
  </ol>
@endsection

@section('main-content')
<div class="container">
  {!!Form::open(['action' => ['Admin\Student\RectoController@update', $student->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
    <div class="accordion" id="accordionExample">
      <div class="row justify-content-center">
          <div class="col-md-3">
            <div class="avatar">
              <img class="mb-3 rounded" src="http://placehold.it/100x100" width="100%">
            </div>
            <div class="list-group mb-3">
              <button type="button" class="list-group-item list-group-item-action" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                Student Account
              </button>
              <button type="button" class="list-group-item list-group-item-action" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                Student Information
              </button>
              <button type="button" class="list-group-item list-group-item-action" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                Grades
              </button>
              <button type="button" class="list-group-item list-group-item-action">Balance</button>
            </div>
            <div class="form-group mb-0">
              {{Form::hidden('_method','PUT')}}
              <button type="submit" class="btn btn-block btn-primary">Save changes</button>
            </div>
          </div>
          <div class="col-md-9">
            @if (session()->has('message'))
                <div class="alert alert-success">{{ session()->get('message') }}</div>
            @endif
              <div class="card collapse show" id="collapseOne" aria-labelledby="headingOne" data-parent="#accordionExample">
                {{--

                    Student Account

                 --}}
                  <div class="card-header h5">Student Account</div>
                  <div class="card-body">
                        <div class="form-group row">
                          <div class="col-md-4">
                            <label for="firstname">Firstname</label>
                            <input id="firstname" type="text" class="form-control text-capitalize{{ $errors->has('firstname') ? ' is-invalid' : '' }}" name="firstname" value="{{ $student->firstname }}" required autofocus>
                            @if ($errors->has('firstname'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('firstname') }}</strong>
                                </span>
                            @endif
                          </div>
                          <div class="col-md-4">
                            <label for="lastname">Lastname</label>
                            <input id="lastname" type="text" class="form-control text-capitalize{{ $errors->has('lastname') ? ' is-invalid' : '' }}" name="lastname" value="{{ $student->lastname }}" required autofocus>
                            @if ($errors->has('lastname'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('lastname') }}</strong>
                                </span>
                            @endif
                          </div>
                          <div class="col-md-4">
                            <label for="middlename">Middlename</label>
                            <input id="middlename" type="text" class="form-control text-capitalize{{ $errors->has('middlename') ? ' is-invalid' : '' }}" name="middlename" value="{{ $student->middlename }}" required autofocus>
                            @if ($errors->has('middlename'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('middlename') }}</strong>
                                </span>
                            @endif
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="studentid">Student ID number</label>
                          <input id="studentid" type="text" class="form-control text-uppercase{{ $errors->has('studentid') ? ' is-invalid' : '' }}" name="studentid" value="{{ $student->studentid }}" required autofocus>
                          @if ($errors->has('studentid'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('studentid') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group">
                          <label for="email">Email Address</label>
                          <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $student->email }}" required autofocus>
                          @if ($errors->has('email'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('email') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="form-group row">
                          <div class="col-md-6">
                            <label for="password">{{ __('Password') }}</label>
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" value="{{ $student->password }}" required>
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                          </div>
                          <div class="col-md-6">
                            <label for="password-confirm">{{ __('Confirm Password') }}</label>
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" value="{{ $student->password }}" required>
                          </div>
                        </div>


                  </div>
              </div>
              {{--

                  Student Information

               --}}
              <div class="collapse" id="collapseTwo" aria-labelledby="headingTwo" data-parent="#accordionExample">
                <div class="card">
                  <div class="card-header h5">Student's Information</div>
                  <div class="card-body">
                    <div class="form-group">
                      <label for="exampleFormControlSelect1">Campuses</label>
                      <select class="form-control form-control-sm" id="exampleFormControlSelect1">
                        <option>Select ACCESS School</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="exampleFormControlSelect1">Courses</label>
                      <select class="form-control form-control-sm" id="exampleFormControlSelect1">
                        <option>Select Course</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                      </select>
                    </div>
                    <div class="form-group row">
                      <div class="col-md-4">
                        <label>Firstname</label>
                        <input type="text" class="form-control form-control-sm text-capitalize" name="" value="{{ $student->firstname }}" disabled>
                      </div>
                      <div class="col-md-4">
                        <label>Lastname</label>
                        <input type="text" class="form-control form-control-sm text-capitalize" name="" value="{{ $student->lastname }}" disabled>
                      </div>
                      <div class="col-md-4">
                        <label>Middlename</label>
                        <input type="text" class="form-control form-control-sm text-capitalize" name="" value="{{ $student->middlename }}" disabled>
                      </div>
                    </div>

                    <div class="form-group row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="exampleFormControlSelect1">Gender</label>
                          <select class="form-control form-control-sm" id="exampleFormControlSelect1">
                            <option></option>
                            <option>2</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="exampleFormControlSelect1">Status</label>
                          <select class="form-control form-control-sm" id="exampleFormControlSelect1">
                            <option></option>
                            <option>2</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>Citizenship</label>
                          <input type="text" class="form-control form-control-sm" name="" value="">
                        </div>
                      </div>
                    </div>

                    <div class="form-group row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>Birthplace</label>
                          <input type="text" class="form-control form-control-sm" name="" value="">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>Religion</label>
                          <input type="text" class="form-control form-control-sm" name="" value="">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>Date of Birth</label>
                          <input type="date" class="form-control form-control-sm" name="" value="">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="card mt-3">
                  <div class="card-header h5">Current Address</div>
                  <div class="card-body">
                    <div class="form-group row">
                      <div class="col-md-3">
                        <label>Street No. / Unit No.</label>
                        <input type="text" class="form-control form-control-sm" name="" value="">
                      </div>
                      <div class="col-md-9">
                        <label>Street</label>
                        <input type="text" class="form-control form-control-sm" name="" value="">
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-md-6">
                        <label>Subdivision / Village / Building</label>
                        <input type="text" class="form-control form-control-sm" name="" value="">
                      </div>
                      <div class="col-md-6">
                        <label>Barangay</label>
                        <input type="text" class="form-control form-control-sm" name="" value="">
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-md-5">
                        <div class="form-group">
                          <label>City / Municipality</label>
                          <input type="text" class="form-control form-control-sm" name="" value="">
                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="form-group">
                          <label>Province</label>
                          <input type="text" class="form-control form-control-sm" name="" value="">
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group">
                          <label>Zip Code</label>
                          <input type="text" class="form-control form-control-sm" name="" value="">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="card mt-3">
                  <div class="card-header h5">Contact Details</div>
                  <div class="card-body">
                    <div class="form-group row">
                      <div class="col-md-6">
                        <label>Telephone No.</label>
                        <input type="text" class="form-control form-control-sm" name="" value="">
                      </div>
                      <div class="col-md-6">
                        <label>Mobile No.</label>
                        <input type="text" class="form-control form-control-sm" name="" value="">
                      </div>
                    </div>
                  </div>
                </div>

                <div class="card mt-3">
                  <div class="card-header h5">Current or Last School Attended</div>
                  <div class="card-body">
                    <div class="form-group row">
                      <div class="col-md-6">
                        <label for="exampleFormControlSelect1">School Type</label>
                        <select class="form-control form-control-sm" id="exampleFormControlSelect1">
                          <option>2</option>
                          <option>3</option>
                          <option>4</option>
                          <option>5</option>
                        </select>
                      </div>
                      <div class="col-md-6">
                        <label>Name of School</label>
                        <input type="text" class="form-control form-control-sm" name="" value="">
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-md-3">
                        <label>Date of Graduation</label>
                        <input type="date" class="form-control form-control-sm" name="" value="">
                      </div>
                      <div class="col-md-3">
                        <label for="exampleFormControlSelect1">School Year</label>
                        <select class="form-control form-control-sm" id="exampleFormControlSelect1">
                          <option>2</option>
                          <option>3</option>
                          <option>4</option>
                          <option>5</option>
                        </select>
                      </div>
                      <div class="col-md-3">
                        <label for="exampleFormControlSelect1">Year / Grade</label>
                        <select class="form-control form-control-sm" id="exampleFormControlSelect1">
                          <option>2</option>
                          <option>3</option>
                          <option>4</option>
                          <option>5</option>
                        </select>
                      </div>
                      <div class="col-md-3">
                        <label for="exampleFormControlSelect1">Term</label>
                        <select class="form-control form-control-sm" id="exampleFormControlSelect1">
                          <option>2</option>
                          <option>3</option>
                          <option>4</option>
                          <option>5</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="card mt-3">
                  <div class="card-header h5">Parents / Guardian's Information</div>
                  <div class="card-body">
                    <h6>Father's Information</h6>
                    <div class="form-group row">
                      <div class="col-md-4">
                        <label>First Name</label>
                        <input type="text" class="form-control form-control-sm" name="" value="">
                      </div>
                      <div class="col-md-4">
                        <label>Last Name</label>
                        <input type="text" class="form-control form-control-sm" name="" value="">
                      </div>
                      <div class="col-md-2">
                        <label>Middle Initial</label>
                        <input type="text" class="form-control form-control-sm" name="" value="">
                      </div>
                      <div class="col-md-2">
                        <label>Suffix</label>
                        <input type="text" class="form-control form-control-sm" name="" value="">
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-md-4">
                        <label>Mobile Number</label>
                        <input type="text" class="form-control form-control-sm" name="" value="">
                      </div>
                      <div class="col-md-4">
                        <label>Email</label>
                        <input type="text" class="form-control form-control-sm" name="" value="">
                      </div>
                      <div class="col-md-4">
                        <label>Occupation</label>
                        <input type="text" class="form-control form-control-sm" name="" value="">
                      </div>
                    </div>

                    <hr class="my-5">

                    <h6>Mother's Information</h6>
                    <div class="form-group row">
                      <div class="col-md-4">
                        <label>First Name</label>
                        <input type="text" class="form-control form-control-sm" name="" value="">
                      </div>
                      <div class="col-md-4">
                        <label>Last Name</label>
                        <input type="text" class="form-control form-control-sm" name="" value="">
                      </div>
                      <div class="col-md-2">
                        <label>Middle Initial</label>
                        <input type="text" class="form-control form-control-sm" name="" value="">
                      </div>
                      <div class="col-md-2">
                        <label>Suffix</label>
                        <input type="text" class="form-control form-control-sm" name="" value="">
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-md-4">
                        <label>Mobile Number</label>
                        <input type="text" class="form-control form-control-sm" name="" value="">
                      </div>
                      <div class="col-md-4">
                        <label>Email</label>
                        <input type="text" class="form-control form-control-sm" name="" value="">
                      </div>
                      <div class="col-md-4">
                        <label>Occupation</label>
                        <input type="text" class="form-control form-control-sm" name="" value="">
                      </div>
                    </div>

                    <hr class="my-5">

                    <h6>Guardian's Information</h6>
                    <div class="form-group row">
                      <div class="col-md-4">
                        <label>First Name</label>
                        <input type="text" class="form-control form-control-sm" name="" value="">
                      </div>
                      <div class="col-md-4">
                        <label>Last Name</label>
                        <input type="text" class="form-control form-control-sm" name="" value="">
                      </div>
                      <div class="col-md-2">
                        <label>Middle Initial</label>
                        <input type="text" class="form-control form-control-sm" name="" value="">
                      </div>
                      <div class="col-md-2">
                        <label>Suffix</label>
                        <input type="text" class="form-control form-control-sm" name="" value="">
                      </div>
                    </div>

                    <div class="form-group row">
                      <div class="col-md-3">
                        <label>Mobile Number</label>
                        <input type="text" class="form-control form-control-sm" name="" value="">
                      </div>
                      <div class="col-md-3">
                        <label>Email</label>
                        <input type="text" class="form-control form-control-sm" name="" value="">
                      </div>
                      <div class="col-md-3">
                        <label>Occupation</label>
                        <input type="text" class="form-control form-control-sm" name="" value="">
                      </div>
                      <div class="col-md-3">
                        <label>Relationship</label>
                        <input type="text" class="form-control form-control-sm" name="" value="">
                      </div>
                    </div>
                  </div>
                </div>
              </div>


              {{--

                  Student Grades

               --}}
              <div class="card collapse" id="collapseThree" aria-labelledby="headingThree" data-parent="#accordionExample">
                <div class="card-header h5">Grades</div>
                <div class="card-body">

                </div>
              </div>
          </div>
      </div>
    </div>
  {!! Form::close() !!}
</div>
@endsection
