@extends('multiauth::layouts.admin')

@section('title')
  Access | Access Lagro
@endsection

@section('main-title')
  Access Lagro
@endsection

@section('breadcrumb')
  <ol class="breadcrumb float-right">
      <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
      <li class="breadcrumb-item"><a href="/admin/lagro/students">Student Management</a></li>
      <li class="breadcrumb-item active">Create</li>
  </ol>
@endsection

@section('main-content')
  <div class="container">
      <div class="row justify-content-center">
          <div class="col">
            @if (session()->has('message'))
                <div class="alert alert-success">{{ session()->get('message') }}</div>
            @endif
              <div class="card">
                  <div class="card-header">Create Student</div>

                  <div class="card-body">
                      {!! Form::open(['action' => 'Admin\Student\LagroController@store', 'method' => 'POST', 'files' => true]) !!}
                          <div class="form-group avatar text-center">
                            <label>Avatar</label><br>
                            <input type="file" name="photo_id" class="border">
                          </div>
                          <div class="form-group row">
                            <div class="col-md-6">
                              <label>Courses</label>
                              <select name="course" class="form-control">
                                <optgroup label="Senior High School">
                                  @foreach ($seniorhigh as $senior)
                                    <option value="{{ $senior->course_name }}">{{ $senior->course_name }}</option>
                                  @endforeach
                                </optgroup>
                                <optgroup label="Vocational Programs">
                                  @foreach ($vocationals as $voc)
                                    <option value="{{ $voc->course_name }}">{{ $voc->course_name }}</option>
                                  @endforeach
                                </optgroup>
                                <optgroup label="Collegiate Programs">
                                  @foreach ($collegiate as $college)
                                    <option value="{{ $college->course_name }}">{{ $college->course_name }}</option>
                                  @endforeach
                                </optgroup>
                                <optgroup label="Short Training Programs">
                                  @foreach ($shortcourses as $shortcourse)
                                    <option value="{{ $shortcourse->course_name }}">{{ $shortcourse->course_name }}</option>
                                  @endforeach
                                </optgroup>
                              </select>
                            </div>
                            <div class="col-md-6">
                              <label for="major">Major</label>
                              <input id="major" type="text" class="form-control{{ $errors->has('major') ? ' is-invalid' : '' }}" name="major">
                              @if ($errors->has('major'))
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('major') }}</strong>
                                  </span>
                              @endif
                            </div>
                          </div>
                          <div class="form-group row">
                            <div class="col-md-6">
                              <label for="section">Section</label>
                              <input id="section" type="text" class="form-control{{ $errors->has('section') ? ' is-invalid' : '' }}" name="section" value="{{ old('section') }}" required autofocus>
                              @if ($errors->has('section'))
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('section') }}</strong>
                                  </span>
                              @endif
                            </div>
                            <div class="col-md-6">
                              <label for="student_status">Status</label>
                              <select name="student_status" class="form-control">
                                  <option value="Regular">Regular</option>
                                  <option value="Iregular">Iregular</option>
                              </select>
                            </div>
                          </div>
                          <div class="form-group row">
                            <div class="col-md-4">
                              <label for="firstname">Firstname</label>
                              <input id="firstname" type="text" class="form-control{{ $errors->has('firstname') ? ' is-invalid' : '' }}" name="firstname" value="{{ old('firstname') }}" required autofocus>
                              @if ($errors->has('firstname'))
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('firstname') }}</strong>
                                  </span>
                              @endif
                            </div>
                            <div class="col-md-4">
                              <label for="lastname">Lastname</label>
                              <input id="lastname" type="text" class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }}" name="lastname" value="{{ old('lastname') }}" required autofocus>
                              @if ($errors->has('lastname'))
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('lastname') }}</strong>
                                  </span>
                              @endif
                            </div>
                            <div class="col-md-4">
                              <label for="middlename">Middlename</label>
                              <input id="middlename" type="text" class="form-control{{ $errors->has('middlename') ? ' is-invalid' : '' }}" name="middlename" value="{{ old('middlename') }}" required autofocus>
                              @if ($errors->has('middlename'))
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('middlename') }}</strong>
                                  </span>
                              @endif
                            </div>
                          </div>



                          <div class="form-group">
                            <label for="studentid">Student ID number</label>
                            <input id="studentid" type="text" class="form-control{{ $errors->has('studentid') ? ' is-invalid' : '' }}" name="studentid" value="{{ old('studentid') }}" required autofocus>
                            @if ($errors->has('studentid'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('studentid') }}</strong>
                                </span>
                            @endif
                          </div>
                          <div class="form-group">
                            <label for="email">Email Address</label>
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                          </div>
                          <div class="form-group row">
                            <div class="col-md-6">
                              <label for="password">{{ __('Password') }}</label>
                              <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                              @if ($errors->has('password'))
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('password') }}</strong>
                                  </span>
                              @endif
                            </div>
                            <div class="col-md-6">
                              <label for="password-confirm">{{ __('Confirm Password') }}</label>
                              <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                          </div>
                          <div class="form-group row mb-0">
                              <div class="col">
                                  <button type="submit" class="btn btn-primary">
                                      {{ __('Register') }}
                                  </button>
                              </div>
                          </div>
                      {!! Form::close() !!}
                  </div>
              </div>
          </div>
      </div>
  </div>
@endsection
