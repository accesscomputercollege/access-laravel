@extends('multiauth::layouts.admin')

@section('title')
Access | Access Lagro
@endsection

@section('main-title')
Access Lagro
@endsection

@section('breadcrumb')
<ol class="breadcrumb float-right">
    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
    <li class="breadcrumb-item"><a href="/admin/lagro/students">Student Management</a></li>
    <li class="breadcrumb-item active">Grades</li>
</ol>
@endsection

@section('main-content')
  @include('multiauth::message')
  <div class="row">
      <div class="col">
        <div class="card mb-3">
          <div class="card-header">
            <span class="float-right">
              <a href="/admin/lagro/students/grades/{{ $student->id }}/create" class="btn btn-sm btn-success ml-3"><i class="fas fa-plus"></i> Encode new grades</a>
              <a href="/admin/lagro/students" class="btn btn-sm btn-danger ml-3">Back</a>
            </span>
            <h3>Lagro Campus</h3>
          </div>
          <div class="card-body">
            <div class="row student-info">
              <div class="col-md-6">
                <label>Student Name : </label>
                <span>{{ $student->lastname }}, {{ $student->firstname }} {{ $student->middlename }}</span>
              </div>
              <div class="col-md-6">
                <label>Course : </label>
                <span>{{ $student->course }}</span>
              </div>
            </div>
            <div class="form-group my-3">
              <select class="form-control" onchange="location = this.value;">
                <optgroup label="1st Year">
                  <option value="/admin/lagro/students/grades/{{ $student->id }}">First Year - First Semester</option>
                  <option value="/admin/lagro/students/grades2/{{ $student->id }}">First Year - Second Semester</option>
                </optgroup>
                <optgroup label="2nd Year">
                  <option value="/admin/lagro/students/grades3/{{ $student->id }}">Second Year - First Semester</option>
                  <option value="/admin/lagro/students/grades4/{{ $student->id }}">Second Year - Second Semester</option>
                </optgroup>
                <optgroup label="3rd Year">
                  <option value="/admin/lagro/students/grades5/{{ $student->id }}">Third Year - First Semester</option>
                  <option value="/admin/lagro/students/grades6/{{ $student->id }}">Third Year - Second Semester</option>
                </optgroup>
                <optgroup label="4th Year">
                  <option value="/admin/lagro/students/grades7/{{ $student->id }}">Fourth Year - First Semester</option>
                  <option value="/admin/lagro/students/grades8/{{ $student->id }}">Fourth Year - Second Semester</option>
                </optgroup>
              </select>
            </div>
            <div class="table-responsive">
            <table id="example1" class="table table-bordered table-hover display">
              <thead>
                <tr>
                  <th>Subjects Code</th>
                  <th>Subjects Name</th>
                  <th>Grades</th>
                  <th>Remarks</th>
                  <th>Instructor</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($grades as $grade)
                  <tr>
                    <td>{{ $grade->subject_code }}</td>
                    <td>{{ $grade->subject_name }}</td>
                    <td>{{ $grade->grade }}</td>
                    <td>{{ $grade->remark }}</td>
                    <td>{{ $grade->instructor }}</td>
                    <td>
                      <div class="text-center">
                        <!-- EDIT button -->
                        <button type="button" class="btn btn-primary btn-sm mb-2" data-toggle="modal" data-target="#edit{{ $grade->id }}">
                          <i class="far fa-edit"></i>
                        </button>
                        <!-- EDIT Modal -->
                        <div class="modal fade" id="edit{{ $grade->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              {!!Form::open(['action' => ['Admin\Student\Grades\StudentGrade1Controller@update', $grade->id], 'method' => 'POST'])!!}
                              <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Update Grades</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                <div class="form-group">
                                  <label>Subject Code</label>
                                  <input type="text" name="subject_code" class="form-control text-center" value="{{ $grade->subject_code }}">
                                </div>
                                <div class="form-group">
                                  <label>Subject Name</label>
                                  <input type="text" name="subject_name" class="form-control text-center" value="{{ $grade->subject_name }}">
                                </div>
                                <div class="form-group">
                                  <label>Grade</label>
                                  <input type="text" name="grade" class="form-control text-center" value="{{ $grade->grade }}">
                                </div>
                                <div class="form-group">
                                  <label>Remark</label>
                                  <input type="text" name="remark" class="form-control text-center" value="{{ $grade->remark }}">
                                </div>
                                <div class="form-group">
                                  <label>Instructor</label>
                                  <input type="text" name="instructor" class="form-control text-center" value="{{ $grade->instructor }}">
                                </div>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    {{Form::hidden('_method', 'PUT')}}
                                    {{Form::submit('Save changes', ['class' => 'btn btn-primary'])}}
                                {!!Form::close()!!}
                              </div>
                            </div>
                          </div>
                        </div>

                        <!-- DELETE button -->
                        <button type="button" class="btn btn-dark btn-sm mb-2" data-toggle="modal" data-target="#delete{{ $grade->id }}">
                          <i class="far fa-trash-alt"></i>
                        </button>
                        <!-- DELETE Modal -->
                        <div class="modal fade" id="delete{{ $grade->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Student Grades Management</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                Are you sure to remove this grades?
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                                {!!Form::open(['action' => ['Admin\Student\Grades\StudentGrade1Controller@destroy', $grade->id], 'method' => 'POST'])!!}
                                    {{Form::hidden('_method', 'DELETE')}}
                                    {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
                                {!!Form::close()!!}
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
            </div>
          </div>
        </div><!-- end card-->
      </div>
  </div>
@endsection
