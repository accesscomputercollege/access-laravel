@extends('multiauth::layouts.admin')

@section('title')
Access | Access Lagro
@endsection

@section('main-title')
Access Lagro
@endsection

@section('breadcrumb')
<ol class="breadcrumb float-right">
    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
    <li class="breadcrumb-item"><a href="/admin/lagro/students">Student Management</a></li>
    <li class="breadcrumb-item"><a href="/admin/lagro/students/grades/{{ $student->id }}">Grades</a></li>
    <li class="breadcrumb-item active">Encode</li>
</ol>
@endsection

@section('main-content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col">
            @if (session()->has('message'))
            <div class="alert alert-success">{{ session()->get('message') }}</div>
            @endif
            <div class="card">
                <div class="card-header"><a href="/admin/lagro/students/grades/{{ $student->id }}" class="btn btn-danger btn-sm pull-right">Back</a></div>
                <div class="card-body">
                  <div class="row student-info">
                    <div class="col-md-6">
                      <label>Student Name : </label>
                      <span>{{ $student->lastname }}, {{ $student->firstname }} {{ $student->middlename }}</span>
                    </div>
                    <div class="col-md-6">
                      <label>Course : </label>
                      <span>{{ $student->course }}</span>
                    </div>
                  </div>
                  <div class="form-group mt-3">
                    <select class="form-control" onchange="location = this.value;">
                      <optgroup label="1st Year">
                        <option value="/admin/lagro/students/grades/{{ $student->id }}/create">First Year - First Semester</option>
                        <option value="/admin/lagro/students/grades2/{{ $student->id }}/create">First Year - Second Semester</option>
                      </optgroup>
                      <optgroup label="2nd Year">
                        <option value="/admin/lagro/students/grades3/{{ $student->id }}/create">Second Year - First Semester</option>
                        <option value="/admin/lagro/students/grades4/{{ $student->id }}/create">Second Year - Second Semester</option>
                      </optgroup>
                      <optgroup label="3rd Year">
                        <option value="/admin/lagro/students/grades5/{{ $student->id }}/create">Third Year - First Semester</option>
                        <option value="/admin/lagro/students/grades6/{{ $student->id }}/create">Third Year - Second Semester</option>
                      </optgroup>
                      <optgroup label="4th Year">
                        <option value="/admin/lagro/students/grades7/{{ $student->id }}/create">Fourth Year - First Semester</option>
                        <option value="/admin/lagro/students/grades8/{{ $student->id }}/create">Fourth Year - Second Semester</option>
                      </optgroup>
                    </select>
                  </div>
                  {!! Form::open(['action' => 'Admin\Student\Grades\StudentGrade1Controller@store', 'method' => 'POST', 'files' => true]) !!}
                  <section>
                      <div class="panel panel-header">
                          <div class="row">
                              <div class="col">
                                  <div class="form-group">
                                      <input type="text" name="lagro_student_id" class="form-control d-none" value="{{ $student->id }}">
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="panel panel-footer">
                          <table class="table table-bordered">
                              <thead>
                                  <tr>
                                      <th>Subjects Code</th>
                                      <th>Subjects Name</th>
                                      <th>Grades</th>
                                      <th>Remarks</th>
                                      <th>Instructor</th>
                                      <th class="text-center"><a href="#" class="addRow"><i class="fas fa-plus"></i></a></th>
                                  </tr>
                              </thead>
                              <tbody>
                                  <tr>
                                      <td><input type="text" name="subject_code[]" class="form-control" required=""></td>
                                      <td><input type="text" name="subject_name[]" class="form-control"></td>
                                      <td><input type="text" name="grade[]" class="form-control" required=""></td>
                                      <td><input type="text" name="remark[]" class="form-control"></td>
                                      <td><input type="text" name="instructor[]" class="form-control"></td>
                                      <td class="text-center"><a href="#" class="btn btn-danger remove"><i class="fas fa-times"></i></a></td>
                                  </tr>
                              </tbody>
                              <tfoot>
                                  <tr>
                                      <td class="border-0"></td>
                                      <td class="border-0"></td>
                                      <td class="border-0"></td>
                                      <td class="border-0"></td>
                                      <td class="border-0"></td>
                                      <td class="border-0"><input type="submit" name="" value="Submit" class="btn btn-success"></td>
                                  </tr>
                              </tfoot>
                          </table>
                      </div>
                  </section>
                  {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
