@extends('multiauth::layouts.admin')

@section('title')
  Access | Access Lagro
@endsection

@section('main-title')
  Access Lagro
@endsection

@section('breadcrumb')
  <ol class="breadcrumb float-right">
      <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
      <li class="breadcrumb-item"><a href="/admin/lagro/students">Student Management</a></li>
      <li class="breadcrumb-item active">Edit</li>
  </ol>
@endsection

@section('main-content')
<div class="container">
  <div class="alert alert-warning alert-dismissible fade show" role="alert">
    Complete all student information
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  {!!Form::open(['action' => ['Admin\Student\LagroController@update', $student->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
    <div class="accordion" id="accordionExample">
      <div class="row justify-content-center">
          <div class="col-md-3">
            <div class="avatar mb-3">
              <img class="w-100 img-thumbnail" src="{{ $student->photo ? $student->photo->file : 'http://placehold.it/100x100' }}">
              <input type="file"name="photo_id" class="mt-1">
            </div>
            <div class="list-group mb-3">
              <button type="button" class="list-group-item list-group-item-action" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                Student Account
              </button>
              <button type="button" class="list-group-item list-group-item-action" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                Student Information
              </button>
              <a href="/admin/lagro/students/grades/{{ $student->id }}" class="list-group-item list-group-item-action">
                Student Grades
              </a>
            </div>
            <div class="form-group mb-0">
              {{Form::hidden('_method','PUT')}}
              <button type="submit" class="btn btn-block btn-primary">Save changes</button>
            </div>
          </div>
          <div class="col-md-9">
            @if (session()->has('message'))
                <div class="alert alert-success">{{ session()->get('message') }}</div>
            @endif
              <div class="card collapse show" id="collapseOne" aria-labelledby="headingOne" data-parent="#accordionExample">
                {{--

                    Student Account

                 --}}
                <div class="card-header h5">Student Account</div>
                <div class="card-body">
                    <div class="form-group row">
                      <div class="col-md-6">
                        <label>Courses</label>
                        <select name="course" class="form-control">
                          <option value="{{ $student->course ? $student->course : '' }}" selected>{{ $student->course ? $student->course : 'Select Course' }}</option>
                          <optgroup label="Senior High School">
                            @foreach ($seniorhigh as $senior)
                              <option value="{{ $senior->course_name }}">{{ $senior->course_name }}</option>
                            @endforeach
                          </optgroup>
                          <optgroup label="Vocational Programs">
                            @foreach ($vocationals as $voc)
                              <option value="{{ $voc->course_name }}">{{ $voc->course_name }}</option>
                            @endforeach
                          </optgroup>
                          <optgroup label="Collegiate Programs">
                            @foreach ($collegiate as $college)
                              <option value="{{ $college->course_name }}">{{ $college->course_name }}</option>
                            @endforeach
                          </optgroup>
                          <optgroup label="Short Training Programs">
                            @foreach ($shortcourses as $shortcourse)
                              <option value="{{ $shortcourse->course_name }}">{{ $shortcourse->course_name }}</option>
                            @endforeach
                          </optgroup>
                        </select>
                      </div>
                      <div class="col-md-6">
                        <label for="major">Major</label>
                        <input id="major" type="text" class="form-control{{ $errors->has('major') ? ' is-invalid' : '' }}" name="major" value="{{ $student->major }}">
                        @if ($errors->has('major'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('major') }}</strong>
                            </span>
                        @endif
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-md-6">
                        <label for="section">Section</label>
                        <input id="section" type="text" class="form-control{{ $errors->has('section') ? ' is-invalid' : '' }}" name="section" value="{{ $student->section }}">
                        @if ($errors->has('section'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('section') }}</strong>
                            </span>
                        @endif
                      </div>
                      <div class="col-md-6">
                        <label for="student_status">Status</label>
                        <select name="student_status" class="form-control">
                            <option value="{{ $student->student_status }}" selected>{{ $student->student_status }}</option>
                            <option value="Regular">Regular</option>
                            <option value="Iregular">Iregular</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label for="firstname">Firstname</label>
                            <input id="firstname" type="text" class="form-control text-capitalize{{ $errors->has('firstname') ? ' is-invalid' : '' }}" name="firstname" value="{{ $student->firstname }}" required autofocus>
                            @if ($errors->has('firstname'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('firstname') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="col-md-4">
                            <label for="lastname">Lastname</label>
                            <input id="lastname" type="text" class="form-control text-capitalize{{ $errors->has('lastname') ? ' is-invalid' : '' }}" name="lastname" value="{{ $student->lastname }}" required autofocus>
                            @if ($errors->has('lastname'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('lastname') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="col-md-4">
                            <label for="middlename">Middlename</label>
                            <input id="middlename" type="text" class="form-control text-capitalize{{ $errors->has('middlename') ? ' is-invalid' : '' }}" name="middlename" value="{{ $student->middlename }}" required autofocus>
                            @if ($errors->has('middlename'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('middlename') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="studentid">Student ID number</label>
                        <input id="studentid" type="text" class="form-control text-uppercase{{ $errors->has('studentid') ? ' is-invalid' : '' }}" name="studentid" value="{{ $student->studentid }}" required autofocus>
                        @if ($errors->has('studentid'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('studentid') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="email">Email Address</label>
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $student->email }}" required autofocus>
                        @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="password">{{ __('Password') }}</label>
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" value="{{ $student->password }}" required>
                            @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="col-md-6">
                            <label for="password-confirm">{{ __('Confirm Password') }}</label>
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" value="{{ $student->password }}" required>
                        </div>
                    </div>
                </div>
              </div>
              {{--

                  Student Information

               --}}
              <div class="collapse" id="collapseTwo" aria-labelledby="headingTwo" data-parent="#accordionExample">
                <div class="card">
                  <div class="card-header h5">Student's Information</div>
                  <div class="card-body">
                    <div class="form-group">
                      <label>Campuses</label>
                      <input type="text" class="form-control form-control-sm text-capitalize" value="Access Lagro Campus" disabled>
                    </div>

                    <div class="form-group row">
                      <div class="col-md-4">
                        <label>Firstname</label>
                        <input type="text" class="form-control form-control-sm text-capitalize" name="firstname" value="{{ $student->firstname }}" disabled>
                      </div>
                      <div class="col-md-4">
                        <label>Lastname</label>
                        <input type="text" class="form-control form-control-sm text-capitalize" name="lastname" value="{{ $student->lastname }}" disabled>
                      </div>
                      <div class="col-md-4">
                        <label>Middlename</label>
                        <input type="text" class="form-control form-control-sm text-capitalize" name="middlename" value="{{ $student->middlename }}" disabled>
                      </div>
                    </div>

                    <div class="form-group row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="exampleFormControlSelect1">Gender</label>
                          <select name="gender" class="form-control form-control-sm" id="exampleFormControlSelect1">
                            <option value="male">Male</option>
                            <option value="female">Female</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="exampleFormControlSelect1">Status</label>
                          <select name="status" class="form-control form-control-sm" id="exampleFormControlSelect1">
                            <option value="single">Single</option>
                            <option value="married">Married</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>Citizenship</label>
                          <input type="text" class="form-control form-control-sm" name="citizenship" value="{{ $student->citizenship }}">
                        </div>
                      </div>
                    </div>

                    <div class="form-group row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>Birthplace</label>
                          <input type="text" class="form-control form-control-sm" name="birthplace" value="{{ $student->birthplace }}">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>Religion</label>
                          <input type="text" class="form-control form-control-sm" name="religion" value="{{ $student->religion }}">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>Date of Birth</label>
                          <input type="date" class="form-control form-control-sm" name="date_of_birth" value="{{ $student->date_of_birth }}">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="card mt-3">
                  <div class="card-header h5">Current Address</div>
                  <div class="card-body">
                    <div class="form-group row">
                      <div class="col-md-3">
                        <label>Street No. / Unit No.</label>
                        <input type="text" class="form-control form-control-sm" name="street_number" value="{{ $student->street_number }}">
                      </div>
                      <div class="col-md-9">
                        <label>Street</label>
                        <input type="text" class="form-control form-control-sm" name="street" value="{{ $student->street }}">
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-md-6">
                        <label>Subdivision / Village / Building</label>
                        <input type="text" class="form-control form-control-sm" name="subdivision_village_building" value="{{ $student->subdivision_village_building }}">
                      </div>
                      <div class="col-md-6">
                        <label>Barangay</label>
                        <input type="text" class="form-control form-control-sm" name="barangay" value="{{ $student->barangay }}">
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-md-5">
                        <div class="form-group">
                          <label>City / Municipality</label>
                          <input type="text" class="form-control form-control-sm" name="city" value="{{ $student->city }}">
                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="form-group">
                          <label>Province</label>
                          <input type="text" class="form-control form-control-sm" name="province" value="{{ $student->province }}">
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group">
                          <label>Zip Code</label>
                          <input type="text" class="form-control form-control-sm" name="zip_code" value="{{ $student->zip_code }}">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="card mt-3">
                  <div class="card-header h5">Contact Details</div>
                  <div class="card-body">
                    <div class="form-group row">
                      <div class="col-md-6">
                        <label>Telephone No.</label>
                        <input type="text" class="form-control form-control-sm" name="telephone" value="{{ $student->telephone }}">
                      </div>
                      <div class="col-md-6">
                        <label>Mobile No.</label>
                        <input type="text" class="form-control form-control-sm" name="mobile" value="{{ $student->mobile }}">
                      </div>
                    </div>
                  </div>
                </div>

                <div class="card mt-3">
                  <div class="card-header h5">Current or Last School Attended</div>
                  <div class="card-body">
                    <div class="form-group row">
                      <div class="col-md-6">
                        <label for="exampleFormControlSelect1">School Type</label>
                        <select name="school_type" class="form-control form-control-sm" id="exampleFormControlSelect1">
                          <option value="{{ $student->school_type ? $student->school_type : 'Select type' }}" selected>{{ $student->school_type ? $student->school_type : 'Select Course' }}</option>
                          <option value="high-school">High School</option>
                          <option value="junior-high">Junior High School</option>
                          <option value="senior-high">Senior High School</option>
                          <option value="college">College</option>
                        </select>
                      </div>
                      <div class="col-md-6">
                        <label>Name of School</label>
                        <input type="text" class="form-control form-control-sm" name="school_name" value="{{ $student->school_name }}">
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-md-3">
                        <label>Date of Graduation</label>
                        <input type="date" class="form-control form-control-sm" name="grad_date" value="{{ $student->grad_date }}">
                      </div>
                      <div class="col-md-3">
                        <label for="exampleFormControlSelect1">School Year</label>
                        <select name="school_year" class="form-control form-control-sm" id="exampleFormControlSelect1">
                          <option value="{{ $student->school_year ? $student->school_year : 'Select School Year' }}" selected>{{ $student->school_year ? $student->school_year : 'Select School Year' }}</option>
                          <option value="2018-2019">2018-2019</option>
                          <option value="2017-2018">2017-2018</option>
                          <option value="2016-2017">2016-2017</option>
                          <option value="2015-2016">2015-2016</option>
                          <option value="2014-2015">2014-2015</option>
                          <option value="2013-2014">2013-2014</option>
                          <option value="2012-2013">2012-2013</option>
                          <option value="2011-2012">2011-2012</option>
                          <option value="2010-2011">2010-2011</option>
                          <option value="2009-2010">2009-2010</option>
                          <option value="2008-2009">2008-2009</option>
                          <option value="2007-2008">2007-2008</option>
                          <option value="2006-2007">2006-2007</option>
                          <option value="2005-2006">2005-2006</option>
                          <option value="2004-2005">2004-2005</option>
                          <option value="2003-2004">2003-2004</option>
                          <option value="2002-2003">2002-2003</option>
                          <option value="2001-2002">2001-2002</option>
                        </select>
                      </div>
                      <div class="col-md-3">
                        <label for="exampleFormControlSelect1">Year / Grade</label>
                        <select name="year_grade" class="form-control form-control-sm" id="exampleFormControlSelect1">
                          <option value="{{ $student->year_grade ? $student->year_grade : 'Select year/grade' }}" selected>{{ $student->year_grade ? $student->year_grade : 'Select year/grade' }}</option>
                          <option value="grade-10">Grade 10</option>
                          <option value="grade-9">Grade 9</option>
                          <option value="grade-8">Grade 8</option>
                          <option value="first_year">First Year</option>
                          <option value="second_year">Second Year</option>
                          <option value="third_year">Third Year</option>
                          <option value="fourth_year">Fourth Year</option>
                        </select>
                      </div>
                      <div class="col-md-3">
                        <label for="exampleFormControlSelect1">Term</label>
                        <select name="term" class="form-control form-control-sm" id="exampleFormControlSelect1">
                          <option value="{{ $student->term ? $student->term : 'Select School Term' }}" selected>{{ $student->term ? $student->term : 'Select year/grade' }}</option>
                          <option value="first_term">First Term</option>
                          <option value="second_term">Second Term</option>
                          <option value="third_term">Third Term</option>
                          <option value="fourth_term">Fourth Term</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="card mt-3">
                  <div class="card-header h5">Parents / Guardian's Information</div>
                  <div class="card-body">
                    <h6>Father's Information</h6>
                    <div class="form-group row">
                      <div class="col-md-4">
                        <label>First Name</label>
                        <input type="text" class="form-control form-control-sm" name="f_firstname" value="{{ $student->f_firstname }}">
                      </div>
                      <div class="col-md-4">
                        <label>Last Name</label>
                        <input type="text" class="form-control form-control-sm" name="f_lastname" value="{{ $student->f_lastname }}">
                      </div>
                      <div class="col-md-2">
                        <label>Middle Initial</label>
                        <input type="text" class="form-control form-control-sm" name="f_initial" value="{{ $student->f_initial }}">
                      </div>
                      <div class="col-md-2">
                        <label>Suffix</label>
                        <input type="text" class="form-control form-control-sm" name="f_suffix" value="{{ $student->f_suffix }}">
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-md-4">
                        <label>Mobile Number</label>
                        <input type="text" class="form-control form-control-sm" name="f_mobile" value="{{ $student->f_mobile }}">
                      </div>
                      <div class="col-md-4">
                        <label>Email</label>
                        <input type="text" class="form-control form-control-sm" name="f_email" value="{{ $student->f_email }}">
                      </div>
                      <div class="col-md-4">
                        <label>Occupation</label>
                        <input type="text" class="form-control form-control-sm" name="f_occupation" value="{{ $student->f_occupation }}">
                      </div>
                    </div>

                    <hr class="my-5">

                    <h6>Mother's Information</h6>
                    <div class="form-group row">
                      <div class="col-md-4">
                        <label>First Name</label>
                        <input type="text" class="form-control form-control-sm" name="m_firstname" value="{{ $student->m_firstname }}">
                      </div>
                      <div class="col-md-4">
                        <label>Last Name</label>
                        <input type="text" class="form-control form-control-sm" name="m_lastname" value="{{ $student->m_lastname }}">
                      </div>
                      <div class="col-md-2">
                        <label>Middle Initial</label>
                        <input type="text" class="form-control form-control-sm" name="m_initial" value="{{ $student->m_initial }}">
                      </div>
                      <div class="col-md-2">
                        <label>Suffix</label>
                        <input type="text" class="form-control form-control-sm" name="m_suffix" value="{{ $student->m_suffix }}">
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-md-4">
                        <label>Mobile Number</label>
                        <input type="text" class="form-control form-control-sm" name="m_mobile" value="{{ $student->m_mobile }}">
                      </div>
                      <div class="col-md-4">
                        <label>Email</label>
                        <input type="text" class="form-control form-control-sm" name="m_email" value="{{ $student->m_email }}">
                      </div>
                      <div class="col-md-4">
                        <label>Occupation</label>
                        <input type="text" class="form-control form-control-sm" name="m_occupation" value="{{ $student->m_occupation }}">
                      </div>
                    </div>

                    <hr class="my-5">

                    <h6>Guardian's Information</h6>
                    <div class="form-group row">
                      <div class="col-md-4">
                        <label>First Name</label>
                        <input type="text" class="form-control form-control-sm" name="g_firstname" value="{{ $student->g_firstname }}">
                      </div>
                      <div class="col-md-4">
                        <label>Last Name</label>
                        <input type="text" class="form-control form-control-sm" name="g_lastname" value="{{ $student->g_lastname }}">
                      </div>
                      <div class="col-md-2">
                        <label>Middle Initial</label>
                        <input type="text" class="form-control form-control-sm" name="g_initial" value="{{ $student->g_initial }}">
                      </div>
                      <div class="col-md-2">
                        <label>Suffix</label>
                        <input type="text" class="form-control form-control-sm" name="g_suffix" value="{{ $student->g_suffix }}">
                      </div>
                    </div>

                    <div class="form-group row">
                      <div class="col-md-3">
                        <label>Mobile Number</label>
                        <input type="text" class="form-control form-control-sm" name="g_mobile" value="{{ $student->g_mobile }}">
                      </div>
                      <div class="col-md-3">
                        <label>Email</label>
                        <input type="text" class="form-control form-control-sm" name="g_email" value="{{ $student->g_email }}">
                      </div>
                      <div class="col-md-3">
                        <label>Occupation</label>
                        <input type="text" class="form-control form-control-sm" name="g_occupation" value="{{ $student->g_occupation }}">
                      </div>
                      <div class="col-md-3">
                        <label>Relationship</label>
                        <input type="text" class="form-control form-control-sm" name="g_relationship" value="{{ $student->g_relationship }}">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          </div>
      </div>
    </div>
  {!! Form::close() !!}
</div>
@endsection
