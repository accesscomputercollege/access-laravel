<nav class="navbar navbar-expand-lg navbar-light bg-primary">
    <div class="container-fluid">
      <div class="d-none d-sm-block">
        <h5 class="">Access Computer</h5>
      </div>

        <button type="button" id="sidebarCollapse" class="btn btn-primary">
          <i class="fas fa-bars" style="font-size: 30px;"></i>
        </button>

        <ul class="navbar-nav ml-auto">
            <!-- Authentication Links -->
            @guest('admin')
            <li class="nav-item">
                <a class="nav-link" href="{{route('admin.login')}}">{{ ucfirst(config('multiauth.prefix')) }} Login</a>
            </li>
            @else
            <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false" v-pre>
                        {{ auth('admin')->user()->name }} <span class="caret"></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="/admin/sign-out" onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                            Sign-out
                        </a>
                    <form id="logout-form" action="{{ route('admin.signout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
            @endguest
        </ul>
    </div>
</nav>
