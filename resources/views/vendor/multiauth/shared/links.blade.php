<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<!-- Styles -->
<link href="{{ asset('template/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('template/css/style.css') }}" rel="stylesheet">
<link href="{{ asset('template/plugins/select2/css/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('template/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" />
<link rel="icon" href="../../../../../../../../images/access-logo-icon.png">

{{-- ---------------------------------------------------
    FOR TEMPORARY UP THE SITE
----------------------------------------------------- --}}
{{-- <link rel="stylesheet" href="../template/css/style.css">
<link rel="stylesheet" href="../template/css/bootstrap.min.css">
<link rel="stylesheet" href="../template/font-awesome/css/font-awesome.min.css"> --}}
