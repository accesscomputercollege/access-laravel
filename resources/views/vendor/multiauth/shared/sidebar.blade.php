<!-- Left Sidebar -->
<div class="left main-sidebar">
  <div class="sidebar-inner leftscroll">
    <div id="sidebar-menu">
      <ul>
          <li class="submenu">
              <a class="active" href="{{ route('admin.dashboard') }}"><i class="fa fa-fw fa-dashboard"></i><span> Dashboard </span> </a>
          </li>
          @admin('super', 'administrator')
          <li class="submenu">
              <a href="{{ route('admin.show') }}"><i class="fa fa-fw fa-users"></i><span> Admin Management </span> </a>
          </li>
          <li class="submenu">
              <a href="{{ route('admin.roles') }}"><i class="fas fa-id-card-alt"></i><span> Role Management </span> </a>
          </li>
          <li class="submenu">
              <a href="/admin/events"><i class="far fa-calendar-check"></i><span> Event Management </span> </a>
          </li>
          <li class="submenu">
              <a href="/admin/campuses"><i class="fas fa-school"></i><span> Campus Management </span> </a>
          </li>
          <li class="submenu">
              <a href="#"><i class="fas fa-book"></i> <span>Programs </span> <span class="menu-arrow"></span></a>
              <ul class="list-unstyled">
                <li><a href="/admin/vocational">Vocational Programs</a></li>
                <li><a href="/admin/collegiate">Collegiate Programs</a></li>
                <li><a href="/admin/shortcourse">Short Training Programs</a></li>
                <li><a href="/admin/senior_high">Senior High School</a></li>
              </ul>
          </li>
          @endadmin
          <li class="submenu">
            <a href="#"><i class="fas fa-user-graduate"></i><span> Student Management</span><span class="menu-arrow"></span></a>
            <ul class="list-unstyled">
              @admin('lagro')
                <li><a href="/admin/lagro/students">Access Lagro</a></li>
              @endadmin
              @admin('zabarte')
                <li><a href="#">Access Zabarte</a></li>
              @endadmin
              @admin('camarin')
                <li><a href="#">Access Camarin</a></li>
              @endadmin
              @admin('recto')
                <li><a href="#">Access Recto</a></li>
              @endadmin
              @admin('cubao')
                <li><a href="#">Access Cubao</a></li>
              @endadmin
              @admin('balagtas')
                <li><a href="#">Access Balagtas</a></li>
              @endadmin
              @admin('carmen')
                <li><a href="#">Access Carmen Rosales</a></li>
              @endadmin
              @admin('pasig2')
                <li><a href="#">Access Pasig 2</a></li>
              @endadmin
              @admin('marilao')
                <li><a href="#">Access Marilao</a></li>
              @endadmin
              @admin('monumento')
                <li><a href="#">Access Monumento</a></li>
              @endadmin
              @admin('pasig')
                <li><a href="#">Access Pasig</a></li>
              @endadmin
              @admin('villasis')
                <li><a href="#">Access Villasis</a></li>
              @endadmin
              @admin('marikina')
                <li><a href="#">Access Marikina</a></li>
              @endadmin
              @admin('meycauayan')
                <li><a href="#">Access Meycauayan</a></li>
              @endadmin
            </ul>
          </li>
          <li class="submenu">
              <a href="/admin/news"><i class="far fa-newspaper"></i><span> School News</span> </a>
          </li>
          <li class="submenu">
              <a href="{{ route('admin.announcements') }}"><i class="fas fa-bullhorn"></i><span> Announcements</span> </a>
          </li>
        </ul>
      <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
<!-- End Sidebar -->
