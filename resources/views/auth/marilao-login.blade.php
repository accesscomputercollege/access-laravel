@extends('layouts.student-marilao')

@section('title')
  Access Computer College | Marilao Login
@endsection

@section('content')
  <div class="login">
      <!-- Login -->
      <div class="login__block active" id="l-login">
          <div class="login__block__header">
              <i class="zmdi zmdi-account-circle"></i>
              Access Marilao Campus
          </div>

          <div class="login__block__body">
            <form method="POST" action="{{ route('marilao.login.submit') }}">
              @csrf
              <div class="form-group form-group--float form-group--centered">
                  <input type="text" name="studentid" class="form-control {{ $errors->has('studentid') ? ' is-invalid' : '' }}" value="{{ old('studentid') }}" required>
                  <label>Student ID</label>
                  <i class="form-group__bar"></i>
              </div>

              <div class="form-group form-group--float form-group--centered">
                  <input type="password" name="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" required>
                  <label>Password</label>
                  <i class="form-group__bar"></i>
              </div>

              <button type="submit" class="btn btn--icon login__block__btn"><i class="zmdi zmdi-long-arrow-right"></i></button>
            </form>
          </div>
      </div>
  </div>
@endsection
