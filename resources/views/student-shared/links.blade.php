<!-- Vendor styles -->
<link rel="icon" href="../../images/access-logo-icon.png">
<link rel="stylesheet" href="../student_theme/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css">
<link rel="stylesheet" href="../student_theme/vendors/bower_components/animate.css/animate.min.css">
<link rel="stylesheet" href="../student_theme/vendors/bower_components/jquery.scrollbar/jquery.scrollbar.css">
<link rel="stylesheet" href="../student_theme/vendors/bower_components/fullcalendar/dist/fullcalendar.min.css">

<!-- App styles -->
<link rel="stylesheet" href="../student_theme/css/app.min.css">
