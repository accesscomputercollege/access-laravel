<!-- Javascript -->
<!-- Vendors -->
<script src="../student_theme/vendors/bower_components/jquery/dist/jquery.min.js"></script>
<script src="../student_theme/vendors/bower_components/tether/dist/js/tether.min.js"></script>
<script src="../student_theme/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../student_theme/vendors/bower_components/Waves/dist/waves.min.js"></script>
<script src="../student_theme/vendors/bower_components/jquery.scrollbar/jquery.scrollbar.min.js"></script>
<script src="../student_theme/vendors/bower_components/jquery-scrollLock/jquery-scrollLock.min.js"></script>
<script src="../student_theme/vendors/bower_components/Waves/dist/waves.min.js"></script>

<script src="../student_theme/vendors/bower_components/flot/jquery.flot.js"></script>
<script src="../student_theme/vendors/bower_components/flot/jquery.flot.resize.js"></script>
<script src="../student_theme/vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
<script src="../student_theme/vendors/bower_components/jqvmap/dist/jquery.vmap.min.js"></script>
<script src="../student_theme/vendors/bower_components/jqvmap/dist/maps/jquery.vmap.world.js"></script>
<script src="../student_theme/vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
<script src="../student_theme/vendors/bower_components/salvattore/dist/salvattore.min.js"></script>
<script src="../student_theme/vendors/jquery.sparkline/jquery.sparkline.min.js"></script>
<script src="../student_theme/vendors/bower_components/moment/min/moment.min.js"></script>
<script src="../student_theme/vendors/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>

<!-- Charts and maps-->
<script src="../student_theme/demo/js/flot-charts/curved-line.js"></script>
<script src="../student_theme/demo/js/flot-charts/line.js"></script>
<script src="../student_theme/demo/js/flot-charts/chart-tooltips.js"></script>
<script src="../student_theme/demo/js/other-charts.js"></script>
<script src="../student_theme/demo/js/jqvmap.js"></script>

<!-- App functions and actions -->
<script src="../student_theme/js/app.min.js"></script>
