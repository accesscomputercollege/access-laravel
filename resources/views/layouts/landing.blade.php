<!DOCTYPE html>
<html lang="en">

  <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>@yield('title')</title>
      @include('shared.links')
      <!-- Material Design for Bootstrap CSS -->
      <link rel="stylesheet" href="https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css" integrity="sha384-wXznGJNEXNG1NFsbm0ugrLFMQPWswR3lds2VeinahP8N0zJw9VWSopbjv2x7WCvX" crossorigin="anonymous">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
      @yield('css')
  </head>

  <body class="bg-light" onload="myFunction()">

    @include('shared.pageloader')

    <div style="display:none;" id="myDiv" class="animate-bottom">
      <div class="bmd-layout-container bmd-drawer-f-l bmd-drawer-overlay">
        @include('shared.navbar-drawer')
        <main class="bmd-layout-content">
            @yield('content')
        </main>
        @include('shared.footer')
      </div>
    </div>

    @include('shared.scripts')
    @yield('js')

    <script>
      var myVar;

      function myFunction() {
        myVar = setTimeout(showPage, 500);
      }

      function showPage() {
        document.getElementById("loader").style.display = "none";
        document.getElementById("myDiv").style.display = "block";
      }
    </script>
  </body>
</html>
{{-- <nav class="nav float-right">
    @if (Route::has('login'))
      @auth
        <a class="nav-link" href="{{ url('/home') }}">Home</a>
@else
<a class="nav-link" href="{{ route('login') }}">Login</a>

@if (Route::has('register'))
<a class="nav-link" href="{{ route('register') }}">Register</a>
@endif
@endauth
@endif
</nav> --}}
