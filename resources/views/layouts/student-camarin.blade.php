<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <title>
        Access | Camarin Student Login
      </title>
      @include('student-shared.links')
  </head>
  <body data-ma-theme="lime">
    <div class="">
      @yield('content')
    </div>
    @guest
      @else
        <main class="main">
            @include('student-shared.page-loader')
            @include('student-shared.header')
            <aside class="sidebar">
                <div class="scrollbar-inner">
                    <div class="user">
                        <div class="user__info" data-toggle="dropdown">
                            <img class="user__img" src="../../student_theme/demo/img/profile-pics/4.jpg" alt="">
                            <div>
                                <div class="user__name">{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</div>
                                <div class="user__email">Student ID • {{ Auth::user()->studentid }}</div>
                            </div>
                        </div>

                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="{{ route('camarin.student.profile') }}">View Profile</a>
                            <a class="dropdown-item" href="{{ route('camarin.changePasswordForm') }}">Change Password</a>
                            <a class="dropdown-item" href="{{ route('camarin.student.logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('camarin.student.logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </div>
                    <ul class="navigation">
                        <li class="navigation__active"><a href="/camarin/dashboard"><i class="zmdi zmdi-home"></i> Home</a></li>
                        <li><a href="{{ route('camarin.student.personal-info') }}"><i class="zmdi zmdi-account"></i> Personal Information</a></li>
                        <li><a href="#"><i class="zmdi zmdi-assignment-check"></i> Check List</a></li>
                        <li><a href="#"><i class="zmdi zmdi-assignment"></i> Class Assignment</a></li>
                        <li><a href="#"><i class="zmdi zmdi-widgets"></i> Awards/Achievements</a></li>
                        <li><a href="#"><i class="zmdi zmdi-group-work"></i> Field Work Profile/OJT</a></li>
                        </li>
                    </ul>
                </div>
            </aside>

            <section class="content">
                <header class="content__title">
                    <h1>@yield('title')</h1>
                    <div class="actions">
                        <div class="dropdown actions__item">
                            <i data-toggle="dropdown" class="zmdi zmdi-more-vert"></i>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a href="" class="dropdown-item">Refresh</a>
                            </div>
                        </div>
                    </div>
                </header>
                @yield('main-content')
                @include('student-shared.footer')
            </section>
        </main>
    @endguest
    @include('student-shared.scripts')
  </body>
</html>
