<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <title>
        Access | Marilao Student Automate
      </title>
      @include('student-shared.links')
  </head>
  <body data-ma-theme="blue">
    <div class="student-login">
      @yield('content')
    </div>
    @guest
      @else
        <main class="main">
            @include('student-shared.page-loader')
            @include('student-shared.header')
            <aside class="sidebar">
                <div class="scrollbar-inner">
                    <div class="user">
                        <div class="user__info" data-toggle="dropdown">
                            <img class="user__img" src="{{ Auth::user()->photo ? Auth::user()->photo->file : 'http://placehold.it/100x100' }}">
                            <div>
                                <div class="user__name">{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</div>
                                <div class="user__email">Student ID • {{ Auth::user()->studentid }}</div>
                            </div>
                        </div>

                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="/marilao/my-profile">View Profile</a>
                            <a class="dropdown-item" href="/marilao/change-password">Change Password</a>
                            <a class="dropdown-item" href="/marilao/logout"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="/marilao/logout" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </div>
                    <ul class="navigation">
                        <li class="navigation__active"><a href="/marilao/dashboard"><i class="zmdi zmdi-home"></i> Home</a></li>
                        <li><a href="/marilao/personal-information"><i class="zmdi zmdi-account"></i> Personal Information</a></li>
                        <li class="navigation__sub">
                            <a href=""><i class="zmdi zmdi-collection-text"></i> Academic Performance</a>

                            <ul class="navigation__sub">
                                <li><a href="/marilao/check-list">Check List</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </aside>

            <section class="content">
                <header class="content__title">
                    <h1>@yield('title')</h1>
                    <div class="actions">
                        <div class="dropdown actions__item">
                            <i data-toggle="dropdown" class="zmdi zmdi-more-vert"></i>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a href="" class="dropdown-item">Refresh</a>
                            </div>
                        </div>
                    </div>
                </header>
                @yield('main-content')
                @include('student-shared.footer')
            </section>
        </main>
    @endguest
    @include('student-shared.scripts')
  </body>
</html>
