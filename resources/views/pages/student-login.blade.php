<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Access | Student Login</title>
    <link rel="icon" href="../../images/access-logo-icon.png">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  </head>
  <body class="bg-light">
    <div class="container d-flex justify-content-center align-items-center" style="height: 80vh;">
      <div class="form-group text-center">
        <img src="../../images/access-logo-icon-white.png" width="100%" style="max-width: 100px;">
        <select class="form-control" onchange="location = this.value;">
          <option value="#">Select Campus to login</option>
          <option value="/camarin/login">Access Camarin</option>
          <option value="/recto/login">Access Recto</option>
          <option value="/lagro/login">Access Lagro</option>
          <option value="/cubao/login">Access Cubao</option>
          <option value="/marilao/login">Access Marilao</option>
          <option value="/balagtas/login">Access Balagtas</option>
          <option value="/meycauayan/login">Access Meycauayan</option>
          <option value="/marikina/login">Access Marikina</option>
          <option value="/pasig/login">Access Pasig</option>
          <option value="/pasig2/login">Access Pasig2</option>
          <option value="/zabarte/login">Access Zabarte</option>
          <option value="/villasis/login">Access Villasis</option>
          <option value="/carmen/login">Access Carmen Rosales</option>
          <option value="/monumento/login">Access Monumento</option>
        </select>
      </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  </body>
</html>
