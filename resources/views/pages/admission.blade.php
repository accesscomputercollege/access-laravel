@extends('layouts.landing')

@section('title')
  Access | Admission
@endsection

@section('content')
  <div id="header-admission">
    <div class="container">
      <div class="text-center">
        <h1 class="text-white">Admission</h1>
        <p>A Bridge We Can Help You Cross!</p>
      </div>
    </div>
  </div>
  <div class="container mt-5 pt-5">
    <div class="row">
      <div class="col-lg-9 mb-4">
        <h2>ADMISSION REQUIRMENTS</h2>
        <hr class="bg-danger accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 100px;">
        <h5 class="text-navyblue">Entrance Qualification:</h5>
        <p class="text-navyblue">Must be at least a high school graduate (No entrance examination)</p>
        <h5 class="py-4">Credentials to be submitted:</h5>
        <h6>For Freshmen Students</h6>
        <hr>
        <ul class="list-unstyled ml-4">
          <li class="list-item pb-2"><i class="fas fa-angle-double-right"></i> High school card or Form 138</li>
          <li class="list-item pb-2"><i class="fas fa-angle-double-right"></i> Form 137-A (High School Permanent Record)</li>
          <li class="list-item pb-2"><i class="fas fa-angle-double-right"></i> Good Moral Character</li>
          <li class="list-item pb-2"><i class="fas fa-angle-double-right"></i> 2pcs 2 x 2 colored ID Pictures</li>
        </ul>

        <h6>For Transferees</h6>
        <hr>
        <ul class="list-unstyled ml-4">
          <li class="list-item pb-2"><i class="fas fa-angle-double-right"></i> Honorable Dismissal/Transfer Credentials</li>
          <li class="list-item pb-2"><i class="fas fa-angle-double-right"></i> Transcript of Records</li>
          <li class="list-item pb-2"><i class="fas fa-angle-double-right"></i> Course Description for assessment of subjects taken</li>
          <li class="list-item pb-2"><i class="fas fa-angle-double-right"></i> 2pcs 2 x 2 colored ID Pictures</li>
        </ul>
      </div>
        @include('shared.side-content')
    </div>
  </div>
  @include('shared.contact-button')
@endsection
