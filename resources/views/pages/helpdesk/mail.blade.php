<div class="container">
  <div class="header">
    <p>Hello, this is <strong>{{ $e_fullname }}</strong></p>
    <p>Address: {{ $e_location }}</p>
  </div>
  <div class="message-details">
    <br>
    <p><strong>{{ strtoupper($e_subject) }}</strong></p>
    <p>{{ $e_message }}</p>
    <br>
  </div>
  <div class="footer">
    <p>From: {{ $e_sender }}</p>
    <p>Contact Number: {{ $e_contact_no }}</p>
  </div>
</div>
