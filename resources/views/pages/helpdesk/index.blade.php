@extends('layouts.landing')

@section('title')
  Access | Contact
@endsection

@section('css')
  <style>
    .news-card-img {
      width: 100%;
      height: 7rem;
      object-fit: cover;
    }
  </style>
@endsection

@section('content')
  <div id="header-contact">
    <div class="container pt-5">
      <div class="text-center">
        <h1 class="text-white">Access Helpdesk</h1>
        <hr class="bg-light w-100" style="max-width: 300px;">
        <p>We're ready to lead you into the future of success.</p>
      </div>
    </div>
  </div>
  <div class="container mt-5 pt-5">
    <h2>CONTACT US TODAY</h2>
    <hr class="bg-danger accent-2 mt-0 d-inline-block mx-auto" style="width: 100px;">
    @if ($message = Session::get('success'))
      <div class="card text-center border mb-5">
        <div class="card-header">
          Access Computer College
        </div>
        <div class="card-body">
          <h5 class="card-title">{{ $message }}</h5>
          <p class="card-text">We will contact you regarding your request as much as possible.</p>
          <a href="/" class="btn btn-outline-navyblue">Go back home</a>
        </div>
      </div>
    @else
    <div class="row">
      <div class="col-lg-9">
          <form action="/contact-us/send" method="POST">
            @csrf
            <div class="mb-5 pb-5">
              <p>Please fill out the quick form.</p>

              <div class="form-row mb-4">
                <div class="col-sm">
                  <div class="form-group">
                    <label for="Name" class="bmd-label-floating"> Full Name</label>
                    <input type="text" class="form-control" id="Name" name="fullname" required>
                    <span class="bmd-help">Enter your Fullname</span>
                  </div>
                </div>
              </div>
              <div class="form-row mb-4">
                <div class="col-sm mb-4">
                  <div class="form-group">
                    <label for="email" class="bmd-label-floating">Email Address</label>
                    <input type="email" class="form-control" id="email" name="sender" required>
                    <span class="bmd-help">We'll never share your email with anyone else.</span>
                  </div>
                </div>
                <div class="col-sm">
                  <label>Select Campus</label>
                  <select name="email" class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                    <option selected disabled>Select Campus</option>
                      @foreach ($categories->all() as $key => $category)
                        @if ($key > 0)
                          <option value="{{ $category->email }}">{{$category->branch_name}}</option>
                        @endif
                      @endforeach
                  </select>
                </div>
              </div>

              <div class="form-row mb-4">
                <div class="col-sm mb-4">
                  <div class="form-group">
                    <label for="location" class="bmd-label-floating">Current Address</label>
                    <input type="text" class="form-control" id="location" name="location" required>
                  </div>
                </div>
                <div class="col-sm ">
                  <div class="form-group">
                    <label for="contact_no" class="bmd-label-floating">Contact Number</label>
                    <input type="text" class="form-control" id="contact_no" name="contact_no" required>
                    <span class="bmd-help">Landline or Mobile Number</span>
                  </div>
                </div>
              </div>
              <div class="form-row mb-4">
                <div class="col">
                  <label>Topic</label>
                  <select name="subject" class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                    <option selected disabled>I would like to...</option>
                    @if (count($subjects) > 0)
                      @foreach ($subjects->all() as $subject)
                        <option value="{{ $subject->subject }}">{{$subject->subject}}</option>
                      @endforeach
                    @endif
                  </select>
                </div>
              </div>
              <div class="mb-4">
                <div class="form-group">
                  <label for="exampleTextarea" class="bmd-label-floating">Message</label>
                  <textarea class="form-control" name="message" id="exampleTextarea" rows="5"></textarea>
                </div>
              </div>
              <div class="form-row mb-5">
                <div class="custom-control custom-checkbox mr-sm-2">
                  <input type="checkbox" class="custom-control-input" id="customControlAutosizing" required>
                  <label class="custom-control-label" for="customControlAutosizing">By sending this form I confirm that <br/>I have read and accept <b>ACCESS</b> <a href="#" class="text-navyblue">Privacy Policy.</a></label>
                </div>
              </div>
              <button type="submit" class="btn btn-light bg-navyblue py-3 px-5">SEND MESSAGE</button>
            </div>
          </form>
        </div>
        <div class="col-lg-1"></div>
        <div class="col-lg-2">
          <h5>Get Connected</h5>
          <ul class="list-unstyled">
            <li><a href="https://web.facebook.com/accesscomputercollege/" target="_blank" class="nav-link text-navyblue pl-0" data-toggle="tooltip" data-placement="right" title="Like us on Facebook"><i class="fab fa-facebook mr-1"> </i> <span>Facebook</span></a></li>
            <li><a href="#" target="_blank" class="nav-link text-navyblue pl-0" data-toggle="tooltip" data-placement="right" title="Follow us on Twitter"><i class="fab fa-twitter mr-1" > </i> <span>Twitter</span></a></li>
            <li><a href="#" target="_blank" class="nav-link text-navyblue pl-0" data-toggle="tooltip" data-placement="right" title="Follow us on Instagram"><i class="fab fa-instagram mr-1" > </i> <span>Instagram</span></a></li>
          </ul>
          <hr>

          <h5>Related News</h5>
          <div class="row">
            @foreach ($news as $new)
              <div class="col-6 col-lg-12 col-md-3 col-sm-6">
                <a href="news/{{ $new->id }}">
                  <div class="mb-4">
                    <img class="news-card-img pb-2" src="{{ $new->photo->file }}" width="100%"  alt="Generic placeholder image">
                    <div class="">
                      <h6 class="card-title text-navyblue"><small>{{ $new->title }}</small></h6>
                    </div>
                  </div>
                </a>
              </div>
            @endforeach
          </div>
        </div>
      @endif
    </div>
  </div>
@endsection
