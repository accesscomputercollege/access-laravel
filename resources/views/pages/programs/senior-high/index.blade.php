@extends('layouts.landing')

@section('title')
  Access | Senior High School
@endsection

@section('content')
  <div id="header-seniorhigh">
    <div class="container pt-5">
      <div class="">
        <h1 class="text-light text-center display-3 d-block d-sm-block d-md-none">WELCOME <br><span style="color: red;">SENIOR HIGH SCHOOL <br> <sup><small class="font-weight-bold">S.Y. 2019-2020</small></sup></span> REGISTER TODAY!</h1>
      </div>
    </div>
  </div>
  <div id="divider" class="bg-darkred py-1"></div>
  <div class="container mb-5">
    <div class="row mt-5 pt-5">
      <div class="col-md-8">
        <div class="title mb-4">
          <h2 class="text-uppercase">Senior High School</h2>
          <hr class="bg-danger accent-2 mt-0 d-inline-block mx-auto" style="width: 100px;">
          <p>Senior high school (SHS) refers to Grades 11 and 12, the last two years of the K-12 program that DepEd has been implementing since 2012. ... High school in the old system consisted of First Year to Fourth Year. What corresponds to those four years today are Grades 7 to 10, also known as junior high school (JHS).</p>

          <div class="form mt-5">
            <h5>Campus with Senior High School</h5>
            <select onchange="location = this.value;" class="custom-select mr-sm-2" id="inlineFormCustomSelect">
              <option value="/campus/2">Access Balagtas Bulacan</option>
              <option value="/campus/3">Access Camarin Caloocan</option>
              <option value="/campus/4">Access Cubao, Quezon City</option>
              <option value="/campus/5">Access Lagro, Quezon City</option>
              <option value="/campus/6">Access C.M Recto Manila</option>
              <option value="/campus/7">Access Concepcion, Marikina</option>
              <option value="/campus/8">Access Marilao, Bulacan</option>
              <option value="/campus/9">Access Meycauayan, Bulacan</option>
              <option value="/campus/10">Access Monumento, Caloocan</option>
              <option value="/campus/11">Access Carmen Rosales, Pangasinan</option>
              <option value="/campus/12">Access Villasis, Pangasinan</option>
              <option value="/campus/13">Access San Nicolas, Pasig</option>
              <option value="/campus/14">Access Kapasigan, Pasig 2</option>
              <option value="/campus/15">Access Zabarte, Novaliches, Quezon City</option>
            </select>
          </div>

          <h4 class="mb-4 pt-5 text-center text-primary">Tracks, Strands, & Specializations</h4>
          <ul class="list-group">
            <li class="list-group-item bg-darkred text-white font-weight-bold">ACADEMIC TRACK</li>
            <table class="table table-hover border bg-white text-sm">
              <tbody>
                <tr>
                  <th scope="row">ABM</th>
                  <td>Accountancy, Business and Management</td>
                </tr>
                <tr>
                  <th scope="row">GAS</th>
                  <td>General Academic Strand</td>
                </tr>
              </tbody>
            </table>
          </ul>
          <ul class="list-group">
            <li class="list-group-item bg-navyblue text-white font-weight-bold">TECHNICAL-VOCATIONAL-LIVELIHOOD TRACK</li>
            <table class="table table-hover border bg-white text-sm">
              <tbody>
                <tr>
                  <th scope="row">HE</th>
                  <td>Home Economics</td>
                </tr>
                <tr>
                  <th scope="row">IA</th>
                  <td>Industrial Arts</td>
                </tr>
                <tr>
                  <th scope="row">ICT</th>
                  <td>Information & Communications Technology</td>
                </tr>
              </tbody>
            </table>
          </ul>
        </div>

      </div>
      <div class="col-md-4">
        <img class="img-thumbnail mb-3" src="../../images/courses/senior_high2.jpg" width="100%">
        <img class="img-thumbnail " src="../../images/courses/senior_high.jpg" width="100%">
      </div>
    </div>
  </div>
  @include('shared.contact-button')
@endsection
