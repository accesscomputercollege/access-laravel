@extends('layouts.landing')

@section('title')
  Access | Collegiate Programs
@endsection

@section('content')
<div id="header-collegiate">
  <div class="container pt-5">
    <div class="text-center">
      <h1 class="text-white">Collegiate Programs</h1>
      <hr class="bg-light w-100" style="max-width: 300px;">
      <p class="text-uppercase">{{ $college->course_name }}</p>
    </div>
  </div>
</div>
<div id="divider" class="bg-blue py-1"></div>

<div class="container mt-5 pt-5">
  <a href="/collegiate" class="text-dark"><i class="fas fa-angle-left mr-2"></i> Back to Collegiate Programs</a>
  <div class="row mt-3">
    <div class="col-lg-9 mb-5 pb-5">
      <h2>{{ $college->course_name }}</h2>
      <p>{!! $college->description !!}</p>
      <img class="w-100" src="https://scontent.fmnl6-2.fna.fbcdn.net/v/t1.0-9/11059748_10153306742973777_8979594987902190545_n.jpg?_nc_cat=100&_nc_eui2=AeHa8ZisymumUxkcrXz3dnanHEQc2mHMAeS96qzBMk2xr_wRs84LgeTaDUrz5Z58fer07ByVIx8jgBdaKZOROM7CRGxHCdhfQalFEsrcrZCbetqFdwpPi8yA9QV5quWOaeI&_nc_ht=scontent.fmnl6-2.fna&oh=f978dc84671af0f0382fb7e1a72721d9&oe=5CF14913">

      <div class="row mt-3">
        <div class="col-md-6">
          <div class="form mt-5">
            <h3 class="mb-4">Campus with {{ $college->course_code }}</h3>
            <select onchange="location = this.value;" class="custom-select mr-sm-2" id="inlineFormCustomSelect">
              @foreach ($college->campuses as $campus)
                <option value="/campus/{{ $campus->id }}">{{ $campus->branch_name }}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="col-md-6">
          <h3 class="mt-5 mb-3"><i class="fas fa-book"></i> Career Opportunities</h3>
          <div class="career-list">
            {!! $college->career_list !!}
          </div>
        </div>
      </div>
    </div>
    @include('shared.side-content')
  </div>
</div>
@include('shared.contact-button')
@endsection
