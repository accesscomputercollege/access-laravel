@extends('layouts.landing')

@section('title')
  Access | Collegiate Programs
@endsection

@section('content')
  <div id="header-collegiate">
    <div class="container pt-5">
      <div class="text-center">
        <h1 class="text-white">Collegiate Programs</h1>
      </div>
    </div>
  </div>
  <div id="divider" class="bg-blue py-1"></div>
  <div class="container mb-5">
    <div class="row mt-5 pt-5">
      <div class="col-lg-8">
        <div class="title mb-4">
          <h2 class="text-uppercase">Collegiate Programs</h2>
          <hr class="bg-danger accent-2 mt-0 d-inline-block mx-auto" style="width: 100px;">
          <p>A degree given to students of a higher learning institution signifying required credits have been obtained in specific areas of study. There are several levels of college degrees ranging from an Associate's degree to a Doctoral degree.</p>
        </div>
        @if (count($collegiate) > 0)
          <h4 class="mb-4 text-primary">Offered Course</h4>
            @foreach ($collegiate as $college)
              <div class="card border mb-2">
                <a class="text-light" href="/collegiate/{{$college->id}}">
                  <div class="card-header font-weight-bold bg-blue text-uppercase py-3">
                    <i class="fas fa-plus mr-2"></i> {{$college->course_name}}
                  </div>
                </a>
              </div>
            @endforeach
            <div class="pagination mt-5">
              {{ $collegiate->links() }}
            </div>
          @else
            <p class="alert alert-warning">No Collegiate Programs found</p>
        @endif
        </div>
        <div class="col-lg-4">
          <img src="../images/courses/earn-your-college.jpg" width="100%">
        </div>
      </div>
    </div>
  </div>
  @include('shared.contact-button')
@endsection
