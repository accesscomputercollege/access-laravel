@extends('layouts.landing')

@section('title')
  Access | Short Training Programs
@endsection

@section('content')
  <div id="header-shortcourse">
    <div class="container pt-5">
      <div class="text-center">
        <h1 class="text-white">Short Training Programs</h1>
      </div>
    </div>
  </div>
  <div id="divider" class="bg-darkred py-1"></div>
  <div class="container mt-5 pt-5 mb-5">
    <div class="row">
      <div class="col-md-8">
        <div class="title mb-4">
          <h2 class="text-uppercase">Short Training Programs</h2>
          <hr class="bg-danger accent-2 mt-0 d-inline-block mx-auto" style="width: 100px;">
          <p>Short-term training can give your career a boost. Short-term training includes any class or program that lasts less than two years. All short-term training can help you find a job, get a promotion, or earn more money. Many programs lead to a certificate, which can give you a helpful edge in the job market.</p>
        </div>
        @if (count($shortcourses) > 0)
          <h4 class="mb-4 text-primary">Offered Course</h4>
            @foreach ($shortcourses as $shortcourse)
              <div class="card border mb-2">
                <a class="text-light" href="/shortcourse/{{$shortcourse->id}}">
                  <div class="card-header font-weight-bold bg-darkred text-uppercase py-3">
                    <i class="fas fa-plus mr-2"></i> {{$shortcourse->course_name}}
                  </div>
                </a>
              </div>
            @endforeach
            <div class="pagination mt-5">
              {{ $shortcourses->links() }}
            </div>
          @else
            <p class="alert alert-warning">No Collegiate Programs found</p>
        @endif
      </div>
      <div class="col-md-4">
        <img src="../images/courses/quality-education.jpg" width="100%">
      </div>
    </div>
  </div>
  @include('shared.contact-button')
@endsection
