@extends('layouts.landing')

@section('title')
  Access | Short Training Programs
@endsection

@section('content')
<div id="header-shortcourse">
  <div class="container pt-5">
    <div class="text-center">
      <h1 class="text-white">Short Training Programs</h1>
      <hr class="bg-light w-100" style="max-width: 300px;">
      <p class="text-uppercase">{{ $shortcourse->course_name }}</p>
    </div>
  </div>
</div>
<div id="divider" class="bg-darkred py-1"></div>
<div class="container mt-5 pt-5">
  <a href="/shortcourses" class="text-dark"><i class="fas fa-angle-left mr-2"></i> Back to Short Traning Programs</a>
  <div class="row mt-3">
    <div class="col-lg-9 mb-5 pb-5">
      <h2>{{ $shortcourse->course_name }}</h2>
      <p>{!! $shortcourse->description !!}</p>
      <img class="w-100" src="https://scontent.fmnl6-1.fna.fbcdn.net/v/t1.0-9/13015343_10154260035443777_2129007883618946631_n.jpg?_nc_cat=101&_nc_eui2=AeFt-Gbx1QcxAGjhKHZYhR-4w_lRn9DyCW85Id_DwoHLiqhtb9S6T-tyDnZlWMRXFZnjmEVcWNum457SJmuWuxGM38-U8mcUOdF7y4vIj-hJGNVaBuFWUxn6-caClJSnKRk&_nc_ht=scontent.fmnl6-1.fna&oh=7b79e87dc1f01d180bbd183294809158&oe=5CF589FB">

      <div class="row mt-3">
        <div class="col-md-6">
          <div class="form mt-5">
            <h3 class="mb-4">Campus with {{ $shortcourse->course_name }}</h3>
            <select onchange="location = this.value;" class="custom-select mr-sm-2" id="inlineFormCustomSelect">
              @foreach ($shortcourse->campuses as $campus)
                <option value="/campus/{{ $campus->id }}">{{ $campus->branch_name }}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="col-md-6">
          <h3 class="mt-5 mb-4"><i class="fas fa-book"></i> Career Opportunities</h3>
          <div class="career-list">
            {!! $shortcourse->career_list !!}
          </div>
        </div>
      </div>
    </div>
    @include('shared.side-content')
  </div>
</div>
@include('shared.contact-button')
@endsection
