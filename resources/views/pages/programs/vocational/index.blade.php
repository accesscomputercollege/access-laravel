@extends('layouts.landing')

@section('title')
  Access | Technical Vocational Programs
@endsection

@section('content')
  <div id="header-vocational">
    <div class="container pt-5">
      <div class="text-center">
        <h1 class="text-white">Technical Vocational Programs</h1>
      </div>
    </div>
  </div>
  <div id="divider" class="bg-navyblue py-1"></div>

  <div class="container mt-5 pt-5 mb-5">
    <div class="row">
      <div class="col-lg-8">
        <div class="title mb-4">
          <h2 class="text-uppercase">Technical Vocational Programs</h2>
          <hr class="bg-danger accent-2 mt-0 d-inline-block mx-auto" style="width: 100px;">
          <p>Vocational training programs allow students to get ready for specific careers. Some high schools provide vocational training; at the postsecondary level, prospective students can consider standalone courses, certificate/diploma-granting programs, associate's degree programs and apprenticeships.</p>
        </div>
        @if (count($vocationals) > 0)
          <h4 class="mb-4 text-primary">Offered Course</h4>
            @foreach ($vocationals as $voc)
              <div class="card border mb-2">
                <a class="text-light" href="/vocational/{{$voc->id}}">
                  <div class="card-header font-weight-bold bg-navyblue text-uppercase py-3">
                    <i class="fas fa-plus mr-2"></i> {{$voc->course_name}}
                  </div>
                </a>
              </div>
            @endforeach
            <div class="pagination mt-5">
              {{ $vocationals->links() }}
            </div>
          @else
            <p class="alert alert-warning">No Vocational Programs found</p>
        @endif
      </div>
      <div class="col-lg-4">
        <img src="../images/courses/change-the-shape.jpg" width="100%">
      </div>
    </div>
  </div>
  @include('shared.contact-button')
@endsection
