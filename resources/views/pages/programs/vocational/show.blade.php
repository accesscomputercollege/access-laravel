@extends('layouts.landing')

@section('title')
  Access | Technical Vocational Programs
@endsection

@section('content')
<div id="header-vocational">
  <div class="container pt-5">
    <div class="text-center">
      <h1 class="text-white">Technical Vocational Programs</h1>
      <hr class="bg-light w-100" style="max-width: 300px;">
      <p class="text-uppercase">{{ $vocational->course_name }}</p>
    </div>
  </div>
</div>
<div id="divider" class="bg-navyblue py-1"></div>

<div class="container mt-5 pt-5">
  <a href="/vocationals" class="text-dark"><i class="fas fa-angle-left mr-2"></i> Back to Vocationals Programs</a>
  <div class="row mt-3">
    <div class="col-lg-9 mb-5 pb-5">
      <h2>{{ $vocational->course_name }}</h2>
      <p>{!! $vocational->description !!}</p>
      <img class="w-100 img-thumbnail" src="https://scontent.fmnl6-1.fna.fbcdn.net/v/t1.0-9/10154079_10152469235598777_4234245997862654253_n.png?_nc_cat=107&_nc_eui2=AeGjm53OQ2paRScZjzYUPoM4nVWqzgSaDrxRT0Q-DG4F-jR0Aa6pdiQXxHxPPlbXoCb2-7MqQj3D3uau2d6M7pTGXjgzZ0CVFx0zkY2gcronkWcZGCthr1COr3zoujAmPkw&_nc_ht=scontent.fmnl6-1.fna&oh=0d106e0ad58ff40f78e4fbb57dcc4712&oe=5CB475DA">
      <div class="row mt-3">
        <div class="col-md-6">
          <div class="form mt-5">
            <h3 class="mb-4">Campus with {{ $vocational->course_code }}</h3>
            <select onchange="location = this.value;" class="custom-select mr-sm-2" id="inlineFormCustomSelect">
              @foreach ($vocational->campuses as $campus)
                <option value="/campus/{{ $campus->id }}">{{ $campus->branch_name }}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="col-md-6">
          <h3 class="mt-5 mb-4"><i class="fas fa-book"></i> Career Opportunities</h3>
          <div class="career-list">
            {!! $vocational->career_list !!}
          </div>
        </div>
      </div>
    </div>
    @include('shared.side-content')
  </div>
</div>
@include('shared.contact-button')
@endsection
