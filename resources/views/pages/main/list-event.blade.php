<div class="title mt-5 mb-3 pt-5">
  <h3>UPCOMING EVENTS</h3>
  <hr class="bg-danger accent-2 mt-0 d-inline-block mx-auto" style="width: 100px;">
</div>

@if (count($events) > 0)
  @foreach ($events as $event)
    <div class="row mb-3">
      <div class="col-4">
        <ul class="list-unstyled text-center">
          <li class="p-3 text-white bg-navyblue">{{date('D', strtotime($event->startdate))}}</li>
          <li class="p-3 bg-white border">{{date('M j', strtotime($event->startdate))}}</li>
        </ul>
      </div>
      <div class="col-8 pl-0 mt-2">
        <a href="/events/{{ $event->id }}" class="nav-link p-0">
          <h6 class="text-navyblue font-weight-bold mb-2">{{ $event->title }}</h6>
        </a>
        <p class="mb-0"><small><i class="far fa-clock mr-2 text-navyblue"></i> {{date('h:i A', strtotime($event->start_time))}} - {{date('h:i A', strtotime($event->end_time))}}</small></p>
        <p><small>All Campuses</small></p>
      </div>
    </div>
    <hr>
  @endforeach
  @else
    <p class="alert alert-warning">No Events found</p>
@endif
<div class="d-flex justify-content-center">
  <a href="/events" class="text-navyblue text-sm font-weight-bold">ALL EVENTS <i class="fas fa-angle-right ml-2"></i></a>
</div>
