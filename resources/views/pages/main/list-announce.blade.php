<div class="title mt-5 mb-3 pt-5">
  <h3>ANNOUNCEMENTS</h3>
  <hr class="bg-danger accent-2 mt-0 d-inline-block mx-auto" style="width: 100px;">
</div>
@if (count($announcements) > 0)
  @foreach ($announcements as $announce)
    <div class="card mb-4 bg-white border">
      <div class="card-header text-right py-2 bg-navyblue">
        <small class="badge badge-light p-2">{{$announce->category->branch_name}}</small>
      </div>
      <div class="card-body py-2">
        <a href="/announcement/{{ $announce->id }}/{{ $announce->category->slug }}" class="nav-link pl-0">
          <h6 class="mt-0 font-weight-bold mb-0 text-navyblue">{{ $announce->title }}</h6>
        </a>
        <div class="d-flex justify-content-between">
          @if ($announce->created_at == $announce->updated_at)
            <p class="text-sm text-muted mb-0 pb-0">Published • {{date('M j, Y', strtotime($announce->created_at))}}</p>
          @else
            <p class="text-sm badge badge-info mb-0 py-1">Updated • {{date('M j, Y', strtotime($announce->updated_at))}}</p>
          @endif
        </div>
        <p class="text-truncate">{!! $announce->content_brief !!}</p>
      </div>
    </div>
  @endforeach
@else
  <p class="alert alert-warning">No Announcement</p>
@endif
<hr>
<div class="d-flex justify-content-center">
  <a href="/announcements" class="text-navyblue text-sm font-weight-bold">ALL ANNOUNCEMENTS <i class="fas fa-angle-right ml-2"></i></a>
</div>
