<div class="bg-light py-5">
  <div class="container py-5">
    <div class="title text-center mb-4">
      <h2>OUR LATEST NEWS</h2>
      <a href="/news" class="text-navyblue text-sm font-weight-bold">ALL POST <i class="fas fa-angle-right ml-2"></i></a>
    </div>
    @if (! $news->count())
      <div class="alert alert-warning text-center">
        <h4>No News Found</h4>
      </div>
    @else
    <section id="card-news">
      <div class="row">
        @foreach ($new as $n)
          <div class="col-md-6 mb-4">
            <div class="card bg-light">
              <a class="text-dark" href="news/{{ $n->id }}">
              <span class="badge text-white bg-darkred p-2 m-1" style="position: absolute; right: 0">NEWS</span>
              <img src="{{ $n->photo->file }}" class="top-img-card align-self-center d-block d-sm-block d-md-block rounded" alt="...">
              <h6 class="text-truncate mt-2">{{ $n->title }}</h6>
              <div class="campus">{{ $n->campus }}</div>
              <div class="date-status">
                @if ($n->created_at == $n->updated_at)
                  <p class="text-sm text-muted mb-1 pb-0"><b>Published</b> • {{date('M j, Y', strtotime($n->created_at))}}</p>
                @else
                  <p class="text-sm badge badge-info mb-1 py-1"><b>Updated Post</b> • {{date('M j, Y', strtotime($n->updated_at))}}</p>
                @endif
              </div>
              </a>
            </div>
          </div>
        @endforeach
      </div>

      <div class="row">
        @foreach ($news->all() as $key => $new)
          @if ($key > 1)
            <div class="col-md-6 col-lg-3 d-flex mb-4">
              <div class="card bg-light">
                <a class="text-dark" href="news/{{ $new->id }}">
                <span class="badge text-white bg-darkred p-2 m-1" style="position: absolute; right: 0">NEWS</span>
                <img src="{{ $new->photo->file }}" class="top-img-card align-self-center d-block d-sm-block d-md-block rounded" alt="...">
                <h6 class="text-truncate mt-2">{{ $new->title }}</h6>
                <div class="campus">{{ $new->campus }}</div>
                <div class="date-status">
                  @if ($new->created_at == $new->updated_at)
                    <p class="text-sm text-muted mb-1 pb-0"><b>Published</b> • {{date('M j, Y', strtotime($new->created_at))}}</p>
                  @else
                    <p class="text-sm badge badge-info mb-1 py-1"><b>Updated Post</b> • {{date('M j, Y', strtotime($new->updated_at))}}</p>
                  @endif
                </div>
                </a>
              </div>
            </div>
          @endif
        @endforeach
      </div>
    </section>
    @endif
</div>
