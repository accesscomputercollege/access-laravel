@extends('layouts.landing')

@section('title')
  Access Computer College | Quality Education Within Reach
@endsection

@section('css')
  <style>
      .top-img-card{
        width: 100%;
        height: 10rem;
        object-fit: cover;
      }
  </style>
@endsection

@section('content')
@include('shared.carousel')
<section id="welcome-to-access" class="bg-white" style="padding: 100px 0px;">
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <h2 class="font-weight-bold">WELCOME TO ACCESS</h2>
        <hr class="bg-danger accent-2 mt-0 d-inline-block mx-auto" style="width: 100px;">
        <p>The country's recognized leader and pioneer in High-tech education since 1981,
          continues its mission to offer the most in-demand courses wherein graduates are most
          in-demand today. To keep pace with the rapid advancement of technology, without sacrificing
          thorough training. The curriculum is geared to a world class excellence. Effective Training is
          achieved through personalized method of teaching in small-sized classes, with actual laboratory
          exercises and extensive hands-on practice, aided by high-tech audio - video devices and supplemented
          by scheduled on-the-job training</p>
      </div>
      <div class="col-lg-6 text-center">
        <iframe width="100%" height="315" frameborder="0" style="border:0"
          src="https://www.youtube.com/embed/NPi5Q38iPTY">
        </iframe>
      </div>
    </div>
    <div class="d-flex justify-content-center mt-5">
      <a href="/contact-us" class="btn btn-outline-navyblue px-4">TALK TO OUR ADMIN <i class="fas fa-angle-right ml-2"></i></a>
    </div>
  </div>
  <div>
</section>

  @include('pages.main.list-news')
</div>
<div id="advocacy">
  <div class="container">
    <h1 class="text-light text-center">Come in</h1>
    <h2 class="text-light text-center">and</h2>
    <h1 class="text-light text-center display-2">ENROLL TODAY</h1>
  </div>
</div>
<div id="divider" class="bg-blue py-1"></div>
<div class="container">
  <div class="row">
    <div class="col-lg-8">
      @include('pages.main.list-announce')
    </div>
    <div class="col-lg-4">
      @include('pages.main.list-event')
    </div>
  </div>
</div>
<div class="map mt-5 pt-5 text-center">
  <h2 class="mb-3">ACCESS CAMPUSES</h2>
  <hr class="bg-danger accent-2 mt-0 d-inline-block mx-auto" style="width: 100px;">
  <div class="map">
    @include('shared.googlemap')
  </div>
</div>
@include('shared.contact-button')
@endsection
