@extends('layouts.landing')

@section('title')
  Access | Event
@endsection

@section('content')
<div id="header-event">
  <div class="container pt-5">
    <div class="text-center">
      <h1 class="text-white">Access Upcoming Events</h1>
      <hr class="bg-light w-100" style="max-width: 300px;">
      <p class="text-uppercase">{{ $event->title }}</p>
    </div>
  </div>
</div>
<div class="container mt-5 pt-5">
  <a href="/events" class="text-dark"><i class="fas fa-angle-left mr-2"></i> Back to Events</a>
  <div class="row mt-3">
    <div class="col-lg-9 mb-5 pb-5">
      <div class="border" id="card">
        <span class="badge bg-darkred text-white m-2 p-2" style="position: absolute;">Event</span>
        <img class="shadow-sm" src="{{ $event->photo ? $event->photo->file : '../../images/event-placeholder.png' }}" width="100%" alt="">
        <div class="card-body bg-white">
          <div class="row">
            <div class="col-md-2">
              <ul class="list-unstyled text-center">
                <li class="p-3 text-white bg-navyblue">{{date('D', strtotime($event->startdate))}}</li>
                <li class="p-3 bg-white border">{{date('M j, Y', strtotime($event->startdate))}}</li>
              </ul>
            </div>
            <div class="col-md-10">
              <h4>{{ $event->title }}</h4>
              <p class="text-muted"><small>Date Posted : {{date('M j Y', strtotime($event->created_at))}}</small></p>
              <p class="card-text">{!! $event->detail !!}</p>
            </div>
          </div>
        </div>
      </div>
      <div class="card border mt-3">
        <div class="card-header bg-darkred text-light mb-3"><i class="fas fa-exclamation-circle mr-2"></i>Important to know</div>
        <div class="card-body">{!! $event->note !!}</div>
      </div>
    </div>
    @include('shared.side-content')
  </div>
</div>
@include('shared.contact-button')
@endsection
