@extends('layouts.landing')

@section('title')
  Access | Event
@endsection

@section('css')
  <style>
      .card-img-top{
        width: 100%;
        height: 15vw;
        height: 35vh;
        object-fit: cover;
      }
  </style>
@endsection

@section('content')
  <div id="header-event">
    <div class="container pt-5">
      <div class="text-center">
        <h1 class="text-white">Access Upcoming Events</h1>
      </div>
    </div>
  </div>
  <div class="container mt-5 pt-5">
    <div class="title mb-4">
      <h2>Our Upcoming Events</h2>
      <hr class="bg-danger accent-2 mt-0 d-inline-block mx-auto" style="width: 100px;">
    </div>
    @if (count($events) > 0)
      <div class="row">
        @foreach ($event as $ev)
          <div class="col-md-6 mb-4">
            <div class="card bg-white">
              <a class="text-dark" href="events/{{ $ev->id }}">
                <div class="calendar p-2" style="position: absolute;">
                  <ul class="list-unstyled text-center">
                    <li class="px-4 py-2 text-white bg-navyblue">{{date('D', strtotime($ev->startdate))}}</li>
                    <li class="px-4 py-2 bg-white border">{{date('M j', strtotime($ev->startdate))}}</li>
                  </ul>
                </div>
                <img class="card-img-top align-self-center d-block d-sm-block d-md-block rounded" src="{{ $ev->photo ? $ev->photo->file : '../../images/event-placeholder.png' }}">
                <div class="card-body">
                  <h6 class="text-truncate mt-2">{{ $ev->title }}</h6>
                  <div class="campus">All Campus</div>
                      <p class="mb-0"><small><i class="far fa-clock mr-2 text-navyblue"></i> {{date('h:i A', strtotime($ev->start_time))}} - {{date('h:i A', strtotime($ev->end_time))}}</small></p>
                  <p>{!! $ev->detail_brief !!}</p>
                </div>
              </a>
            </div>
          </div>
        @endforeach
      </div>
      <div class="row">
        @foreach ($events->all() as $key => $event)
          @if ($key > 1)
            <div class="col-md-4 mb-4">
              <div class="card bg-white">
                <a class="text-dark" href="events/{{ $event->id }}">
                  <div class="calendar p-2" style="position: absolute;">
                    <ul class="list-unstyled text-center">
                      <li class="px-4 py-2 text-white bg-navyblue">{{date('D', strtotime($event->startdate))}}</li>
                      <li class="px-4 py-2 bg-white border">{{date('M j', strtotime($event->startdate))}}</li>
                    </ul>
                  </div>
                  <img class="card-img-top align-self-center d-block d-sm-block d-md-block rounded" src="{{ $event->photo ? $event->photo->file : '../../images/event-placeholder.png' }}">
                  <div class="card-body">
                    <h6 class="text-truncate mt-2">{{ $event->title }}</h6>
                    <div class="campus">All Campus</div>
                        <p class="mb-0"><small><i class="far fa-clock mr-2 text-navyblue"></i> {{date('h:i A', strtotime($event->start_time))}} - {{date('h:i A', strtotime($event->end_time))}}</small></p>
                    <p>{!! $event->detail_brief !!}</p>
                  </div>
                </a>
              </div>
            </div>
          @endif
        @endforeach
      </div>
      @else
        <p class="alert alert-warning">No Events found</p>
    @endif
    <div class="pagination d-flex justify-content-end">
      {{ $events->links() }}
    </div>
  </div>
  @include('shared.contact-button')
@endsection
