@extends('layouts.landing')

@section('title')
  Access | Campuses
@endsection

@section('css')
  <style>
    .top-img-card {
      width: 100%;
      height: 12rem;
      object-fit: cover;
    }

    .top-img-card:hover {
      opacity: 0.8;
    }
  </style>
@endsection

@section('content')
  <div id="header-campuses">
    <div class="container">
      <div class="text-center">
        <h1 class="text-white mt-5">Campus Directory</h1>
        <p>A Great Place For Education</p>
      </div>
    </div>
  </div>
  <div class="container mt-5 pt-5">
    <div class="row">
      <div class="col-lg-9 mb-5 pb-5">
        <h2 class="mb-0">ACCESS CAMPUSES</h2>
        <hr class="bg-danger accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 100px;">
        <div id="accordion">
          <div class="border mb-4">
            <div class="card-header bg-navyblue" id="headingOne">
              <p class="mb-0 text-center">
                <a href="#" class="nav-link p-0 text-light" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                  View Location Map <i class="fas fa-map-marker-alt"></i>
                </a>
              </p>
            </div>
            <div id="collapseOne" class="accordion-body collapse" aria-labelledby="headingOne" data-parent="#accordion">
              <div class="map">
                @include('shared.googlemap')
              </div>
            </div>
          </div>
        </div>
        @foreach ($campuses as $key => $campus)
          @if ($key > 0)
            <div class="row no-gutters mb-5 bg-white border">
              <div class="col-md-4">
                <a href="/campus/{{ $campus->id }}"><img class="top-img-card" src="{{ $campus->photo ? $campus->photo->file : '../images/image-placeholder.png' }}"></a>
              </div>
              <div class="col-md-8">
                <div class="bg-navyblue">
                  <h5 class="branch_name py-2 pl-3"><a href="/campus/{{ $campus->id }}" class="text-white">ACCESS {{ $campus->branch_name }}</a></h5>
                </div>
                <div class="card-body">
                  <p class="text-sm font-weight-bold">SCHOOL INFORMATION</p>
                  <p class="text-sm text-truncate"><i class="fas fa-hotel mr-2 text-darkred"></i> {{ $campus->address }}</p>
                  <p class="text-sm"><i class="fas fa-phone mr-2 text-darkred"></i> {{ $campus->telephone }} {{ $campus->mobile }}</p>
                </div>
              </div>
            </div>
          @endif
        @endforeach
        <div class="pagination">
          {{ $campuses->links() }}
        </div>
      </div>
      @include('shared.side-content')
    </div>
  </div>
  @include('shared.contact-button')
@endsection
