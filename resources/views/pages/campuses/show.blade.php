@extends('layouts.landing')

@section('title')
  Access | Campuses
@endsection

@section('css')
  <style>
    #header-campus {
      width: 100%;
      height: 70vh;
      background:linear-gradient(0deg,rgba(0,20,70,0.6),rgba(0,20,70,0.6)),url('{{$campus->photo ? $campus->photo->file : 'http://placehold.it/100x60'}}');
      background-size: cover;
      background-attachment: fixed;
      box-shadow: 0px 10px 15px lightgray !important;
      color: #fff;
      display: flex;
      flex-direction: column;
      justify-content: center;
      text-transform: uppercase;
    }
  </style>
@endsection

@section('content')
  @include('shared.contact-button')
  <div id="header-campus">
    <div class="container">
      <div class="text-center">
        <h1 class="text-white mt-5 pt-5">Access {{ $campus->branch_name }} Campus</h1>
        <hr class="bg-light w-100" style="max-width: 300px;">
        <p>A Great Place For Education</p>
      </div>
    </div>
  </div>
  <div class="container mt-5 pt-3">
    <a href="/campuses" class="text-dark"><i class="fas fa-angle-left mr-2"></i> Back to Campuses</a>
    <div class="section mt-4 mb-5">
      <div class="row">
        <div class="col-lg-9 mb-5">
          <div class="row">
            <div class="col mb-3">
              <h3 class="mb-0 text-uppercase">ACCESS {{ $campus->branch_name }}</h3>
              <hr class="bg-danger accent-2 mb-3 mt-0 d-inline-block mx-auto" style="width: 100px;">
              <img class="w-100 img-thumbnail" src="{{ $campus->photo ? $campus->photo->file : '../images/image-placeholder.png' }}">
            </div>
          </div>
          <div id="accordion">
            <div class="border">
              <div class="card-header bg-navyblue" id="headingOne">
                <h6 class="mb-0">
                  <a href="#" class="nav-link p-0 text-light" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    School Information <i class="fas fa-plus float-right"></i>
                  </a>
                </h6>
              </div>
              <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="bg-white">
                  <div class="table-responsive">
                    <table class="table table-hover">
                      <tr class="d-flex border-bottom" data-toggle="tooltip" data-placement="top" title="School Address">
                        <td class="align-self-center border-0 mr-1"><i class="fas fa-map-marked-alt"></i></td>
                        <td class="text-sm mt-1 border-0">{{ $campus->address }}</td>
                      </tr>
                      <tr class="d-flex border-bottom" data-toggle="tooltip" data-placement="top" title="Telephone No.">
                        <td class="align-self-center border-0 mr-1"><i class="fas fa-phone"></i></td>
                        <td class="text-sm mt-1 border-0">{{ $campus->telephone }}</td>
                      </tr>
                      <tr class="d-flex border-bottom" data-toggle="tooltip" data-placement="top" title="Mobile No.">
                        <td class="justify-content-center align-self-center border-0 mr-2"><i class="fas fa-mobile-alt"></i></td>
                        <td class="text-sm mt-1 border-0">{{ $campus->mobile }}</td>
                      </tr>
                      <tr class="d-flex border-bottom" data-toggle="tooltip" data-placement="top" title="Email Address">
                        <td class="align-self-center border-0 mr-1"><i class="fas fa-envelope"></i></td>
                        <td class="text-sm mt-1 border-0">{{ $campus->email }}</td>
                      </tr>
                      <tr class="d-flex border-bottom" data-toggle="tooltip" data-placement="top" title="Facebook Page">
                        <td class="align-self-center border-0 mr-1"><i class="fab fa-facebook"></i></td>
                        <td class="text-sm mt-1 border-0"><a class="text-navyblue" href="{{ $campus->facebook }}">Click Here</a></td>
                      </tr>
                      <tr class="d-flex border-bottom" data-toggle="tooltip" data-placement="top" title="ACCESS Helpdesk">
                        <td class="align-self-center border-0"><i class="fas fa-comments"></i></td>
                        <td class="text-sm mt-1 border-0"><a class="text-navyblue" href="/contact-us">Contact now</a></td>
                      </tr>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="border">
              <div class="card-header bg-navyblue" id="headingTwo">
                <h6 class="mb-0">
                  <a href="#" class="nav-link p-0 text-light" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    Programs <i class="fas fa-plus float-right"></i>
                  </a>
                </h6>
              </div>
              <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                <div class=" bg-white">
                  <div class="list-group">
                    <li class="list-group-item list-group-item-action text-sm border-bottom font-weight-bold"><i class="fas fa-chevron-circle-right mr-2"></i> SENIOR HIGH</li>
                    @foreach ($campus->seniorhigh as $senior)
                      <a href="/senior-high" class="list-group-item list-group-item-action text-sm border-bottom">{{ $senior->course_name }}</a>
                    @endforeach
                    <li class="list-group-item list-group-item-action text-sm border-bottom font-weight-bold"><i class="fas fa-chevron-circle-right mr-2"></i> VOCATIONALS</li>
                    @foreach ($campus->vocationals as $vocational)
                      <a href="/vocational/{{ $vocational->id }}" class="list-group-item list-group-item-action text-sm border-bottom">{{ $vocational->course_name }}</a>
                    @endforeach

                    <li class="list-group-item list-group-item-action text-sm border-bottom font-weight-bold"><i class="fas fa-chevron-circle-right mr-2"></i> COLLEGIATE</li>
                    @foreach ($campus->collegiate as $college)
                      <a href="/collegiate/{{ $college->id }}" class="list-group-item list-group-item-action text-sm border-bottom">{{ $college->course_name }}</a>
                    @endforeach

                    <li class="list-group-item list-group-item-action text-sm border-bottom font-weight-bold"><i class="fas fa-chevron-circle-right mr-2"></i> SHORT TRAININGS</li>
                    @foreach ($campus->shortcourses as $shortcourse)
                      <a href="#" class="list-group-item list-group-item-action text-sm border-bottom">{{ $shortcourse->course_name }}</a>
                    @endforeach
                  </div>
                </div>
              </div>
            </div>
            <div class="border">
              <div class="card-header bg-navyblue" id="headingThree">
                <h6 class="mb-0">
                  <a href="#" class="nav-link p-0 text-light" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    Location Map <i class="fas fa-plus float-right"></i>
                  </a>
                </h6>
              </div>
              <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                <div class="bg-white">
                  {!! $campus->map !!}
                </div>
              </div>
            </div>
          </div>
        </div>
        @include('shared.side-content')
      </div>
    </div>
  </div>
@endsection
