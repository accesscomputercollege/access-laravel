@extends('layouts.landing')

@section('title')
  Access | Announcement
@endsection

@section('content')
<div id="header-announcement">
  <div class="container">
    <div class="text-center">
      <h2 class="text-white">ANNOUNCEMENTS</h2>
      <p class="text-uppercase">{{$announcement->category->branch_name}}</p>
    </div>
  </div>
</div>
<div class="bg-transparent">
  <div class="container my-5">
    <a href="/announcements" class="text-dark"><i class="fas fa-angle-left mr-2"></i> Back to Announcements</a>
    <div class="mt-4">
      <div class="card border">
        <div class="card-header pt-2 pb-0 bg-navyblue">
          <h4 class="text-light text-center">{{$announcement->category->branch_name}}</h4>
        </div>
        <div class="card-body">
          <p class="mb-4" style="font-size: 23px;">{{ $announcement->title }}</p>
          <div class="d-flex justify-content-between">
            @if ($announcement->created_at == $announcement->updated_at)
              <p class="text-muted mb-0 pb-0">Date posted - {{date('M j, Y', strtotime($announcement->created_at))}}</p>
            @else
              <p class="bg-info text-light mb-0 px-2 py-1">Updated Post - {{date('M j, Y', strtotime($announcement->updated_at))}}</p>
            @endif
          </div>
          <p class="mt-3">{!! $announcement->content_extended !!}</p>
        </div>
      </div>
    </div>
  </div>
</div>
@include('shared.contact-button')
@endsection
