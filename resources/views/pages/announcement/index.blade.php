@extends('layouts.landing')

@section('title')
  Access | Announcement
@endsection

@section('content')
  <div id="header-announcement">
    <div class="container">
      <div class="text-center">
        <h1 class="text-white">Announcements</h1>
      </div>
    </div>
  </div>
  <div>
    <div class="container my-5 pt-5">
      <div class="row">
        <div class="col-lg-4" id="sidebar">
          <div>
            <h3 class="mb-4">Campuses</h3>
            <button type="button" class="btn btn-dark bg-white d-block d-sm-block d-lg-none shadow-sm btn-block mb-3 d-flex justify-content-between" id="sidebarCollapse2">
              Back to Announcements
            </button>
            <ul class="list-group border bg-white">
              @foreach ($categories as $category)
                <a href="/announcement/{{ $category->slug }}" class="list-group-item btn">{{ $category->branch_name }}
                  <span class="badge badge-dark pull-xs-right">{{ $category->announcements->count() }}</span>
                </a>
              @endforeach
            </ul>
          </div>
        </div>
        <div class="col-lg-8" id="content">

          <h3 class="mb-4">Announcements</h3>
          <button type="button" id="sidebarCollapse" class="btn btn-dark d-block d-sm-block d-lg-none bg-white shadow-sm btn-block mb-3 d-flex justify-content-between">
              <span>Select Campus</span>
              <i class="fas fa-filter mt-1"></i>
          </button>
          @if (count($announcements) > 0)
            @foreach ($announcements as $announce)
              <div class="card mb-4 border">
                <div class="card-header text-right py-2 bg-navyblue">
                  <small class="badge badge-light p-2">{{$announce->category->branch_name}}</small>
                </div>
                <div class="card-body py-2">
                  <a href="/announcement/{{ $announce->id }}/{{ $announce->category->slug }}" class="nav-link pl-0">
                    <h6 class="mt-0 font-weight-bold text-navyblue">{{ $announce->title }}</h6>
                  </a>
                  <div class="d-flex justify-content-start">
                    @if ($announce->created_at == $announce->updated_at)
                      <p class="text-sm text-muted mb-0 pb-0">Published • {{date('M j, Y', strtotime($announce->created_at))}}</p>
                    @else
                      <p class="text-sm badge badge-info mb-0 py-1">Updated • {{date('M j, Y', strtotime($announce->updated_at))}}</p>
                    @endif
                  </div>
                  <p class="text-truncate">{!! $announce->content_brief !!}</p>
                </div>
              </div>
            @endforeach
          @else
            <div class="alert alert-warning text-center">
              <h4>No Announcement</h4>
            </div>
          @endif
        </div>
      </div>
    </div>
  </div>
  @include('shared.contact-button')
@endsection
