@extends('layouts.landing')

@section('title')
  Access | News
@endsection

@section('content')
<div id="header-news">
  <div class="container">
    <div class="text-center">
      <h1 class="text-white">Access News</h1>
    </div>
  </div>
</div>
<div class="container mt-5 pt-5">
  <a href="/news" class="text-dark"><i class="fas fa-angle-left mr-2"></i> Back to News</a>
  <div class="row mt-3">
    <div class="col-lg-9 mb-5">
      <div class="card border">
        <span class="badge bg-navyblue text-white m-2 p-2" style="position: absolute;">News</span>
        <img class="img-thumbnail" src="{{ $new->photo->file }}" width="100%">
        <div class="card-body">
          <ul class="list-inline">
            <li class="list-inline-item">
              @if ($new->created_at == $new->updated_at)
                <p class="text-sm text-muted mb-0 pb-0">Date posted - {{date('M j, Y', strtotime($new->created_at))}}</p>
              @else
                <p class="text-sm badge badge-info mb-0 py-1">Updated Post - {{date('M j, Y', strtotime($new->updated_at))}}</p>
              @endif
            </li>
            <li class="list-inline-item text-sm border px-3">{{ $new->campus }}</li>
          </ul>
          <h4 class="mt-3 mb-4">{{ $new->title }}</h4>
          <p>{!! $new->detail_extended !!}</p>
        </div>
      </div>
    </div>
    @include('shared.side-content')
  </div>
</div>
@include('shared.contact-button')
@endsection
