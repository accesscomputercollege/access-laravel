@extends('layouts.landing')

@section('title')
  Access | News
@endsection

@section('css')
  <style>
      .card-img-top{
        width: 100%;
        height: 15vw;
        height: 35vh;
        object-fit: cover;
      }
  </style>
@endsection

@section('content')
  <div id="header-news">
    <div class="container">
      <div class="text-center">
        <h1 class="text-white mt-5 pt-4">Our Latest News</h1>
      </div>
    </div>
  </div>
  <div class="container mb-5">
    <div class="title text-center my-5 pt-5">
      <h2>ACCESS COMPUTER COLLEGE NEWS</h2>
      <hr class="bg-danger accent-2 mt-0 d-inline-block mx-auto" style="width: 100px;">
    </div>

    <div class="row">
      @foreach ($new as $n)
        <div class="col-md-6 mb-4">
          <div class="card bg-white">
            <a class="text-dark" href="news/{{ $n->id }}">
            <img src="{{ $n->photo->file }}" class="card-img-top align-self-center d-block d-sm-block d-md-block rounded" alt="...">
            <div class="card-body">
              <h6 class="text-truncate mt-2">{{ $n->title }}</h6>
              <div class="campus">{{ $n->campus }}</div>
              <div class="date-status">
                @if ($n->created_at == $n->updated_at)
                  <p class="text-sm text-muted mb-1 pb-0"><b>Published</b> • {{date('M j, Y', strtotime($n->created_at))}}</p>
                @else
                  <p class="text-sm badge badge-info mb-1 py-1"><b>Updated Post</b> • {{date('M j, Y', strtotime($n->updated_at))}}</p>
                @endif
              </div>
              <p>{!! $n->detail_brief !!}</p>
            </div>

            </a>
          </div>
        </div>
      @endforeach
    </div>

    @if (count($news) > 0)
      <div class="row">
        @foreach ($news->all() as $key => $new)
          @if ($key > 1)
            <div class="col col-12 col-sm-12 col-md-6 col-lg-4 d-flex">
              <div class="card border mb-4">
                <a href="news/{{ $new->id }}"><img class="card-img-top" src="{{ $new->photo->file }}"></a>
                <div class="card-body">
                  <h6 class="text-dark mb-0 text-truncate">{{ $new->title }}</h6>
                  <div class="campus">{{ $new->campus }}</div>
                  <div class="date">
                    {{-- <span class="text-sm border mb-0 px-3 text-uppercase text-muted">{{ $new->campus }}</span></p> --}}
                    @if ($new->created_at == $new->updated_at)
                      <p class="text-sm text-muted mb-0"><b>Published</b> • {{date('M j, Y', strtotime($new->created_at))}}
                    @else
                      <p class="text-sm badge badge-info mb-0 py-1 mb-0"><b>Updated Post</b> • {{date('M j, Y', strtotime($new->updated_at))}}</p>
                    @endif
                  </div>
                  <p>{!! $new->detail_brief !!}</p>
                </div>
              </div>
            </div>
          @endif
        @endforeach
      </div>
      @else
        <p class="alert alert-warning">No News found</p>
    @endif
    <div class="pagination d-flex justify-content-center">
      {{ $news->links() }}
    </div>
  </div>
  @include('shared.contact-button')
@endsection
