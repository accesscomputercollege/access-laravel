@extends('layouts.landing')

@section('title')
  Access | Discover Access
@endsection

@section('content')
  <div id="header-discover">
    <div class="container">
      <div class="text-center">
        <h1 class="text-white">Discover Access</h1>
        <p>
          <a href="#history" class="text-light">HISTORY</a> &#8226;
          <a href="#vision-mission" class="text-light">VISION</a> &#8226;
          <a href="#vision-mission" class="text-light">MISSION</a> &#8226;
          <a href="#institution" class="text-light">INSTITUTION</a> &#8226;
          <a href="#curriculum" class="text-light">CURRICULUM</a> &#8226;
          <a href="#curriculum" class="text-light">FACULTY</a> &#8226;
          <a href="#curriculum" class="text-light">STUDENTS</a>
        </p>
      </div>
    </div>
  </div>

  <div class="container mt-5 pt-5" id="history">
    <div class="row pb-5 mb-5">
      <div class="col-md-6" id="row">
        <h2>
          <i class="fas fa-history text-navyblue"></i>
          <span class="border-underline">Our History</span>
        </h2>
        <p class="text-muted mt-2">
          The country's recognized leader and pioneer in High-tech education since 1981, continues its mission to offer the most in-demand courses wherein graduates are most in-demand today. To keep pace with the rapid advancement of technology, without sacrificing thorough training. The curriculum is geared to a world class excellence. Effective Training is achieved through personalized method of teaching in small-sized classes, with actual laboratory exercises and extensive hands-on practice, aided by high-tech audio - video devices and supplemented by scheduled on-the-job training.
        </p>
      </div>
      <div class="col-md-6">
        <img class="img-thumbnail" src="../images/discover-access/lagro_branch.jpg" width="100%" style="max-width: 500px;">
      </div>
    </div>
  </div>

  <div class="text-white bg-navyblue">
    <div class="container" id="vision-mission">
      <div class="row py-5 mb-5">
        <div class="col-md-6">
          <h2>
            <i class="fas fa-hands text-light"></i>
            <span class="text-light">Our Mission</span>
          </h2>
          <hr class="bg-danger accent-2 mt-0 d-inline-block mx-auto" style="width: 100px;">
          <p>To educate its students by means of curriculum grounded on practical application and technological integration along side the philosophy of the total academic, co and extra curricular experiences. We strive to prepare competent graduates aimed at changing the nation through competency and values, fulfilling its duty to our faculty, employees, students and society as true educational institution.</p>
        </div>
        <div class="col-md-6">
          <h2>
            <i class="fas fa-eye text-light"></i>
            <span class="text-light">Our Vision</span>
          </h2>
          <hr class="bg-danger accent-2 mt-0 d-inline-block mx-auto" style="width: 100px;">
          <p>To be the nation's premiere choice of school for tertiary level education and technical vocational training.</p>
        </div>
      </div>
    </div>
  </div>

  <div class="container" id="institution">
    <div class="row py-5">
      <div class="col-md-6 mb-2">
        <img class="img-thumbnail" src="../images/discover-access/pasig_kapasigan_branch.png" width="100%" style="max-width: 500px;">
      </div>
      <div class="col-md-6" id="row">
        <h2>
          <i class="fas fa-university text-navyblue"></i>
        <span class="border-underline">Our Institution</span>
        </h2>
        <p class="text-muted mt-2">Promotes synergy and harmony in diversity. As a non-sectarian, co-ed institution, the school becomes a catalyst for complete academic exchanges and learning perspectives.</p>
      </div>
    </div>
  </div>
  <div class="container" id="curriculum">
    <hr class="d-block d-sm-block d-md-none">
    <div class="row py-5 mb-5">
      <div class="col-md-4">
        <h2>
          <i class="fas fa-graduation-cap text-navyblue"></i>
          <span>Our Curriculum</span>
        </h2>
        <hr class="bg-danger accent-2 mt-0 d-inline-block mx-auto" style="width: 100px;">
        <p class="text-muted">Is finely crafted by its dedicated faculty. Sharpening students to proficiency at their chosen courses, each curriculum is carefully designed to induce rigorous training supplemented by ingenious practical application through hands on experience turning each student a real expert.</p>
      </div>
      <div class="col-md-4">
        <h2>
          <i class="fas fa-users text-navyblue"></i>
          <span>Our Faculty</span>
        </h2>
        <hr class="bg-danger accent-2 mt-0 d-inline-block mx-auto" style="width: 100px;">
        <p class="text-muted">Are the most competent and seriously dedicatedin the academic progress of its students. Mastery of the subject and technical expertisein conjunction with ingenious practical applicationscenarios continously challenge its students into striving for proficiency and excellence in their chosen fields.</p>
      </div>
      <div class="col-md-4">
        <h2>
          <i class="fas fa-user-graduate text-navyblue"></i>
          <span>Our Students</span>
        </h2>
        <hr class="bg-danger accent-2 mt-0 d-inline-block mx-auto" style="width: 100px;">
        <p class="text-muted">Develop to their full potential. Completely supported by the school through its faculty and staff, the delivery of the total academic, co-curricular, and extra curricular activities make our students outshine all others by being proficient in their chosen fields and as a fully integrated professional capable of adapting to different real world scenarios.</p>
      </div>
    </div>
  </div>
  @include('shared.contact-button')
@endsection
