<footer class="page-footer font-small bg-dark">
    <div class="bg-navyblue text-white">
      <div class="container">
        <!-- Grid row-->
        <div class="row py-3 d-flex align-items-center">
          <!-- Grid column -->
          <div class="col-md-6 col-lg-5 text-center text-md-left mb-4 mb-md-0">
            <h5 class="mb-0 text-light ">Get connected with us on social networks!</h5>
          </div>
          <!-- Grid column -->
          <div class="col-md-6 col-lg-7 text-center text-md-right">
            <!-- Facebook -->
            <a href="https://web.facebook.com/accesscomputercollege?_rdc=1&_rdr" target="_blank" class="fb-ic">
              <i class="fab fa-facebook text-white"> </i>
            </a>
            <!-- Twitter -->
            <a href="https://twitter.com/accesseduph/status/732117907954487296" target="_blank" class="tw-ic">
              <i class="fab fa-twitter text-white  mx-4"> </i>
            </a>
            <!-- instagram +-->
            <a href="https://www.instagram.com/explore/locations/706862043/access-computer-college-lagro" target="_blank" class="gplus-ic">
              <i class="fab fa-instagram text-white"> </i>
            </a>
          </div>
          <!-- Grid column -->
        </div>
        <!-- Grid row-->
      </div>
    </div>

    <!-- Footer Links -->
    <div class="container text-center text-md-left mt-5 text-white">
      <!-- Grid row -->
      <div class="row mt-3">
        <!-- Grid column -->
        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
          <!-- Content -->
          <h6 class=" text-light text-uppercase font-weight-bold">Access Computer College</h6>
          <hr class="bg-warning accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
          <p class="text-sm">The country's recognized leader and pioneer in High-tech education since 1981, continues its mission to offer the most in-demand courses wherein graduates are most in-demand today.</p>
        </div>
        <!-- Grid column -->
        <!-- Grid column -->
        <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
          <!-- Links -->
          <h6 class=" text-light text-uppercase font-weight-bold">Programs</h6>
          <hr class="bg-warning accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
          <p>
            <a href="/vocationals" class="nav-link p-0 text-light">Vocational</a>
          </p>
          <p>
            <a href="/collegiate" class="nav-link p-0 text-light">Collegiate</a>
          </p>
          <p>
            <a href="/shortcourses" class="nav-link p-0 text-light">Short Training</a>
          </p>
          <p>
            <a href="/senior-high" class="nav-link p-0 text-light">Senior High</a>
          </p>
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4 d-none d-md-block d-lg-block">

          <!-- Links -->
          <h6 class=" text-light text-uppercase font-weight-bold">Useful links</h6>
          <hr class="bg-warning accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
          <p>
            <a href="#!" class="nav-link p-0 text-light">Discover Access</a>
          </p>
          <p>
            <a href="#!" class="nav-link p-0 text-light">Admission</a>
          </p>
          <p>
            <a href="#!" class="nav-link p-0 text-light">Campuses</a>
          </p>

        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">

          <!-- Links -->
          <h6 class=" text-light text-uppercase font-weight-bold">Recognized By</h6>
          <hr class="bg-warning accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
          <ul class="list-inline">
            <li class="list-inline-item text-white" data-toggle="tooltip" data-placement="bottom" title="TESDA">
              <img src="../../images/government-logo/tesda-logo.png" width="30" alt="TESDA">
            </li>
            <li class="list-inline-item text-white" data-toggle="tooltip" data-placement="bottom" title="CHED">
              <img class="mx-2" src="../../images/government-logo/ched-logo.png" width="30" alt="CHED">
            </li>
            <li class="list-inline-item text-white" data-toggle="tooltip" data-placement="bottom" title="DEPED">
              <img src="../../images/government-logo/DepEd-logo.png" width="30" alt="DEPED">
            </li>
          </ul>
        </div>
        <!-- Grid column -->

      </div>
      <!-- Grid row -->

    </div>
    <!-- Footer Links -->

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3 text-white">
      <small>All rights reserved | Access Computer & Technical Colleges 2019 | <a class="text-info" href="#">Privacy Policy</a></small>
    </div>
    <!-- Copyright -->
  </footer>
  <!-- Footer -->
