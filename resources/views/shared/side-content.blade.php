<div class="col-lg-3 mb-5">
  <h5>Latest News</h5>
  <hr>
  @if (count($news) > 0)
    @foreach ($news as $new)
      <div class="row mb-2 border bg-white py-3 side-content">
        <dt class="col-4 pr-0">
          <a href="/news/{{ $new->id }}"><img class="img-side-card" src="{{ $new->photo->file }}" style="width:100%; height: 4.5rem; object-fit: cover;"></a>
        </dt>
        <dd class="col-8 pl-2 text-truncate">
          <a href="/news/{{ $new->id }}" class="text-dark-navy font-weight-bold text-sm">{{ $new->title }}</a>
          <p class="mb-0 text-muted" style="font-size: 12px;"><b>Published</b> • {{date('m-j-Y', strtotime($new->created_at))}}</p>
          <p class="mb-0 text-muted" style="font-size: 12px;">{{$new->campus}}</p>
        </dd>
      </div>
    @endforeach
    @else
      <p class="alert alert-warning">No News</p>
  @endif

  <h5 class="mt-5">Upcoming Events</h5>
  <hr>
  @if (count($events) > 0)
    @foreach ($events as $event)
      <div class="row no-gutters">
        <dt class="col-4">
          <ul class="list-unstyled text-center">
            <li class="py-1 text-white bg-navyblue"><small>{{date('D', strtotime($event->startdate))}}</small> </li>
            <li class="py-1 bg-white border"><small>{{date('M j', strtotime($event->startdate))}}</small> </li>
          </ul>
        </dt>
        <dd class="col-8 pl-3">
          <p class="mb-0 text-muted" style="font-size: 12px;">Date Posted - {{date('m.j.Y', strtotime($event->created_at))}}</p>
          <div class="text-truncate">
            <a href="/events/{{ $event->id }}" class="text-dark-navy font-weight-bold text-sm">{{ $event->title }}</a>
          </div>
          <p><small>All Campuses</small></p>
        </dd>
      </div>
    @endforeach
  @else
    <p class="alert alert-warning">No Events</p>
  @endif
</div>
