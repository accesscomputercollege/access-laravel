<div id="contact-button" class="mr-5 mb-4 d-none d-sm-none d-md-none d-lg-block" data-toggle="tooltip" data-placement="top" title="Access Helpdesk">
  <a href="/contact-us"><i id="contact-button-icon" class="fas fa-phone text-white bg-navyblue p-3 h5" style="border-radius: 30px;"></i></a>
</div>
