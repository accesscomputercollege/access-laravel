{{-- BOOTSTRAP MATERIAL --}}
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://unpkg.com/popper.js@1.12.6/dist/umd/popper.js" integrity="sha384-fA23ZRQ3G/J53mElWqVJEGJzU0sTs+SvzG8fXVWP+kJQ1lwFAOkcUOysnlKJC33U" crossorigin="anonymous"></script>
<script src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js" integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9" crossorigin="anonymous"></script>
<script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>

{{-- Navbar-drawer --}}
<script type="text/javascript">
  $(window).scroll(function() {
    $('nav').toggleClass('scrolled', $(this).scrollTop() > 50 );
  });
</script>

{{-- Tooltip --}}
<script type="text/javascript">
  $(function () {
  $('[data-toggle="tooltip"]').tooltip()
  })
</script>

<script type="text/javascript">
  $(document).ready(function () {
      $('#sidebarCollapse').on('click', function () {
          $('#sidebar').toggleClass('active');
          $('#content').toggleClass('active');
      });
  });

  $(document).ready(function () {
      $('#sidebarCollapse2').on('click', function () {
          $('#sidebar').toggleClass('active');
          $('#content').toggleClass('active');
      });
  });
</script>
