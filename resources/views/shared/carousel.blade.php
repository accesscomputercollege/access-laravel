<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-overlay"></div>
    <div class="carousel-item active">
      <div class="text-center" id="carousel1">
        <h1 class="carousel-title font-weight-bold text-shadow text-white display-2 d-none d-sm-none d-md-block">QUALITY EDUCATION <br> WITHIN REACH</h1>
        <h1 class="carousel-title font-weight-bold text-shadow text-white d-block d-sm-block d-md-none">QUALITY EDUCATION <br> WITHIN REACH</h1>
        <hr class="bg-danger accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 100px;">
      </div>
    </div>
    <div class="carousel-item">
      <div class="text-center" id="carousel2">
        <h1 class="carousel-title font-weight-bold text-shadow text-white display-2 d-none d-sm-none d-md-block">EARN YOUR <br> COLLEGE DEGREE</h1>
        <h1 class="carousel-title font-weight-bold text-shadow text-white d-block d-sm-block d-md-none">EARN YOUR COLLEGE DEGREE</h1>
        <hr class="bg-danger accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 100px;">
        <p>Change your life. Start Here.</p>
      </div>
    </div>
    <div class="carousel-item">
      <div class="text-center" id="carousel3">
        <h1 class="carousel-title font-weight-bold text-shadow text-white display-2 d-none d-sm-none d-md-block">EASY START PROGRAM</h1>
        <h1 class="carousel-title font-weight-bold text-shadow text-white d-block d-sm-block d-md-none">EASY START PROGRAM</h1>
        <hr class="bg-danger accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 100px;">
        <p>Real life. Real knowledge. Real People.</p>
      </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
