<header class="bmd-layout-header fixed-top">
  <nav class="navbar navbar-light bg-transparent navbar-header mb-2 py-2" style="box-shadow: none;">
    <div class="container" style="border-bottom: 1px solid rgba(255,255,255,.5);">
      <ul class="list-inline mb-2">
        <li class="list-inline-item text-white" data-toggle="tooltip" data-placement="bottom" title="TESDA">
          <img src="../../images/government-logo/tesda-logo.png" width="27" alt="TESDA">
        </li>
        <li class="list-inline-item text-white" data-toggle="tooltip" data-placement="bottom" title="CHED">
          <img class="mx-1" src="../../images/government-logo/ched-logo.png" width="27" alt="CHED">
        </li>
        <li class="list-inline-item text-white" data-toggle="tooltip" data-placement="bottom" title="DEPED">
          <img src="../../images/government-logo/DepEd-logo.png" width="27" alt="DEPED">
        </li>
        <li class="list-inline-item text-white">
          <small class="d-none d-sm-none d-md-block">SMART <i class="fas fa-mobile-alt ml-2 mr-1"></i> 0999-464-8124</small>
        </li>
        <li class="list-inline-item text-white">
          <small class="d-none d-sm-none d-md-block">GLOBE <i class="fas fa-mobile-alt ml-2 mr-1"></i> 0999-464-8124</small>
        </li>
        <li class="list-inline-item text-white">
          <small class="d-none d-sm-none d-md-block">SUN <i class="fas fa-mobile-alt ml-2 mr-1"></i> 0999-464-8124</small>
        </li>
      </ul>
      <div class="">
        <a class="navbar-item text-light" href="https://web.facebook.com/accesscomputercollege?_rdc=1&_rdr" target="_blank" data-toggle="tooltip" data-placement="bottom" title="Like us on Facebook">
          <i class="fab fa-facebook-square h5"></i>
        </a>
        <a class="navbar-item text-light mx-3" href="https://twitter.com/accesseduph/status/732117907954487296" target="_blank" data-toggle="tooltip" data-placement="bottom" title="Follow us on Twitter">
          <i class="fab fa-twitter-square h5"></i>
        </a>
        <a class="navbar-item text-light" href="https://www.instagram.com/explore/locations/706862043/access-computer-college-lagro" target="_blank" data-toggle="tooltip" data-placement="bottom" title="Follow us on Instagram">
          <i class="fab fa-instagram h5"></i>
        </a>
      </div>
    </div>
  </nav>

  <nav class="navbar navbar-expand-lg navbar-light topnav py-3" style="box-shadow: none;">
    <div class="container">
      <ul class="nav navbar-nav mr-auto">
        <li class="nav-item">
          <a href="/"><img src="../../images/accesslogo-brandname.png" width="100%" style="max-width: 280px;" alt="Access-logo-name"></a>
        </li>
      </ul>
      <div class="d-none d-lg-block">
        <ul class="nav navbar-nav">
          <li class="nav-item">
            <a href="/discover_access" class="nav-link text-white" style="font-size: 13px;">Discover Access</a>
          </li>
          <li class="nav-item">
            <a href="/admission" class="nav-link text-white" style="font-size: 13px;">Admission</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle text-white" href="#" style="font-size: 13px;" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Programs
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="/vocationals"><small>Vocational Programs</small></a>
              <a class="dropdown-item" href="/collegiate"><small>Collegiate Programs</small></a>
              <a class="dropdown-item" href="/shortcourses"><small>Short Training Programs</small></a>
              <a class="dropdown-item" href="/senior-high"><small>Senior High School</small></a>
            </div>
          </li>
          <li class="nav-item">
            <a href="/campuses" class="nav-link text-white" style="font-size: 13px;">Campuses</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle text-white mr-2" style="font-size: 13px;" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              MORE
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="/news"><small><i class="far fa-newspaper mr-2"></i>Access News</small></a>
              <a class="dropdown-item" href="/announcements"><small><i class="fas fa-bullhorn mr-2"></i>Announcements</small></a>
              <a class="dropdown-item" href="/events"><small><i class="far fa-calendar-check mr-2"></i>Upcoming Events</small></a>
            </div>
          </li>
          <li class="nav-item border px-3">
            <a href="/student-login" target="_blank" class="nav-link text-white" style="font-size: 13px;" data-toggle="tooltip" data-placement="bottom" title="Student Login">LOGIN</a>
          </li>
        </ul>
    </div>
    <a class="d-block d-lg-none" data-toggle="drawer" data-target="#dw-p1">
      <i class="fas fa-list text-white" style="font-size: 22px;"></i>
    </a>
    </div>
  </nav>
</header>

{{-- Sidenav --}}
<div id="dw-p1" class="bmd-layout-drawer bg-light bg-faded" style="position: fixed;">
  <header id="sidenav">
    <a href="/" class="navbar-brand pl-0">
      <img src="../../images/access-logo.png" width="100%" style="max-width: 200px;" alt="Access-logo-name">
    </a>
  </header>
  <ul class="list-group" id="sidenav">
    <div class="text-center mx-2 mb-3">
      <a href="/student-login" class="btn bg-navyblue btn-block text-white py-2">LOGIN</a>
    </div>
    <a href="/" class="list-group-item btn text-white font-weight-normal pl-4">
      <i class="fas fa-home mr-4"></i>
      Home
    </a>
    <a href="/discover_access" class="list-group-item btn text-white font-weight-normal pl-4">
      <i class="fab fa-discourse mr-4"></i>
      Discover Access
    </a>

    <a href="/admission" class="list-group-item btn text-white font-weight-normal pl-4"><i class="far fa-check-circle mr-4"></i> Admission</a>

    <a href="/campuses" class="list-group-item btn text-white font-weight-normal pl-4"><i class="fas fa-university mr-4"></i> Campuses</a>

    <a data-toggle="collapse" href="#programs" class="list-group-item btn text-white font-weight-normal pl-4"><i class="fas fa-book mr-4"></i> Programs</a>
    <div class="collapse" id="programs">
      <ul class="list-group ml-4">
        <a href="/vocationals" class="list-group-item pt-0 text-white font-weight-normal">Vocational</a>
        <a href="/collegiate" class="list-group-item text-white font-weight-normal">Collegiate</a>
        <a href="/shortcourses" class="list-group-item text-white font-weight-normal">Short Training</a>
        <a href="/senior-high" class="list-group-item text-white font-weight-normal">Senior High School</a>
      </ul>
    </div>

    <a data-toggle="collapse" href="#MORE" class="list-group-item btn text-white font-weight-normal pl-4"><i class="fas fa-chevron-circle-down mr-4"></i> MORE</a>
    <div class="collapse" id="MORE">
      <ul class="list-group ml-4">
        <a href="/news" class="list-group-item pt-0 text-white font-weight-normal">Access News</a>
        <a href="/announcements" class="list-group-item text-white font-weight-normal">Announcements</a>
        <a href="/events" class="list-group-item text-white font-weight-normal">Upcoming Events</a>
      </ul>
    </div>
    <div class="text-center mx-2 mt-5">
      <a href="/contact-us" class="btn btn-outline-light btn-block text-white py-3">Access Helpdesk</a>
    </div>
  </ul>
</div>
