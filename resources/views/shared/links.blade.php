<link rel="icon" href="../../images/access-logo-icon.png">
<link rel="stylesheet" href="../../css/main.css">
<link rel="stylesheet" href="../../css/style.css">
<link rel="stylesheet" href="../../css/pageloader.css">

{{-- BOOTSTRAP MATERIAL --}}
<link rel="stylesheet" href="https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css" integrity="sha384-wXznGJNEXNG1NFsbm0ugrLFMQPWswR3lds2VeinahP8N0zJw9VWSopbjv2x7WCvX" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=Montserrat:100,300,400,500,600" rel="stylesheet">
