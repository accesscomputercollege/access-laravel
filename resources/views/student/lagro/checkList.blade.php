@extends('layouts.student-lagro')

@section('title')
Acad Performance • Check List
@endsection

@section('main-content')
  <div class="card">
      <div class="card-header text-center">
          <h2 class="card-title"><img src="../../images/accesslogo-brandname.png" width="280"></h2>
          <small class="card-subtitle">STUDENT ACADEMIC EVALUATION</small>
      </div>

      <div class="card-block">
        <div class="row no-gutters">
          <div class="col-md-6">
            <dl class="row">
              <dt class="col-3">Student_ID:</dt>
              <dd class="col-9">{{ Auth::user()->studentid }}</dd>
              <dt class="col-3">Name:</dt>
              <dd class="col-9">{{ Auth::user()->firstname }} {{ Auth::user()->middlename }} {{ Auth::user()->lastname }}</dd>
              <dt class="col-3">Course:</dt>
              <dd class="col-9">{{ Auth::user()->course }}</dd>
            </dl>
          </div>
          <div class="col-md-6">
            <dl class="row">
              <dt class="col-3">Year:</dt>
              <dd class="col-9">4th Year</dd>
              <dt class="col-3">Status:</dt>
              <dd class="col-9">Regular</dd>
              <dt class="col-3">Major:</dt>
              <dd class="col-9">{{ Auth::user()->major }}</dd>
            </dl>
          </div>
        </div>
        <hr>
        <label>1st Year - First Semester</label>
        <div class="table-responsive">
          <table class="table">
              <thead  class="thead-inverse">
              <tr>
                  <th>SubjectCode</th>
                  <th>SubjectName</th>
                  <th>Grades</th>
                  <th>Remarks</th>
                  <th>Instructor</th>
              </tr>
              </thead>
              <tbody>
                @foreach ($grades as $grade)
                  <tr>
                      <th>{{ $grade->subject_code }}</th>
                      <td>{{ $grade->subject_name }}</td>
                      <td>{{ $grade->grade }}</td>
                      <td>{{ $grade->remark }}</td>
                      <td>{{ $grade->instructor }}</td>
                  </tr>
                @endforeach
              </tbody>
          </table>
        </div>
        <label class="mt-4">1st Year - Second Semester</label>
        <div class="table-responsive">
          <table class="table">
              <thead  class="thead-inverse">
                <tr>
                    <th>SubjectCode</th>
                    <th>SubjectName</th>
                    <th>Grades</th>
                    <th>Remarks</th>
                    <th>Instructor</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($grades2 as $grade)
                  <tr>
                      <th>{{ $grade->subject_code }}</th>
                      <td>{{ $grade->subject_name }}</td>
                      <td>{{ $grade->grade }}</td>
                      <td>{{ $grade->remark }}</td>
                      <td>{{ $grade->instructor }}</td>
                  </tr>
                @endforeach
              </tbody>
          </table>
        </div>

        <label class="mt-4">2nd Year - First Semester</label>
        <div class="table-responsive">
          <table class="table">
              <thead  class="thead-inverse">
                <tr>
                    <th>SubjectCode</th>
                    <th>SubjectName</th>
                    <th>Grades</th>
                    <th>Remarks</th>
                    <th>Instructor</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($grades3 as $grade)
                  <tr>
                      <th>{{ $grade->subject_code }}</th>
                      <td>{{ $grade->subject_name }}</td>
                      <td>{{ $grade->grade }}</td>
                      <td>{{ $grade->remark }}</td>
                      <td>{{ $grade->instructor }}</td>
                  </tr>
                @endforeach
              </tbody>
          </table>
        </div>

        <label class="mt-4">2nd Year - Second Semester</label>
        <div class="table-responsive">
          <table class="table">
              <thead  class="thead-inverse">
                <tr>
                    <th>SubjectCode</th>
                    <th>SubjectName</th>
                    <th>Grades</th>
                    <th>Remarks</th>
                    <th>Instructor</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($grades4 as $grade)
                  <tr>
                      <th>{{ $grade->subject_code }}</th>
                      <td>{{ $grade->subject_name }}</td>
                      <td>{{ $grade->grade }}</td>
                      <td>{{ $grade->remark }}</td>
                      <td>{{ $grade->instructor }}</td>
                  </tr>
                @endforeach
              </tbody>
          </table>
        </div>

        <label class="mt-4">3rd Year - First Semester</label>
        <div class="table-responsive">
          <table class="table">
              <thead  class="thead-inverse">
                <tr>
                    <th>SubjectCode</th>
                    <th>SubjectName</th>
                    <th>Grades</th>
                    <th>Remarks</th>
                    <th>Instructor</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($grades5 as $grade)
                  <tr>
                      <th>{{ $grade->subject_code }}</th>
                      <td>{{ $grade->subject_name }}</td>
                      <td>{{ $grade->grade }}</td>
                      <td>{{ $grade->remark }}</td>
                      <td>{{ $grade->instructor }}</td>
                  </tr>
                @endforeach
              </tbody>
          </table>
        </div>

        <label class="mt-4">3rd Year - Second Semester</label>
        <div class="table-responsive">
          <table class="table">
              <thead  class="thead-inverse">
                <tr>
                    <th>SubjectCode</th>
                    <th>SubjectName</th>
                    <th>Grades</th>
                    <th>Remarks</th>
                    <th>Instructor</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($grades6 as $grade)
                  <tr>
                      <th>{{ $grade->subject_code }}</th>
                      <td>{{ $grade->subject_name }}</td>
                      <td>{{ $grade->grade }}</td>
                      <td>{{ $grade->remark }}</td>
                      <td>{{ $grade->instructor }}</td>
                  </tr>
                @endforeach
              </tbody>
          </table>
        </div>

        <label class="mt-4">4th Year - First Semester</label>
        <div class="table-responsive">
          <table class="table">
              <thead  class="thead-inverse">
                <tr>
                    <th>SubjectCode</th>
                    <th>SubjectName</th>
                    <th>Grades</th>
                    <th>Remarks</th>
                    <th>Instructor</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($grades7 as $grade)
                  <tr>
                      <th>{{ $grade->subject_code }}</th>
                      <td>{{ $grade->subject_name }}</td>
                      <td>{{ $grade->grade }}</td>
                      <td>{{ $grade->remark }}</td>
                      <td>{{ $grade->instructor }}</td>
                  </tr>
                @endforeach
              </tbody>
          </table>
        </div>

        <label class="mt-4">4th Year - Second Semester</label>
        <div class="table-responsive">
          <table class="table">
              <thead  class="thead-inverse">
                <tr>
                    <th>SubjectCode</th>
                    <th>SubjectName</th>
                    <th>Grades</th>
                    <th>Remarks</th>
                    <th>Instructor</th>
                </tr>
              </thead>
              <tbody>
                  @foreach ($grades8 as $grade)
                    <tr>
                        <th>{{ $grade->subject_code }}</th>
                        <td>{{ $grade->subject_name }}</td>
                        <td>{{ $grade->grade }}</td>
                        <td>{{ $grade->remark }}</td>
                        <td>{{ $grade->instructor }}</td>
                    </tr>
                  @endforeach
              </tbody>
          </table>
        </div>
      </div>
  </div>
@endsection
