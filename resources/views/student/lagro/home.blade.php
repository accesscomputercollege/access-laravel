@extends('layouts.student-lagro')

@section('title')
  My Personal Page
@endsection

@section('main-content')
  <div class="card pb-5 col-md-10 offset-md-1 mt-5">
      <div class="card-header text-center">
          <img src="../../images/access-logo-icon.png" width="80">
          <h2 class="card-title mt-4">Welcome <span class="text-uppercase">{{ Auth::user()->firstname }}, {{ Auth::user()->lastname }}</span> to your personal page!</h2>
      </div>
      <div class="card-block">
        <p class="mt-4">Please note that every activity is monitored closely. For any problem in the system, contact System Administrator for details. It is recommended to logout by clicking the logout button everytime you leave your PC.</p>
        <h6 class="mt-5">If you do not agree with the conditions or you are not <span class="text-uppercase">{{ Auth::user()->studentid }}</span> please logout.</h6>
      </div>
  </div>
@endsection
