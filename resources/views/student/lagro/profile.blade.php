@extends('layouts.student-lagro')

@section('title')
  <h1>{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</h1>
  <small>Student ID • {{ Auth::user()->studentid }}</small>
@endsection

@section('main-content')
  <div class="row justify-content-center">
    <div class="col-md-10">
      <div class="card profile mt-5">
          <div class="profile__img">
              <img class="w-100" src="{{ Auth::user()->photo ? Auth::user()->photo->file : 'http://placehold.it/300x300' }}" alt="">
          </div>

          <div class="profile__info">
              <p>I'm a {{ Auth::user()->course }} student from Access Computer College, Lagro Quezon City Branch.</p>

              <ul class="icon-list">
                  <li><i class="zmdi zmdi-phone"></i> {{ Auth::user()->mobile ? Auth::user()->mobile : 'None' }}</li>
                  <li><i class="zmdi zmdi-email"></i> {{ Auth::user()->email }}</li>
                  <li><i class="zmdi zmdi-account-calendar"></i> {{ Auth::user()->studentid }}</li>
              </ul>
          </div>
      </div>

      <div class="card">
          <div class="card-block">
              <h4 class="card-block__title mb-4">About {{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</h4>
              <div class="row">
                <div class="col-md-3 mb-2">
                  <p><b>Campus</b> <br>Lagro, Quezon City Campus</p>
                </div>
                <div class="col-md-5 mb-2">
                  <p><b>Course</b> <br>{{ Auth::user()->course }}</p>
                </div>
                <div class="col-md-3 mb-2">
                  <p><b>Major</b> <br> {{ Auth::user()->major ? Auth::user()->major : 'None' }}</p>
                </div>
              </div>

              <hr class="my-4">

              <h4 class="card-block__title mb-4">Address</h4>
              <p>{{ Auth::user()->street_number }} {{ Auth::user()->street }} {{ Auth::user()->subdivision_village_building }} {{ Auth::user()->barangay }} {{ Auth::user()->city }}</p>

              <hr class="my-4">

              <h4 class="card-block__title mb-4">Contact Information</h4>
              <ul class="icon-list">
                  <li><i class="zmdi zmdi-phone"></i>{{ Auth::user()->telephone ? Auth::user()->telephone : 'None' }}</li>
                  <li><i class="zmdi zmdi-email"></i>{{ Auth::user()->email }}</li>
                  <li><i class="zmdi zmdi-facebook"></i>{{ Auth::user()->facebook ? Auth::user()->facebook : 'None' }}</li>
              </ul>

              <hr class="my-4">

              <h4 class="card-block__title mb-4">Basic Information</h4>
              <dl class="row">
                <dt class="col-6 text-muted">Gender</dt>
                <dd class="col-6 text-capitalize">{{ Auth::user()->gender }}</dd>

                <dt class="col-6 text-muted">Status</dt>
                <dd class="col-6 text-capitalize">{{ Auth::user()->status }}</dd>

                <dt class="col-6 text-muted">Citizenship</dt>
                <dd class="col-6 text-capitalize">{{ Auth::user()->citizenship }}</dd>

                <dt class="col-6 text-muted">Birthplace</dt>
                <dd class="col-6 text-capitalize">{{ Auth::user()->birthplace }}</dd>

                <dt class="col-6 text-muted">Religion</dt>
                <dd class="col-6 text-capitalize">{{ Auth::user()->religion }}</dd>

                <dt class="col-6 text-muted">Date of Birth</dt>
                <dd class="col-6 text-capitalize">{{ Auth::user()->date_of_birth }}</dd>
              </dl>
          </div>
      </div>
    </div>
  </div>

@endsection
