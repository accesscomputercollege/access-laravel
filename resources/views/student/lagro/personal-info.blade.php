@extends('layouts.student-lagro')

@section('title')
  Personal Information
@endsection

@section('main-content')
  <div class="card personal-info">
    <div class="card-header text-center">
        <h2 class="card-title"><img src="../../images/accesslogo-brandname.png" width="300"></h2>
        <small class="card-subtitle">GENERAL STUDENT PERSONAL INFORMATION SHEET</small>
    </div>
    <div class="card-block">
      <div class="table-responsive">
        <table class="table table-sm table-bordered mb-3">
          <div class="bg-grey text-white text-center py-2 px-1">STUDENT'S INFORMATION</div>
          <thead>
            <tr>
                <th>Campus</th>
                <th>Course</th>
                <th>Major</th>
            </tr>
          </thead>
          <tbody class="text-muted text-uppercase">
            <tr>
                <td>Access Lagro</td>
                <td>{{ Auth::user()->course }}</td>
                <td>{{ Auth::user()->major }}</td>
            </tr>
          </tbody>
          <thead>
            <tr>
                <th>Firstname</th>
                <th>Lastname</th>
                <th>Middlename</th>
            </tr>
          </thead>
          <tbody class="text-muted text-uppercase">
            <tr>
                <td>{{ Auth::user()->firstname }}</td>
                <td>{{ Auth::user()->lastname }}</td>
                <td>{{ Auth::user()->middlename }}</td>
            </tr>
          </tbody>
          <thead>
            <tr>
                <th>Gender</th>
                <th>Status</th>
                <th>Citizenship</th>
            </tr>
          </thead>
          <tbody class="text-muted text-uppercase">
            <tr>
                <td>{{ Auth::user()->gender }}</td>
                <td>{{ Auth::user()->status }}</td>
                <td>{{ Auth::user()->citizenship }}</td>
            </tr>
          </tbody>
          <thead>
            <tr>
                <th>Birthplace</th>
                <th>Religion</th>
                <th>Date of Birth</th>
            </tr>
          </thead>
          <tbody class="text-muted text-uppercase">
            <tr>
                <td>{{ Auth::user()->birthplace }}</td>
                <td>{{ Auth::user()->religion }}</td>
                <td>{{ Auth::user()->date_of_birth }}</td>
            </tr>
          </tbody>
        </table>
      </div>

      <div class="table-responsive">
        <table class="table table-sm table-bordered mb-3">
          <div class="bg-grey text-white text-center py-2 px-1">CURRENT ADDRESS</div>
          <thead>
            <tr>
                <th>Street No. / Unit No</th>
                <th>Street</th>
                <th>Subdivision / Village / Building</th>
            </tr>
          </thead>
          <tbody class="text-muted text-uppercase">
            <tr>
                <td>{{ Auth::user()->street_number }}</td>
                <td>{{ Auth::user()->street }}</td>
                <td>{{ Auth::user()->subdivision_village_building }}</td>
            </tr>
          </tbody>
          <thead>
            <tr>
                <th>Barangay</th>
                <th>City / Municipality</th>
                <th>Province</th>
            </tr>
          </thead>
          <tbody class="text-muted text-uppercase">
            <tr>
                <td>{{ Auth::user()->barangay }}</td>
                <td>{{ Auth::user()->city }}</td>
                <td>{{ Auth::user()->province }}</td>
            </tr>
          </tbody>
        </table>
      </div>

      <div class="table-responsive my-5">
        <table class="table table-sm table-bordered my-5">
          <div class="bg-grey text-white text-center py-2 px-1">CONTACT DETAILS</div>
          <thead>
            <tr>
                <th>Telephone No.</th>
                <th>Mobile No.</th>
                <th>Email Address</th>
            </tr>
          </thead>
          <tbody class="text-muted text-uppercase">
            <tr>
                <td>{{ Auth::user()->telephone ? Auth::user()->telephone : 'None' }}</td>
                <td>{{ Auth::user()->mobile }}</td>
                <td>{{ Auth::user()->email }}</td>
            </tr>
          </tbody>
        </table>
      </div>


      <div class="table-responsive mt-5">
        <table class="table table-sm table-bordered mb-0">
          <div class="bg-grey text-white text-center py-2 px-1 mt-5">CURRENT OR LAST SCHOOL ATTENDED</div>
          <thead>
            <tr>
                <th>School Type</th>
                <th>Name of School</th>
            </tr>
          </thead>
          <tbody class="text-muted text-uppercase">
            <tr>
                <td>{{ Auth::user()->school_type }}</td>
                <td>{{ Auth::user()->school_name }}</td>
            </tr>
          </tbody>
        </table>
        <table class="table table-sm table-bordered mb-3">
          <thead>
            <tr>
                <th>Date of Graduation</th>
                <th>School Year</th>
                <th>Year / Grade</th>
                <th>Term</th>
            </tr>
          </thead>
          <tbody class="text-muted text-uppercase">
            <tr>
                <td>{{ Auth::user()->grad_date ? Auth::user()->grad_date : 'None' }}</td>
                <td>{{ Auth::user()->school_year ? Auth::user()->school_year : 'None' }}</td>
                <td>{{ Auth::user()->year_grade ? Auth::user()->year_grade : 'None' }}</td>
                <td>{{ Auth::user()->term ? Auth::user()->term : 'None' }}</td>
            </tr>
          </tbody>
        </table>
      </div>

      <div class="table-responsive">
        <table class="table table-sm table-bordered mb-3">
          <div class="bg-grey text-white text-center py-2 px-1">PARENTS / GUARDIAN'S INFORMATION</div>
          <h6 class="mt-4">Father's Information</h6>
          <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Middle Initial</th>
            </tr>
          </thead>
          <tbody class="text-muted text-uppercase">
            <tr>
                <td>{{ Auth::user()->f_firstname }}</td>
                <td>{{ Auth::user()->f_lastname }}</td>
                <td>{{ Auth::user()->f_initial }}</td>
            </tr>
          </tbody>
          <thead>
            <tr>
                <th>Mobile Number</th>
                <th>Email</th>
                <th>Occupation</th>
            </tr>
          </thead>
          <tbody class="text-muted text-uppercase">
            <tr>
                <td>{{ Auth::user()->f_mobile ? Auth::user()->f_mobile : 'None' }}</td>
                <td>{{ Auth::user()->f_email ? Auth::user()->f_email : 'None' }}</td>
                <td>{{ Auth::user()->f_occupation ? Auth::user()->f_occupation: 'None' }}</td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="table-responsive">
        <table class="table table-sm table-bordered mb-3">
          <h6 class="mt-4">Mother's Information</h6>
          <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Middle Initial</th>
            </tr>
          </thead>
          <tbody class="text-muted text-uppercase">
            <tr>
                <td>{{ Auth::user()->m_firstname }}</td>
                <td>{{ Auth::user()->m_lastname }}</td>
                <td>{{ Auth::user()->m_initial }}</td>
            </tr>
          </tbody>
          <thead>
            <tr>
                <th>Mobile Number</th>
                <th>Email</th>
                <th>Occupation</th>
            </tr>
          </thead>
          <tbody class="text-muted text-uppercase">
            <tr>
                <td>{{ Auth::user()->m_mobile ? Auth::user()->m_mobile : 'None' }}</td>
                <td>{{ Auth::user()->m_email ? Auth::user()->m_email : 'None' }}</td>
                <td>{{ Auth::user()->m_occupation ? Auth::user()->m_occupation: 'None' }}</td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="table-responsive">
        <table class="table table-sm table-bordered mb-3">
          <h6 class="mt-4">Guardian's Information</h6>
          <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Middle Initial</th>
                <th>Suffix</th>
            </tr>
          </thead>
          <tbody class="text-muted text-uppercase">
            <tr>
                <td>{{ Auth::user()->g_firstname }}</td>
                <td>{{ Auth::user()->g_lastname }}</td>
                <td>{{ Auth::user()->g_initial }}</td>
                <td>{{ Auth::user()->g_suffix }}</td>
            </tr>
          </tbody>
          <thead>
            <tr>
                <th>Mobile Number</th>
                <th>Email</th>
                <th>Occupation</th>
                <th>Relationship</th>
            </tr>
          </thead>
          <tbody class="text-muted text-uppercase">
            <tr>
                <td>{{ Auth::user()->g_mobile ? Auth::user()->g_mobile : 'None' }}</td>
                <td>{{ Auth::user()->g_email ? Auth::user()->g_email : 'None' }}</td>
                <td>{{ Auth::user()->g_occupation ? Auth::user()->g_occupation: 'None' }}</td>
                <td>{{ Auth::user()->g_relationship }}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <button class="btn btn-danger btn--action btn--fixed " data-ma-action="print"><i class="zmdi zmdi-print"></i></button>
@endsection
