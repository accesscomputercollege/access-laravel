@extends('layouts.student-camarin')

@section('title')
  <h1>{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</h1>
  <small>Student ID • {{ Auth::user()->studentid }}</small>
@endsection

@section('main-content')
  <div class="row justify-content-center">
    <div class="col-md-10">
      <div class="card profile mt-5">
          <div class="profile__img">
              <img src="../../student_theme/demo/img/contacts/2.jpg" alt="">
          </div>

          <div class="profile__info">
              <p>Cras mattis consectetur purus sit amet fermentum. Maecenas sed diam eget risus varius blandit sit amet non magnae tiam porta sem malesuada magna mollis euismod.</p>

              <ul class="icon-list">
                  <li><i class="zmdi zmdi-phone"></i> 308-360-8938</li>
                  <li><i class="zmdi zmdi-email"></i> {{ Auth::user()->email }}</li>
                  <li><i class="zmdi zmdi-account-calendar"></i> {{ Auth::user()->studentid }}</li>
              </ul>
          </div>
      </div>

      <div class="card">
          <div class="card-block">
              <h4 class="card-block__title mb-4">About {{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</h4>

              <p><b>Campus</b> <br>Access Computer College Camarin Campus</p>
              <p><b>Course</b> <br>Bachelor of Science Information Technology</p>
              <p><b>Major</b> <br> {{ Auth::user()->major ? Auth::user()->major : 'None' }}</p>

              <br>

              <h4 class="card-block__title mb-4">Contact Information</h4>

              <ul class="icon-list">
                  <li><i class="zmdi zmdi-phone"></i>308-360-8938</li>
                  <li><i class="zmdi zmdi-email"></i>robertbosborne@inbound.plus</li>
                  <li><i class="zmdi zmdi-facebook"></i>robertbosborne</li>
              </ul>
          </div>
      </div>
    </div>
  </div>

@endsection
