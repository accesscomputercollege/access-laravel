@extends('layouts.student-camarin')

@section('title')
  Personal Information
@endsection

@section('main-content')
  <div class="card personal-info">
    <div class="card-header text-center">
        <h2 class="card-title"><img src="../../images/accesslogo-brandname.png" width="300"></h2>
        <small class="card-subtitle">GENERAL STUDENT PERSONAL INFORMATION SHEET</small>
    </div>
    <div class="card-block">
      <div class="card-block">
        <table class="table table-sm mb-0">
          <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Username</th>
            </tr>
          </thead>
          <tbody class="text-muted text-uppercase">
            <tr>
                <td>Mark</td>
                <td>Otto</td>
                <td>@mdo</td>
            </tr>
          </tbody>
          <thead>
            <tr>
                <th>Gender</th>
                <th>Status</th>
                <th>Citizenship</th>
            </tr>
          </thead>
          <tbody class="text-muted text-uppercase">
            <tr>
                <td>Male</td>
                <td>Single</td>
                <td>Filipino</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <button class="btn btn-danger btn--action btn--fixed " data-ma-action="print"><i class="zmdi zmdi-print"></i></button>
@endsection
