@extends('layouts.student-camarin')

@section('title')
Change Password
@endsection

@section('main-content')
<div class="row justify-content-center">
    <div class="col-md-9">
        <div class="card">
            <div class="card-header mb-0">
                <h2 class="card-title">Change your password here</h2>
                @if (session('error'))
                <div class="alert alert-danger mt-4 mb-0">
                    {{ session('error') }}
                </div>
                @endif
                @if (session('success'))
                <div class="alert alert-success mt-4 mb-0">
                    {{ session('success') }}
                </div>
                @endif
            </div>

            <div class="card-block">
                <form class="form-horizontal" method="POST" action="{{ route('camarin.changePassword') }}">
                    {{ csrf_field() }}

                    <div class="form-group form-group--float {{ $errors->has('current-password') ? ' has-danger' : '' }}">
                        <input type="password" class="form-control" name="current-password" required>
                        <label>Current Password</label>
                        <i class="form-group__bar"></i>
                    </div>
                    <div class="form-group form-group--float {{ $errors->has('new-password') ? 'has-danger' : '' }}">
                        <input type="password" class="form-control" name="new-password" required>
                        @if ($errors->has('new-password'))
                            <label class="form-control-label">{{ $errors->first('new-password') }}</label>
                          @else
                            <label class="form-control-label">New Password</label>
                        @endif
                        <i class="form-group__bar"></i>
                    </div>
                    <div class="form-group form-group--float">
                        <input type="password" class="form-control" name="new-password_confirmation" required>
                        <label>Confirm New Password</label>
                        <i class="form-group__bar"></i>
                    </div>

                    <div class="text-center">
                      <button type="submit" class="btn btn-outline-primary mt-4 mb-3">
                          Change Password
                      </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
