<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
  protected $table = 'announcements';
  public $primary = 'id';
  public $timestamps = true;

  public function category() {
    return $this->belongsTo(Category::class);
  }
}
