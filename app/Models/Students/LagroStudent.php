<?php

namespace App\Models\Students;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class LagroStudent extends Authenticatable
{
    use Notifiable;

    protected $guard = 'lagro';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        // Student's Info
        'firstname', 'lastname', 'middlename', 'studentid', 'email', 'password', 'photo_id', 'course', 'major', 'student_status', 'section', 'gender', 'status', 'citizenship',  'birthplace', 'religion', 'date_of_birth',
         // Address
        'street_number', 'street', 'subdivision_village_building', 'barangay', 'city', 'province', 'zip_code',
        // Contact details
        'telephone', 'mobile',
        // Current or Last School Attended
        'school_type', 'school_name', 'grad_date', 'school_year', 'year_grade', 'term',
        // Father's Info
        'f_firstname', 'f_lastname', 'f_initial', 'f_suffix', 'f_mobile', 'f_email', 'f_occupation',
        // Mother's Info
        'm_firstname', 'm_lastname', 'm_initial', 'm_suffix', 'm_mobile', 'm_email', 'm_occupation',
        // Guardian's Info
        'g_firstname', 'g_lastname', 'g_initial', 'g_suffix', 'g_mobile', 'g_email', 'g_occupation', 'g_relationship',
        // Grades

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function photo() {
      return $this->belongsTo('App\Photo');
    }

    public function grades() {
      return $this->hasMany('App\Grade');
    }

    public function grades2() {
      return $this->hasMany('App\Grade2');
    }

    public function grades3() {
      return $this->hasMany('App\Grade3');
    }

    public function grades4() {
      return $this->hasMany('App\Grade4');
    }

    public function grades5() {
      return $this->hasMany('App\Grade5');
    }

    public function grades6() {
      return $this->hasMany('App\Grade6');
    }

    public function grades7() {
      return $this->hasMany('App\Grade7');
    }

    public function grades8() {
      return $this->hasMany('App\Grade8');
    }
}
