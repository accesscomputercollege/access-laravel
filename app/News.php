<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
  protected $fillable = ['title', 'campus', 'branch_name', 'detail_brief', 'detail_extended', 'photo_id'];

  protected $table = 'news';
  public $primary = 'id';
  public $timestamps = true;

  public function photo() {
    return $this->belongsTo('App\Photo');
  }

  public function campus() {
    return $this->belongsTo(Category::class);
  }

}
