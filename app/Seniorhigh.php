<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seniorhigh extends Model
{
  protected $table = 'seniorhigh';
  public $primary = 'id';
  public $timestamps = true;

  public function campuses() {
    return $this->belongsToMany(Category::class);
  }
}
