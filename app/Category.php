<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
  protected $table = 'categories';
  public $primary = 'id';
  public $timestamps = true;

  protected $fillable = ['photo_id', 'branch_name', 'slug', 'address', 'telephone', 'mobile', 'facebook', 'map', 'email'];

  public function announcements() {
    return $this->hasMany(Announcement::class);
  }

  public function getRouteKeyName() {
    return 'slug';
  }

  public function photo() {
    return $this->belongsTo('App\Photo');
  }

  public function vocationals() {
    return $this->belongsToMany(Vocational::class);
  }

  public function collegiate() {
    return $this->belongsToMany(Collegiate::class);
  }

  public function shortcourses() {
    return $this->belongsToMany(Shortcourse::class);
  }

  public function seniorhigh() {
    return $this->belongsToMany(Seniorhigh::class);
  }
}
