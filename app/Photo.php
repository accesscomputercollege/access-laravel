<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{

  protected $uploads = '/cover_images/';

  protected $fillable = ['file'];

  public function getFileAttribute($photo) {
    return $this->uploads . $photo;
  }
}
