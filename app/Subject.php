<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
  protected $table = 'subjects';
  public $primary = 'id';
  public $timestamps = true;
}
