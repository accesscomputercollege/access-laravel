<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    public $fullname, $message, $sender, $subject, $location, $contact_no;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($fullname, $message, $sender, $subject, $location, $contact_no)
    {
      $this->fullname = $fullname;
      $this->message = $message;
      $this->sender = $sender;
      $this->subject = $subject;
      $this->location = $location;
      $this->contact_no = $contact_no;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      $e_fullname = $this->fullname;
      $e_message = $this->message;
      $e_sender = $this->sender;
      $e_subject = $this->subject;
      $e_location = $this->location;
      $e_contact_no = $this->contact_no;

      return $this->from($e_sender)
      ->subject($e_subject)
      ->view('pages.helpdesk.mail', compact('e_fullname', 'e_subject', 'e_message', 'e_sender', 'e_location', 'e_contact_no'));
    }
}
