<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailStudent extends Mailable
{
    use Queueable, SerializesModels;

    public $subject, $message;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject, $message)
    {
      $this->message = $message;
      $this->subject = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $e_subject = $this->subject;
        $e_message = $this->message;

        return $this->from('access.edu.ph@gmail.com')
        ->subject($e_subject)
        ->view('multiauth::student-campus.mailformat', compact('e_subject', 'e_message'));
    }
}
