<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{

  protected $fillable = ['title', 'detail', 'startdate', 'start_time', 'end_time', 'note', 'photo_id'];

  protected $table = 'events';
  public $primary = 'id';
  public $timestamps = true;

  public function photo() {
    return $this->belongsTo('App\Photo');
  }
}
