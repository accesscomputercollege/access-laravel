<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Collegiate extends Model
{
  protected $table = 'collegiate';
  public $primary = 'id';
  public $timestamps = true;

  public function campuses() {
    return $this->belongsToMany(Category::class);
  }
}
