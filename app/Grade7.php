<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grade7 extends Model
{
  protected $fillable = ['subject_code', 'subject_name', 'grade', 'remark', 'instructor', 'lagro_student_id'];

  protected $table = 'grade7s';
  public $primary = 'id';
  public $timestamps = true;
}
