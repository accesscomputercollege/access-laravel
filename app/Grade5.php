<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grade5 extends Model
{
  protected $fillable = ['subject_code', 'subject_name', 'grade', 'remark', 'instructor', 'lagro_student_id'];

  protected $table = 'grade5s';
  public $primary = 'id';
  public $timestamps = true;
}
