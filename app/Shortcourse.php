<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shortcourse extends Model
{
  protected $table = 'shortcourse';
  public $primary = 'id';
  public $timestamps = true;

  public function campuses() {
    return $this->belongsToMany(Category::class);
  }

}
