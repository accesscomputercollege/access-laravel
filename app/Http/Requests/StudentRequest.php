<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'firstname' => ['required', 'string', 'max:255'],
          'lastname' => ['required', 'string', 'max:255'],
          'middlename' => ['required', 'string', 'max:255'],
          'studentid' => ['required', 'string', 'min:6', 'unique:lagro_students', 'unique:camarin_students'],
          'email' => ['required', 'string', 'email', 'max:255', 'unique:lagro_students', 'unique:camarin_students'],
          'password' => ['required', 'string', 'min:6', 'confirmed'],
        ];
    }
}
