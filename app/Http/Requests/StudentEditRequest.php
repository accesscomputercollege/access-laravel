<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudentEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'firstname' =>  ['required', 'string', 'max:255'],
          'lastname' =>  ['required', 'string', 'max:255'],
          'middlename' =>  ['required', 'string', 'max:255'],
          'studentid' =>  ['required', 'string', 'min:6'],
          'email' => ['required', 'string', 'email', 'max:255'],
          'password' => ['required', 'string', 'min:6', 'confirmed'],
          'course' =>  ['required', 'string'],
          'gender' =>  ['required', 'string'],
          'status' =>  ['required', 'string'],
          'citizenship' =>  ['required', 'string'],
          'birthplace' =>  ['required', 'string'],
          'religion' =>  ['required', 'string'],
          'date_of_birth' =>  ['required', 'string'],
        ];
    }
}
