<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use App\Category;
use App\Subject;
use App\News;

class HelpDeskController extends Controller
{
    public function index() {

      //Get Related news
      $news = News::orderBy('created_at', 'asc')->take(4)->get();

      //Get Campuses & Subjects/Topics
      $categories = Category::all();
      $subjects = Subject::all();
      return view('pages.helpdesk.index', compact('categories', 'subjects', 'news'));
    }

    public function send(Request $get) {

      $fullname = $get->fullname;
      $subject = $get->subject;
      $location = $get->location;
      $contact_no = $get->contact_no;
      $sender = $get->sender;
      $message = $get->message;

      $categories = Category::where('email', $get->input('email'))->pluck('email');
      Mail::to($categories)->send(new SendMail($fullname, $message, $sender, $subject, $location, $contact_no));

      return back()->with('success', 'Thank you for your message!', compact('categories'));
    }
}
