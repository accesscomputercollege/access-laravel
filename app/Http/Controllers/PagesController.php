<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Announcement;
use App\Category; //Campuses
use App\Event;
use App\News;
use App\Photo;
use App\Vocational;
use App\Collegiate;
use App\Shortcourse;
use App\Seniorhigh;

class PagesController extends Controller
{
/*
|--------------------------------------------------------------------------
| HOME PAGE
|--------------------------------------------------------------------------
*/
    public function index () {
      //SHOW ALL ANNOUNCEMENTS
      $categories = Category::with('announcements')->orderBy('branch_name', 'asc')->get();
      $announcements = Announcement::orderBy('created_at', 'latest')->take(4)->get();
      $announcements = Announcement::orderBy('updated_at', 'latest')->take(4)->get();

      //SHOW ALL EVENTS
      $events = Event::orderBy('startdate', 'asc')->take(4)->get();
      //SHOW ALL NEWS
      $news = News::orderBy('created_at', 'latest')->take(6)->get();
      $news = News::orderBy('updated_at', 'latest')->take(6)->get();
      $new = News::orderBy('created_at', 'latest')->take(2)->get();
      $new = News::orderBy('updated_at', 'latest')->take(2)->get();
      return view('pages.main.index', compact('announcements', 'categories', 'events', 'new', 'news'));
    }

/*
|--------------------------------------------------------------------------
| DISCOVER ACCESS PAGE
|--------------------------------------------------------------------------
*/
    public function discover () {
      return view('pages.discover');
    }

/*
|--------------------------------------------------------------------------
| CAMPUSES PAGE
|--------------------------------------------------------------------------
*/
    public function campuses() {
      $campuses = Category::orderBy('branch_name', 'asc')->paginate(5);
      $events = Event::orderBy('created_at', 'asc')->take(4)->get(); //Get Events
      $news = News::orderBy('created_at', 'asc')->take(4)->get(); //Get Lates news
      return view('pages.campuses.index', compact('news', 'events', 'campuses'));
    }

    public function show_campus($id) {
      $events = Event::orderBy('created_at', 'asc')->take(4)->get(); //Get Events
      $news = News::orderBy('created_at', 'asc')->take(4)->get(); //Get Lates news
      $campus = Category::find($id);
      return view('pages.campuses.show', compact('campus', 'events', 'news'))->with('campus', $campus);
    }

/*
|--------------------------------------------------------------------------
| NEWS PAGE
|--------------------------------------------------------------------------
*/
    public function news() {
      $news = News::orderBy('created_at', 'latest')->paginate(9);
      $news = News::orderBy('updated_at', 'latest')->paginate(9);
      $new = News::orderBy('created_at', 'latest')->take(2)->get();
      $new = News::orderBy('updated_at', 'latest')->take(2)->get();
      return view('pages.news.index', compact('news', 'new'))->with('news', $news);
    }

    public function show_news($id) {
      $events = Event::orderBy('created_at', 'latest')->take(4)->get(); //Get Events
      $news = News::orderBy('created_at', 'latest')->take(4)->get(); //Get Lates news
      $new = News::find($id);
      return view('pages.news.show', compact('news', 'events'))->with('new', $new);
    }

/*
|--------------------------------------------------------------------------
| EVENTS PAGE
|--------------------------------------------------------------------------
*/
    public function event() {
      $events = Event::orderBy('startdate', 'asc')->paginate(8);
      $event = Event::orderBy('startdate', 'asc')->take(2)->get();
      return view('pages.event.index', compact('events', 'event'))->with('events', $events);
    }

    public function show_event($id) {
      $events = Event::orderBy('created_at', 'asc')->take(4)->get(); //Get Events
      $news = News::orderBy('created_at', 'asc')->take(4)->get(); //Get Lates news
      $event = Event::find($id);
      return view('pages.event.show', compact('event', 'events', 'news'))->with('event', $event);
    }

/*
|--------------------------------------------------------------------------
| ANNOUNCEMENTS PAGE
|--------------------------------------------------------------------------
*/
    public function announce() {
      $categories = Category::with('announcements')->orderBy('branch_name', 'asc')->get();
      $announcements = Announcement::orderBy('created_at', 'latest')->take(5)->get();
      $announcements = Announcement::orderBy('updated_at', 'latest')->take(5)->get();
      return view('pages.announcement.index', compact('announcements', 'categories'))->with('announcements', $announcements);
    }

    public function category(Category $category) {
      $categories = Category::with('announcements')->orderBy('branch_name', 'asc')->get();
      $announcements = $category->announcements()->orderBy('created_at', 'latest')->get();
      $announcements = $category->announcements()->orderBy('updated_at', 'latest')->get();
      return view('pages.announcement.index', compact('announcements', 'categories'))->with('announcements', $announcements);
    }

    public function show_announce($id) {
      $categories = Category::find($id);
      $announcement = Announcement::find($id);
      return view('pages.announcement.show', compact('announcement', 'categories'))->with('announcement', $announcement);
    }

/*
|--------------------------------------------------------------------------
| ADMISSION PAGE
|--------------------------------------------------------------------------
*/
    public function admission () {
      $events = Event::orderBy('created_at', 'asc')->take(4)->get(); //Get Events
      $news = News::orderBy('created_at', 'asc')->take(4)->get(); //Get Latest news
      return view('pages.admission', compact('news', 'events'));
    }

/*
|--------------------------------------------------------------------------
| PROGRAMS PAGE
|--------------------------------------------------------------------------
*/
    public function vocational () {
      $vocationals = Vocational::orderBy('course_name', 'asc')->paginate(5);
      return view('pages.programs.vocational.index', compact('vocationals'));
    }

    public function show_vocational ($id) {
      $vocational = Vocational::find($id);
      $events = Event::orderBy('created_at', 'asc')->take(4)->get(); //Get Events
      $news = News::orderBy('created_at', 'asc')->take(4)->get(); //Get Latest news
      return view('pages.programs.vocational.show', compact('vocational', 'events', 'news'));
    }

    public function collegiate () {
      $collegiate = Collegiate::orderBy('course_name', 'asc')->paginate(5);
      return view('pages.programs.collegiate.index', compact('collegiate'));
    }

    public function show_collegiate ($id) {
      $college = Collegiate::find($id);
      $events = Event::orderBy('created_at', 'asc')->take(4)->get(); //Get Events
      $news = News::orderBy('created_at', 'asc')->take(4)->get(); //Get Latest news
      return view('pages.programs.collegiate.show', compact('college', 'events', 'news'));
    }

    public function shortcourses () {
      $shortcourses = Shortcourse::orderBy('course_name', 'asc')->paginate(5);
      return view('pages.programs.shortcourses.index', compact('shortcourses'));
    }

    public function show_shortcourses ($id) {
      $shortcourse = Shortcourse::find($id);
      $events = Event::orderBy('created_at', 'asc')->take(4)->get(); //Get Events
      $news = News::orderBy('created_at', 'asc')->take(4)->get(); //Get Latest news
      return view('pages.programs.shortcourses.show', compact('shortcourse', 'events', 'news'));
    }

    public function senior_high () {
      return view('pages.programs.senior-high.index');
    }

}
