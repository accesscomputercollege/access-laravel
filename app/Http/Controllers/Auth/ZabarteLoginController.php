<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class ZabarteLoginController extends Controller
{
  public function __construct()
  {
      $this->middleware('guest:zabarte', ['except' => 'zabarteLogout']);
  }

  public function zabarteLoginForm() {
    return view('auth.zabarte-login');
  }

  public function login(Request $request) {

    $this->validate($request, [
      'studentid' => 'required',
      'password' => 'required|min:6'
    ]);

    if (Auth::guard('zabarte')->attempt(['studentid' => $request->studentid, 'password' => $request->password], $request->remember)) {
      return redirect()->intended(route('student.zabarte.dashboard'));
    }

    return redirect()->back()->withInput($request->only('studentid', 'remember'));
  }

  public function zabarteLogout(Request $request) {

    Auth::guard('zabarte')->logout();
    return redirect('/zabarte/login');
  }
}
