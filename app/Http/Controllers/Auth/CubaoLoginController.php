<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class CubaoLoginController extends Controller
{
  public function __construct()
  {
      $this->middleware('guest:cubao', ['except' => 'cubaoLogout']);
  }

  public function cubaoLoginForm() {
    return view('auth.cubao-login');
  }

  public function login(Request $request) {

    $this->validate($request, [
      'studentid' => 'required',
      'password' => 'required|min:6'
    ]);

    if (Auth::guard('cubao')->attempt(['studentid' => $request->studentid, 'password' => $request->password], $request->remember)) {
      return redirect()->intended(route('student.cubao.dashboard'));
    }

    return redirect()->back()->withInput($request->only('studentid', 'remember'));
  }

  public function cubaoLogout(Request $request) {

    Auth::guard('cubao')->logout();
    return redirect('/cubao/login');
  }
}
