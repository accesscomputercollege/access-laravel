<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class RectoLoginController extends Controller
{
  public function __construct()
  {
      $this->middleware('guest:recto', ['except' => 'rectoLogout']);
  }

  public function rectoLoginForm() {
    return view('auth.recto-login');
  }

  public function login(Request $request) {

    $this->validate($request, [
      'studentid' => 'required',
      'password' => 'required|min:6'
    ]);

    if (Auth::guard('recto')->attempt(['studentid' => $request->studentid, 'password' => $request->password], $request->remember)) {
      return redirect()->intended(route('student.recto.dashboard'));
    }

    return redirect()->back()->withInput($request->only('studentid', 'remember'));
  }

  public function rectoLogout(Request $request) {

    Auth::guard('recto')->logout();
    return redirect('/recto/login');
  }
}
