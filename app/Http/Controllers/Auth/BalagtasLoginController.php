<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class BalagtasLoginController extends Controller
{
  public function __construct()
  {
      $this->middleware('guest:balagtas', ['except' => 'balagtasLogout']);
  }

  public function balagtasLoginForm() {
    return view('auth.balagtas-login');
  }

  public function login(Request $request) {

    $this->validate($request, [
      'studentid' => 'required',
      'password' => 'required|min:6'
    ]);

    if (Auth::guard('balagtas')->attempt(['studentid' => $request->studentid, 'password' => $request->password], $request->remember)) {
      return redirect()->intended(route('student.balagtas.dashboard'));
    }

    return redirect()->back()->withInput($request->only('studentid', 'remember'));
  }

  public function balagtasLogout(Request $request) {

    Auth::guard('balagtas')->logout();
    return redirect('/balagtas/login');
  }
}
