<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class MarikinaLoginController extends Controller
{
  public function __construct()
  {
      $this->middleware('guest:marikina', ['except' => 'marikinaLogout']);
  }

  public function marikinaLoginForm() {
    return view('auth.marikina-login');
  }

  public function login(Request $request) {

    $this->validate($request, [
      'studentid' => 'required',
      'password' => 'required|min:6'
    ]);

    if (Auth::guard('marikina')->attempt(['studentid' => $request->studentid, 'password' => $request->password], $request->remember)) {
      return redirect()->intended(route('student.marikina.dashboard'));
    }

    return redirect()->back()->withInput($request->only('studentid', 'remember'));
  }

  public function marikinaLogout(Request $request) {

    Auth::guard('marikina')->logout();
    return redirect('/marikina/login');
  }
}
