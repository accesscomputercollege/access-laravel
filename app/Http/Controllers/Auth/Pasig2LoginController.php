<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class Pasig2LoginController extends Controller
{
  public function __construct()
  {
      $this->middleware('guest:pasig2', ['except' => 'pasig2Logout']);
  }

  public function pasig2LoginForm() {
    return view('auth.pasig2-login');
  }

  public function login(Request $request) {

    $this->validate($request, [
      'studentid' => 'required',
      'password' => 'required|min:6'
    ]);

    if (Auth::guard('pasig2')->attempt(['studentid' => $request->studentid, 'password' => $request->password], $request->remember)) {
      return redirect()->intended(route('student.pasig2.dashboard'));
    }

    return redirect()->back()->withInput($request->only('studentid', 'remember'));
  }

  public function pasig2Logout(Request $request) {

    Auth::guard('pasig2')->logout();
    return redirect('/pasig2/login');
  }
}
