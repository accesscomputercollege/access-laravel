<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class PasigLoginController extends Controller
{
  public function __construct()
  {
      $this->middleware('guest:pasig', ['except' => 'pasigLogout']);
  }

  public function pasigLoginForm() {
    return view('auth.pasig-login');
  }

  public function login(Request $request) {

    $this->validate($request, [
      'studentid' => 'required',
      'password' => 'required|min:6'
    ]);

    if (Auth::guard('pasig')->attempt(['studentid' => $request->studentid, 'password' => $request->password], $request->remember)) {
      return redirect()->intended(route('student.pasig.dashboard'));
    }

    return redirect()->back()->withInput($request->only('studentid', 'remember'));
  }

  public function pasigLogout(Request $request) {

    Auth::guard('pasig')->logout();
    return redirect('/pasig/login');
  }
}
