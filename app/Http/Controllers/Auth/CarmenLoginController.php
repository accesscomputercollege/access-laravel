<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class CarmenLoginController extends Controller
{
  public function __construct()
  {
      $this->middleware('guest:carmen', ['except' => 'carmenLogout']);
  }

  public function carmenLoginForm() {
    return view('auth.carmen-login');
  }

  public function login(Request $request) {

    $this->validate($request, [
      'studentid' => 'required',
      'password' => 'required|min:6'
    ]);

    if (Auth::guard('carmen')->attempt(['studentid' => $request->studentid, 'password' => $request->password], $request->remember)) {
      return redirect()->intended(route('student.carmen.dashboard'));
    }

    return redirect()->back()->withInput($request->only('studentid', 'remember'));
  }

  public function carmenLogout(Request $request) {

    Auth::guard('carmen')->logout();
    return redirect('/carmen/login');
  }
}
