<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class LagroLoginController extends Controller
{
  public function __construct()
  {
      $this->middleware('guest:lagro', ['except' => 'lagroLogout']);
  }

  public function lagroLoginForm() {
    return view('auth.lagro-login');
  }

  public function login(Request $request) {

    $this->validate($request, [
      'studentid' => 'required',
      'password' => 'required|min:6'
    ]);

    if (Auth::guard('lagro')->attempt(['studentid' => $request->studentid, 'password' => $request->password], $request->remember)) {
      return redirect()->intended(route('student.lagro.dashboard'));
    }

    return redirect()->back()->withInput($request->only('studentid', 'remember'));
  }

  public function lagroLogout(Request $request) {

    Auth::guard('lagro')->logout();
    return redirect('/lagro/login');
  }
}
