<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class MeycauayanLoginController extends Controller
{
  public function __construct()
  {
      $this->middleware('guest:meycauayan', ['except' => 'meycauayanLogout']);
  }

  public function meycauayanLoginForm() {
    return view('auth.meycauayan-login');
  }

  public function login(Request $request) {

    $this->validate($request, [
      'studentid' => 'required',
      'password' => 'required|min:6'
    ]);

    if (Auth::guard('meycauayan')->attempt(['studentid' => $request->studentid, 'password' => $request->password], $request->remember)) {
      return redirect()->intended(route('student.meycauayan.dashboard'));
    }

    return redirect()->back()->withInput($request->only('studentid', 'remember'));
  }

  public function meycauayanLogout(Request $request) {

    Auth::guard('meycauayan')->logout();
    return redirect('/meycauayan/login');
  }
}
