<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class MarilaoLoginController extends Controller
{
  public function __construct()
  {
      $this->middleware('guest:marilao', ['except' => 'marilaoLogout']);
  }

  public function marilaoLoginForm() {
    return view('auth.marilao-login');
  }

  public function login(Request $request) {

    $this->validate($request, [
      'studentid' => 'required',
      'password' => 'required|min:6'
    ]);

    if (Auth::guard('marilao')->attempt(['studentid' => $request->studentid, 'password' => $request->password], $request->remember)) {
      return redirect()->intended(route('student.marilao.dashboard'));
    }

    return redirect()->back()->withInput($request->only('studentid', 'remember'));
  }

  public function marilaoLogout(Request $request) {

    Auth::guard('marilao')->logout();
    return redirect('/marilao/login');
  }
}
