<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class VillasisLoginController extends Controller
{
  public function __construct()
  {
      $this->middleware('guest:villasis', ['except' => 'villasisLogout']);
  }

  public function villasisLoginForm() {
    return view('auth.villasis-login');
  }

  public function login(Request $request) {

    $this->validate($request, [
      'studentid' => 'required',
      'password' => 'required|min:6'
    ]);

    if (Auth::guard('villasis')->attempt(['studentid' => $request->studentid, 'password' => $request->password], $request->remember)) {
      return redirect()->intended(route('student.villasis.dashboard'));
    }

    return redirect()->back()->withInput($request->only('studentid', 'remember'));
  }

  public function villasisLogout(Request $request) {

    Auth::guard('villasis')->logout();
    return redirect('/villasis/login');
  }
}
