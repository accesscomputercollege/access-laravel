<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class CamarinLoginController extends Controller
{
  public function __construct()
  {
      $this->middleware('guest:camarin', ['except' => 'camarinLogout']);
  }

  public function camarinLoginForm() {
    return view('auth.camarin-login');
  }

  public function login(Request $request) {

    $this->validate($request, [
      'studentid' => 'required',
      'password' => 'required|min:6'
    ]);

    if (Auth::guard('camarin')->attempt(['studentid' => $request->studentid, 'password' => $request->password], $request->remember)) {
      return redirect()->intended(route('student.camarin.dashboard'));
    }

    return redirect()->back()->withInput($request->only('studentid', 'remember'));
  }

  public function camarinLogout(Request $request) {

    Auth::guard('camarin')->logout();
    return redirect('/camarin/login');
  }
}
