<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class MonumentoLoginController extends Controller
{
  public function __construct()
  {
      $this->middleware('guest:monumento', ['except' => 'monumentoLogout']);
  }

  public function monumentoLoginForm() {
    return view('auth.monumento-login');
  }

  public function login(Request $request) {

    $this->validate($request, [
      'studentid' => 'required',
      'password' => 'required|min:6'
    ]);

    if (Auth::guard('monumento')->attempt(['studentid' => $request->studentid, 'password' => $request->password], $request->remember)) {
      return redirect()->intended(route('student.monumento.dashboard'));
    }

    return redirect()->back()->withInput($request->only('studentid', 'remember'));
  }

  public function monumentoLogout(Request $request) {

    Auth::guard('monumento')->logout();
    return redirect('/monumento/login');
  }
}
