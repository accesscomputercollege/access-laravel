<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Hash;
use App\Models\Students\LagroStudent;

class LagroDashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:lagro');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('student.lagro.home');
    }

    public function profile()
    {
        return view('student.lagro.profile');
    }

    public function personalInfo()
    {
        return view('student.lagro.personal-info');
    }

    // CHECK LIST (STUDENT ACAD)
    public function checkList()
    {
      $lagro_student_id = auth()->user()->id;
      $lagro_student = LagroStudent::find($lagro_student_id);

      return view('student.lagro.checkList')
      ->with('grades', $lagro_student->grades)
      ->with('grades2', $lagro_student->grades2)
      ->with('grades3', $lagro_student->grades3)
      ->with('grades4', $lagro_student->grades4)
      ->with('grades5', $lagro_student->grades5)
      ->with('grades6', $lagro_student->grades6)
      ->with('grades7', $lagro_student->grades7)
      ->with('grades8', $lagro_student->grades8);
    }

    public function showChangePasswordForm()
    {
      return view('student.lagro.change-password');
    }

    public function changePassword(Request $request){
    if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
        // The passwords matches
        return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
    }
    if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
        //Current password and new password are same
        return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
    }
    $validatedData = $request->validate([
        'current-password' => 'required',
        'new-password' => 'required|string|min:6|confirmed',
    ]);
    //Change Password
    $user = Auth::user();
    $user->password = bcrypt($request->get('new-password'));
    $user->save();
    return redirect()->back()->with("success","Password changed successfully !");
  }
}
