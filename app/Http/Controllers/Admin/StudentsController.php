<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailStudent;
use App\Student;

class StudentsController extends Controller
{
  public function index() {
    $students = Student::all();
    return view('multiauth::students.index', compact('students'));
  }

  public function mail() {
    $students = Student::all();
    return view('multiauth::students.mail', compact('students'));
  }

  public function send(Request $get) {
      $subject = $get->subject;
      $message = $get->message;


      $students = Student::all();
      foreach ($students as $student) {
        Mail::to($student->email)->send(new SendMailStudent($message, $subject));
      }

      // $students = Student::all('email', $get->input('email'))->pluck('email');
      // Mail::to($students)->send(new SendMailStudent($message, $subject));

      return back()->with('success', 'Successfully send to all email!', compact('students'));
  }
}
