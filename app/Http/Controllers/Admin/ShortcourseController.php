<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shortcourse;
use App\Category;

class ShortcourseController extends Controller
{
  public function index() {
    $shortcourses = Shortcourse::all();
    return view('multiauth::programs.shortcourse.index', compact('shortcourses'));
  }

  public function create() {
    $campuses = Category::all();
    return view('multiauth::programs.shortcourse.create', compact('campuses'));
  }

  public function store(Request $request) {
    $this->validate($request, [
      'course_name' => 'required',
      'career_list' => 'required',
      'description' => 'required'
    ]);

    $shortcourse = new Shortcourse;
    $shortcourse->course_name = $request->input('course_name');
    $shortcourse->career_list = $request->input('career_list');
    $shortcourse->description = $request->input('description');
    $shortcourse->save();

    $shortcourse->campuses()->sync($request->campuses, false);

    return redirect('admin/shortcourse')->with('message', 'Successfully created new Short Training Program course');
  }

  public function edit($id) {
    $shortcourse = Shortcourse::find($id);
    $campus = Category::find($id);
    $campuses = Category::all();
    return view('multiauth::programs.shortcourse.edit', compact('shortcourse', 'campus', 'campuses'));
  }

  public function update(Request $request, $id) {
    $this->validate($request, [
      'course_name' => 'required',
      'career_list' => 'required',
      'description' => 'required'
    ]);

    $shortcourse = Shortcourse::find($id);
    $shortcourse->course_name = $request->input('course_name');
    $shortcourse->career_list = $request->input('career_list');
    $shortcourse->description = $request->input('description');
    $shortcourse->save();

    $shortcourse->campuses()->sync($request->campuses);

    return redirect('admin/shortcourse')->with('message', 'Successfully update Short Training Program course');
  }

  public function destroy($id) {
    $shortcourse = Shortcourse::find($id);
    $shortcourse->campuses()->detach();
    $shortcourse->delete();
    return redirect('admin/shortcourse')->with('message', 'Short Training Program Deleted');
  }
}
