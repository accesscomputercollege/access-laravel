<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Vocational;
use App\Category;

class VocationalController extends Controller
{
    public function index() {
      $vocationals = Vocational::orderBy('created_at', 'latest')->get();
      return view('multiauth::programs.vocational.index', compact('vocationals'));
    }

    public function create() {
      $campuses = Category::all();
      return view('multiauth::programs.vocational.create', compact('campuses'));
    }

    public function store(Request $request) {
      $this->validate($request, [
        'course_code' => 'required',
        'course_name' => 'required',
        'career_list' => 'required',
        'description' => 'required'
      ]);

      $voc = new Vocational;
      $voc->course_code = $request->input('course_code');
      $voc->course_name = $request->input('course_name');
      $voc->career_list = $request->input('career_list');
      $voc->description = $request->input('description');
      $voc->save();

      $voc->campuses()->sync($request->campuses, false);

      return redirect('admin/vocational')->with('message', 'Successfully created new vocational course');
    }

    public function edit($id) {
      $voc = Vocational::find($id);
      $campus = Category::find($id);
      $campuses = Category::all();
      return view('multiauth::programs.vocational.edit', compact('voc', 'campus', 'campuses'));
    }

    public function update(Request $request, $id) {
      $this->validate($request, [
        'course_code' => 'required',
        'course_name' => 'required',
        'career_list' => 'required',
        'description' => 'required'
      ]);

      $voc = Vocational::find($id);
      $voc->course_code = $request->input('course_code');
      $voc->course_name = $request->input('course_name');
      $voc->career_list = $request->input('career_list');
      $voc->description = $request->input('description');
      $voc->save();

      $voc->campuses()->sync($request->campuses);

      return redirect('admin/vocational')->with('message', 'Successfully update vocational course');
    }

    public function destroy($id) {
      $voc = Vocational::find($id);
      $voc->campuses()->detach();
      $voc->delete();
      return redirect('admin/vocational')->with('message', 'Vocational Deleted');
    }
}
