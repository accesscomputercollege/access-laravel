<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Event;
use App\Photo;

class EventsController extends Controller
{
  public function index() {
    $events = Event::orderBy('startdate', 'asc')->get();
    return view('multiauth::events.index', compact('events'));
  }

  public function create() {
    return view('multiauth::events.create');
  }

  public function store(Request $request) {
    $this->validate($request, [
      'title' => 'required',
      'detail' => 'required',
      'startdate' => 'required',
      'photo_id' => 'image|nullable|required|max:1999'
    ]);

    $input = $request->all();

    if ($file = $request->file('photo_id')) {
      $name = time() . $file->getClientOriginalName();
      $file->move('cover_images', $name);
      $photo = Photo::create(['file'=>$name]);
      $input['photo_id'] = $photo->id;
    }

    Event::create($input);

    return redirect('admin/events')->with('message', 'Event Created');
  }

  public function edit($id) {
    $event = Event::find($id);
    return view('multiauth::events.edit', compact('event'));
  }

  public function update(Request $request, $id) {
    $this->validate($request, [
      'title' => 'required',
      'detail' => 'required',
      'startdate' => 'required',
    ]);

    $event = Event::findOrFail($id);

    $input = $request->all();

    if ($file = $request->file('photo_id')) {
      $name = time() . $file->getClientOriginalName();
      $file->move('cover_images', $name);
      $photo = Photo::create(['file'=>$name]);
      $input['photo_id'] = $photo->id;
    }

    $event->update($input);

    return redirect('admin/events')->with('message', 'Event Updated');
  }

  public function destroy($id) {
    $event = Event::find($id);
    unlink(public_path() . $event->photo->file);
    $event->delete();
    return redirect('admin/events')->with('message', 'Event Deleted');
  }
}
