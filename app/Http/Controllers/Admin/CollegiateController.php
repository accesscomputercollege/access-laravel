<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Collegiate;
use App\Category;

class CollegiateController extends Controller
{
  public function index() {
    $collegiate = Collegiate::orderBy('created_at', 'latest')->get();
    return view('multiauth::programs.collegiate.index', compact('collegiate'));
  }

  public function create() {
    $campuses = Category::all();
    return view('multiauth::programs.collegiate.create', compact('campuses'));
  }

  public function store(Request $request) {
    $this->validate($request, [
      'course_code' => 'required',
      'course_name' => 'required',
      'career_list' => 'required',
      'description' => 'required'
    ]);

    $college = new Collegiate;
    $college->course_code = $request->input('course_code');
    $college->course_name = $request->input('course_name');
    $college->career_list = $request->input('career_list');
    $college->description = $request->input('description');
    $college->save();

    $college->campuses()->sync($request->campuses, false);

    return redirect('admin/collegiate')->with('message', 'Successfully created new collegiate course');
  }

  public function edit($id) {
    $college = Collegiate::find($id);
    $campus = Category::find($id);
    $campuses = Category::all();
    return view('multiauth::programs.collegiate.edit', compact('college', 'campus', 'campuses'));
  }

  public function update(Request $request, $id) {
    $this->validate($request, [
      'course_code' => 'required',
      'course_name' => 'required',
      'career_list' => 'required',
      'description' => 'required'
    ]);

    $college = Collegiate::find($id);
    $college->course_code = $request->input('course_code');
    $college->course_name = $request->input('course_name');
    $college->career_list = $request->input('career_list');
    $college->description = $request->input('description');
    $college->save();

    $college->campuses()->sync($request->campuses);

    return redirect('admin/collegiate')->with('message', 'Successfully update collegiate course');
  }

  public function destroy($id) {
    $college = Collegiate::find($id);
    $college->campuses()->detach();
    $college->delete();
    return redirect('admin/collegiate')->with('message', 'Collegiate Deleted');
  }
}
