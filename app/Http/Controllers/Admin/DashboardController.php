<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Event;
use App\News;
use App\Announcement;
use App\Collegiate;
use App\Shortcourse;
use App\Vocational;
use App\Seniorhigh;
use App\Models\Students\LagroStudent;

class DashboardController extends Controller
{

  public function index() {
    $campuses = Category::where('id', '!=', 1)->get();
    $events = Event::all();
    $news = News::all();
    $announcements = Announcement::all();
    $colleges = Collegiate::all();
    $shortcourses = Shortcourse::all();
    $vocationals = Vocational::all();
    $seniorhigh = Seniorhigh::all();
    $lagro = LagroStudent::all();
    return view('multiauth::admin.dashboard', compact('campuses', 'events', 'news', 'announcements', 'colleges', 'shortcourses', 'vocationals', 'seniorhigh', 'lagro'));
  }
}
