<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Announcement;
use App\Category;

class AnnouncementController extends Controller
{
  //For admins
  public function index() {
    $categories = Category::all();
    $announcements = Announcement::all();
    return view('multiauth::announcement.index', compact('announcements', 'categories'));
  }

  public function create() {
    $categories = Category::all();
    return view('multiauth::announcement.create', compact('categories'));
  }

  public function store(Request $request) {
    $this->validate($request, [
      'title' => 'required',
      'category_id' => 'required',
      'content_brief' => 'required',
      'content_extended' => 'required',
    ]);

    $announcement = new Announcement;
    $announcement->title = $request->input('title');
    $announcement->category_id = $request->input('category_id');
    $announcement->content_brief = $request->input('content_brief');
    $announcement->content_extended = $request->input('content_extended');
    $announcement->save();

    return redirect('/admin/announcements')->with('message', 'Announcement Created');
  }

  public function edit($id) {
    $categories = Category::all();
    $category = Category::find($id);
    $announcement = Announcement::find($id);
    return view('multiauth::announcement.edit', compact('categories', 'announcement'));
  }

  public function update(Request $request, $id) {
    $this->validate($request, [
      'title' => 'required',
      'category_id' => 'required',
      'content_brief' => 'required',
      'content_extended' => 'required'
    ]);

    $announcement = Announcement::find($id);
    $announcement->title = $request->input('title');
    $announcement->category_id = $request->input('category_id');
    $announcement->content_brief = $request->input('content_brief');
    $announcement->content_extended = $request->input('content_extended');
    $announcement->save();

    return redirect('/admin/announcements')->with('message', 'Announcement Updated');
  }

  public function destroy($id) {
    $announcement = Announcement::find($id);
    $announcement->delete();
    return redirect('/admin/announcements')->with('message', 'Announcement Deleted');
  }
}
