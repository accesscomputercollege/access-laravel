<?php

namespace App\Http\Controllers\Admin\Student;

use App\Http\Requests\StudentRequest;
use App\Http\Requests\StudentEditRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Students\LagroStudent;
use App\Photo;
use App\Seniorhigh;
use App\Vocational;
use App\Collegiate;
use App\Shortcourse;
use App\Grade;
use App\Grade2;
use App\Grade3;
use App\Grade4;
use App\Grade5;
use App\Grade6;
use App\Grade7;
use App\Grade8;

class LagroController extends Controller
{
  public function index() {

    $students = LagroStudent::all();
    return view('multiauth::student.lagro.index', compact('students'));
  }

  public function create() {
    $seniorhigh = Seniorhigh::all();
    $vocationals = Vocational::all();
    $collegiate = Collegiate::all();
    $shortcourses = Shortcourse::all();
    return view('multiauth::student.lagro.create', compact('vocationals', 'seniorhigh', 'collegiate', 'shortcourses'));
  }

  public function store(StudentRequest $request) {

    if (trim($request->password) == '') {
      $input = $request->except('password');
    } else {
      $input = $request->all();
      $input['password'] = bcrypt($request->password);
    }

    if ($file = $request->file('photo_id')) {
      $name = time() . $file->getClientOriginalName();
      $file->move('cover_images', $name);
      $photo = Photo::create(['file'=>$name]);
      $input['photo_id'] = $photo->id;
    }

    LagroStudent::create($input);

    return redirect('/admin/lagro/students/create')->with('message', 'Added new student successfully');
  }

  public function edit($id) {
    $student = LagroStudent::find($id);
    $seniorhigh = Seniorhigh::all();
    $vocationals = Vocational::all();
    $collegiate = Collegiate::all();
    $shortcourses = Shortcourse::all();
    $grades = Grade::where('lagro_student_id', '=', $id)->get();
    return view('multiauth::student.lagro.edit', compact('student', 'vocationals', 'seniorhigh', 'collegiate', 'shortcourses', 'grades'));
  }

  public function update(StudentEditRequest $request, $id) {

    $student = LagroStudent::find($id);

    if (trim($request->password) == '') {
      $input = $request->except('password');
    } else {
      $input = $request->all();
      $input['password'] = bcrypt($request->password);
    }

    if ($file = $request->file('photo_id')) {
      $name = time() . $file->getClientOriginalName();
      $file->move('cover_images', $name);
      $photo = Photo::create(['file'=>$name]);
      $input['photo_id'] = $photo->id;
    }

    $student->update($input);

    return redirect()->back()->with('message', 'Updated student information successfully');
  }

  public function destroy($id) {
    $student = LagroStudent::find($id);
    $grades = Grade::where('lagro_student_id', '=', $id)->delete();
    $grades2 = Grade2::where('lagro_student_id', '=', $id)->delete();
    $grades3 = Grade3::where('lagro_student_id', '=', $id)->delete();
    $grades4 = Grade4::where('lagro_student_id', '=', $id)->delete();
    $grades5 = Grade5::where('lagro_student_id', '=', $id)->delete();
    $grades6 = Grade6::where('lagro_student_id', '=', $id)->delete();
    $grades7 = Grade7::where('lagro_student_id', '=', $id)->delete();
    $grades8 = Grade8::where('lagro_student_id', '=', $id)->delete();
    $student->delete();

    return redirect('/admin/lagro/students')->with('message', 'Successfully Deleted');
  }
}
