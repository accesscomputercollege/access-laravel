<?php

namespace App\Http\Controllers\Admin\Student;

use App\Http\Requests\StudentRequest;
use App\Http\Requests\StudentEditRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\RectoStudent;

class RectoController extends Controller
{
  public function index() {

    $students = RectoStudent::all();
    return view('multiauth::student.recto.index', compact('students'));
  }

  public function create() {
    return view('multiauth::student.recto.create');
  }

  public function store(StudentRequest $request) {

    if (trim($request->password) == '') {
      $input = $request->except('password');
    } else {
      $input = $request->all();
      $input['password'] = bcrypt($request->password);
    }

    RectoStudent::create($input);

    return redirect('/admin/recto/students/create')->with('message', 'Added new student successfully');
  }

  public function edit($id) {

    $student = RectoStudent::find($id);
    return view('multiauth::student.recto.edit', compact('student'));
  }

  public function update(StudentEditRequest $request, $id) {

    $student = RectoStudent::find($id);

    if (trim($request->password) == '') {
      $input = $request->except('password');
    } else {
      $input = $request->all();
      $input['password'] = bcrypt($request->password);
    }

    $student->update($input);

    return redirect('/admin/recto/students')->with('message', 'Updated student information successfully');

  }

  public function destroy($id) {

    RectoStudent::find($id)->delete();

    return redirect('/admin/recto/students')->with('message', 'Successfully Deleted');
  }
}
