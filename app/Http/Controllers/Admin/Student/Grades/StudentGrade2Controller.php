<?php

namespace App\Http\Controllers\Admin\Student\Grades;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Students\LagroStudent;
use App\Grade2;

class StudentGrade2Controller extends Controller
{

  public function show($id) {
    $student = LagroStudent::find($id);
    $grades2 = Grade2::where('lagro_student_id', '=', $id)->get();
    return view('multiauth::student.lagro.grade2Index', compact('student', 'grades2'));
  }

  public function create($id) {
    $student = LagroStudent::find($id);
    $grades2 = Grade2::where('lagro_student_id', '=', $id)->get();
    return view('multiauth::student.lagro.grade2Create', compact('student', 'grades2'));
  }

  public function store(Request $request) {
    $data=$request->all();
      if(count($request->subject_code) > 0)
      {
      foreach($request->subject_code as $item=>$v){
          $data2=array(
              'lagro_student_id'=>$request->input('lagro_student_id'),
              'subject_code'=>$request->subject_code[$item],
              'subject_name'=>$request->subject_name[$item],
              'grade'=>$request->grade[$item],
              'remark'=>$request->remark[$item],
              'instructor'=>$request->instructor[$item],
          );
      Grade2::create($data2);
        }
      }
    return redirect()->back()->with('message', 'Successfully Added Grades');
  }

  public function update(Request $request, $id) {
    $grade2 = Grade2::find($id);
    $input = $request->all();
    $grade2->update($input);

    return redirect()->back()->with('message', 'Successfully Update Grades');
  }

  public function destroy($id) {
    Grade2::find($id)->delete();

    return redirect()->back()->with('message', 'Successfully Deleted');
  }

}
