<?php

namespace App\Http\Controllers\Admin\Student\Grades;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Students\LagroStudent;
use App\Grade3;

class StudentGrade3Controller extends Controller
{

  public function show($id) {
    $student = LagroStudent::find($id);
    $grades3 = Grade3::where('lagro_student_id', '=', $id)->get();
    return view('multiauth::student.lagro.grade3Index', compact('student', 'grades3'));
  }

  public function create($id) {
    $student = LagroStudent::find($id);
    $grades3 = Grade3::where('lagro_student_id', '=', $id)->get();
    return view('multiauth::student.lagro.grade3Create', compact('student', 'grades3'));
  }

  public function store(Request $request) {
    $data=$request->all();
      if(count($request->subject_code) > 0)
      {
      foreach($request->subject_code as $item=>$v){
          $data2=array(
              'lagro_student_id'=>$request->input('lagro_student_id'),
              'subject_code'=>$request->subject_code[$item],
              'subject_name'=>$request->subject_name[$item],
              'grade'=>$request->grade[$item],
              'remark'=>$request->remark[$item],
              'instructor'=>$request->instructor[$item],
          );
      Grade3::create($data2);
        }
      }
    return redirect()->back()->with('message', 'Successfully Added Grades');
  }

  public function update(Request $request, $id) {
    $grade3 = Grade3::find($id);
    $input = $request->all();
    $grade3->update($input);

    return redirect()->back()->with('message', 'Successfully Update Grades');
  }

  public function destroy($id) {
    Grade3::find($id)->delete();

    return redirect()->back()->with('message', 'Successfully Deleted');
  }

}
