<?php

namespace App\Http\Controllers\Admin\Student\Grades;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Students\LagroStudent;
use App\Grade;

class StudentGrade1Controller extends Controller
{

  public function show($id) {
    $student = LagroStudent::find($id);
    $grades = Grade::where('lagro_student_id', '=', $id)->get();
    return view('multiauth::student.lagro.gradeIndex', compact('student', 'grades'));
  }

  public function create($id) {
    $student = LagroStudent::find($id);
    $grades = Grade::where('lagro_student_id', '=', $id)->get();
    return view('multiauth::student.lagro.gradeCreate', compact('student', 'grades'));
  }

  public function store(Request $request) {
    $data=$request->all();
      if(count($request->subject_code) > 0)
      {
      foreach($request->subject_code as $item=>$v){
          $data2=array(
              'lagro_student_id'=>$request->input('lagro_student_id'),
              'subject_code'=>$request->subject_code[$item],
              'subject_name'=>$request->subject_name[$item],
              'grade'=>$request->grade[$item],
              'remark'=>$request->remark[$item],
              'instructor'=>$request->instructor[$item],
          );
      Grade::create($data2);
        }
      }
    return redirect()->back()->with('message', 'Successfully Added Grades');
  }

  public function update(Request $request, $id) {
    $grade = Grade::find($id);
    $input = $request->all();
    $grade->update($input);

    return redirect()->back()->with('message', 'Successfully Update Grades');
  }

  public function destroy($id) {
    Grade::find($id)->delete();

    return redirect()->back()->with('message', 'Successfully Deleted');
  }

}
