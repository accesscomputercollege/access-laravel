<?php

namespace App\Http\Controllers\Admin\Student\Grades;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Students\LagroStudent;
use App\Grade7;

class StudentGrade7Controller extends Controller
{

  public function show($id) {
    $student = LagroStudent::find($id);
    $grades7 = Grade7::where('lagro_student_id', '=', $id)->get();
    return view('multiauth::student.lagro.grade7Index', compact('student', 'grades7'));
  }

  public function create($id) {
    $student = LagroStudent::find($id);
    $grades7 = Grade7::where('lagro_student_id', '=', $id)->get();
    return view('multiauth::student.lagro.grade7Create', compact('student', 'grades7'));
  }

  public function store(Request $request) {
    $data=$request->all();
      if(count($request->subject_code) > 0)
      {
      foreach($request->subject_code as $item=>$v){
          $data2=array(
              'lagro_student_id'=>$request->input('lagro_student_id'),
              'subject_code'=>$request->subject_code[$item],
              'subject_name'=>$request->subject_name[$item],
              'grade'=>$request->grade[$item],
              'remark'=>$request->remark[$item],
              'instructor'=>$request->instructor[$item],
          );
      Grade7::create($data2);
        }
      }
    return redirect()->back()->with('message', 'Successfully Added Grades');
  }

  public function update(Request $request, $id) {
    $grade7 = Grade7::find($id);
    $input = $request->all();
    $grade7->update($input);

    return redirect()->back()->with('message', 'Successfully Update Grades');
  }

  public function destroy($id) {
    Grade7::find($id)->delete();

    return redirect()->back()->with('message', 'Successfully Deleted');
  }

}
