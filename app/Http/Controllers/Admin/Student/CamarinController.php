<?php

namespace App\Http\Controllers\Admin\Student;

use App\Http\Requests\StudentRequest;
use App\Http\Requests\StudentEditRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CamarinStudent;

class CamarinController extends Controller
{
  public function index() {

    $students = CamarinStudent::all();
    return view('multiauth::student.camarin.index', compact('students'));
  }

  public function create() {
    return view('multiauth::student.camarin.create');
  }

  public function store(StudentRequest $request) {

    if (trim($request->password) == '') {
      $input = $request->except('password');
    } else {
      $input = $request->all();
      $input['password'] = bcrypt($request->password);
    }

    CamarinStudent::create($input);

    return redirect('/admin/camarin/students/create')->with('message', 'Added new student successfully');
  }

  public function edit($id) {

    $student = CamarinStudent::find($id);
    return view('multiauth::student.camarin.edit', compact('student'));
  }

  public function update(StudentEditRequest $request, $id) {

    $student = CamarinStudent::find($id);

    if (trim($request->password) == '') {
      $input = $request->except('password');
    } else {
      $input = $request->all();
      $input['password'] = bcrypt($request->password);
    }

    $student->update($input);

    return redirect('/admin/camarin/students')->with('message', 'Updated student information successfully');

  }

  public function destroy($id) {

    CamarinStudent::find($id)->delete();

    return redirect('/admin/camarin/students')->with('message', 'Successfully Deleted');
  }
}
