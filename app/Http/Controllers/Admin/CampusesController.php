<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Photo;
use App\Vocational;
use App\Collegiate;
use App\Shortcourse;
use App\Seniorhigh;

class CampusesController extends Controller
{
    public function index() {
      $campuses = Category::all();
      return view('multiauth::campuses.index', compact('campuses'));
    }

    public function create() {
      $campuses = Category::all();
      $vocationals = Vocational::all();
      $collegiate = Collegiate::all();
      $shortcourses = Shortcourse::all();
      $seniorhigh = Seniorhigh::all();
      return view('multiauth::campuses.create', compact('campuses', 'vocationals', 'collegiate', 'shortcourses', 'seniorhigh'));
    }

    public function store(Request $request) {

      $this->validate($request, [
        'branch_name' => 'required',
        'slug' => 'required',
        'address' => 'required',
        'map' => 'required',
        'email' => 'required',
        'photo_id' => 'required'
      ]);

      $campus = new Category;
      $campus->branch_name = $request->input('branch_name');
      $campus->slug = $request->input('slug');
      $campus->address = $request->input('address');
      $campus->telephone = $request->input('telephone');
      $campus->mobile = $request->input('mobile');
      $campus->facebook = $request->input('facebook');
      $campus->map = $request->input('map');
      $campus->email = $request->input('email');
      $campus->photo_id = $request->input('photo_id');
      if ($file = $request->file('photo_id')) {
        $name = time() . $file->getClientOriginalName();
        $file->move('cover_images', $name);
        $photo = Photo::create(['file'=>$name]);
        $campus['photo_id'] = $photo->id;
      }
      $campus->save();

      // get available programs
      $campus->vocationals()->sync($request->vocationals, false);
      $campus->collegiate()->sync($request->collegiate, false);
      $campus->shortcourses()->sync($request->shortcourses, false);
      $campus->seniorhigh()->sync($request->seniorhigh, false);

      return redirect('/admin/campuses')->with('message', 'New Campus Created');
    }

    public function edit($id) {
      $vocationals = Vocational::all();
      $collegiate = Collegiate::all();
      $shortcourses = Shortcourse::all();
      $seniorhigh = Seniorhigh::all();

      $campus = Category::find($id);
      $voc = Vocational::find($id);
      $college = Collegiate::find($id);
      $shortcourse = Shortcourse::find($id);
      $senior = Seniorhigh::find($id);

      return view('multiauth::campuses.edit', compact('campus', 'vocational', 'college', 'shortcourse', 'senior', 'vocationals', 'collegiate', 'shortcourses', 'seniorhigh'));
    }

    public function update(Request $request, $id) {
      $this->validate($request, [
        'branch_name' => 'required',
        'slug' => 'required',
        'address' => 'required',
        'map' => 'required',
        'email' => 'required',
      ]);

      $campus = Category::findOrFail($id);
      $input = $request->all();

      if ($file = $request->file('photo_id')) {
        $name = time() . $file->getClientOriginalName();
        $file->move('cover_images', $name);
        $photo = Photo::create(['file'=>$name]);
        $input['photo_id'] = $photo->id;
      }

      $campus->update($input);
      $campus->vocationals()->sync($request->vocationals);
      $campus->collegiate()->sync($request->collegiate);
      $campus->shortcourses()->sync($request->shortcourses);
      $campus->seniorhigh()->sync($request->seniorhigh);

      return redirect('admin/campuses')->with('message', 'Campus has been Updated');
    }

    public function destroy($id) {
      $campus = Category::findOrFail($id);

      unlink(public_path() . $campus->photo->file);

      $campus->vocationals()->detach();
      $campus->collegiate()->detach();
      $campus->shortcourses()->detach();
      $campus->seniorhigh()->detach();

      $campus->delete();
      return redirect('admin/campuses')->with('message', ' Campus has been Deleted');
    }
}
