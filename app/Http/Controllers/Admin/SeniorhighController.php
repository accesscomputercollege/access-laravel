<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Seniorhigh;
use App\Category;

class SeniorhighController extends Controller
{
  public function index() {
    $seniorhigh = Seniorhigh::all();
    return view('multiauth::programs.seniorhigh.index', compact('seniorhigh'));
  }

  public function create() {
    $campuses = Category::all();
    return view('multiauth::programs.seniorhigh.create', compact('campuses'));
  }

  public function store(Request $request) {
    $this->validate($request, [
      'course_name' => 'required',
      'description' => 'required'
    ]);

    $senior = new Seniorhigh;
    $senior->course_name = $request->input('course_name');
    $senior->description = $request->input('description');
    $senior->save();

    $senior->campuses()->sync($request->campuses, false);

    return redirect('admin/senior_high')->with('message', 'Successfully created new seniorhigh course');
  }

  public function edit($id) {
    $senior = Seniorhigh::find($id);
    $campus = Category::find($id);
    $campuses = Category::all();
    return view('multiauth::programs.seniorhigh.edit', compact('senior', 'campus', 'campuses'));
  }

  public function update(Request $request, $id) {
    $this->validate($request, [
      'course_name' => 'required',
      'description' => 'required'
    ]);

    $senior = Seniorhigh::find($id);
    $senior->course_name = $request->input('course_name');
    $senior->description = $request->input('description');
    $senior->save();

    $senior->campuses()->sync($request->campuses);

    return redirect('admin/senior_high')->with('message', 'Successfully update seniorhigh course');
  }

  public function destroy($id) {
    $senior = Seniorhigh::find($id);
    $senior->campuses()->detach();
    $senior->delete();
    return redirect('admin/senior_high')->with('message', 'Senior High Deleted');
  }
}
