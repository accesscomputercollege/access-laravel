<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\News;
use App\Photo;
use App\Category;

class NewsController extends Controller
{
  public function index() {
    $news = News::all();
    return view('multiauth::news.index', compact('news'))->with('news', $news);
  }

  public function create() {
    $campuses = Category::all();
    return view('multiauth::news.create', compact('campuses'));
  }

  public function store(Request $request) {
    $this->validate($request, [
      'title' => 'required',
      'campus' => 'required',
      'detail_brief' => 'required',
      'detail_extended' => 'required',
      'photo_id' => 'image|nullable|required|max:1999'
    ]);

    $input = $request->all();

    if ($file = $request->file('photo_id')) {
      $name = time() . $file->getClientOriginalName();
      $file->move('cover_images', $name);
      $photo = Photo::create(['file'=>$name]);
      $input['photo_id'] = $photo->id;
    }

    News::create($input);

    return redirect('admin/news')->with('message', 'News Created');
  }

  public function edit($id) {
    $new = News::find($id);
    $campus = Category::find($id);
    $campuses = Category::all();
    return view('multiauth::news.edit', compact('new', 'campus', 'campuses'));
  }

  public function update(Request $request, $id) {
    $this->validate($request, [
      'title' => 'required',
      'campus' => 'required',
      'detail_brief' => 'required',
      'detail_extended' => 'required',
    ]);

    $new = News::findOrFail($id);

    $input = $request->all();

    if ($file = $request->file('photo_id')) {
      $name = time() . $file->getClientOriginalName();
      $file->move('cover_images', $name);
      $photo = Photo::create(['file'=>$name]);
      $input['photo_id'] = $photo->id;
    }

    $new->update($input);

    return redirect('admin/news')->with('message', 'News Updated');
  }

  public function destroy($id) {
    $new = News::find($id);
    unlink(public_path() . $new->photo->file);
    $new->delete();
    return redirect('admin/news')->with('message', 'News Deleted');
  }

}
