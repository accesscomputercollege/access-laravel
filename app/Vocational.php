<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vocational extends Model
{
  protected $table = 'vocational';
  public $primary = 'id';
  public $timestamps = true;

  public function campuses() {
    return $this->belongsToMany(Category::class);
  }
}
