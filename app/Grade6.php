<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grade6 extends Model
{
  protected $fillable = ['subject_code', 'subject_name', 'grade', 'remark', 'instructor', 'lagro_student_id'];

  protected $table = 'grade6s';
  public $primary = 'id';
  public $timestamps = true;
}
