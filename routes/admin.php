<?php

Route::group([
    'namespace'  => 'Bitfumes\Multiauth\Http\Controllers',
    'middleware' => 'web',
    'prefix'     => config('multiauth.prefix', 'admin'),
], function () {
    Route::GET('/home', 'AdminController@index')->name('admin.home');
    // Login and Logout
    Route::GET('/', 'LoginController@showLoginForm')->name('admin.login');
    Route::POST('/', 'LoginController@login');
    Route::POST('/sign-out', 'LoginController@logout')->name('admin.signout');

    // Password Resets
    Route::POST('/password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    Route::GET('/password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::POST('/password/reset', 'ResetPasswordController@reset');
    Route::GET('/password/reset/{token}', 'ResetPasswordController@showResetForm')->name('admin.password.reset');

    // Register Admins
    Route::get('/register', 'RegisterController@showRegistrationForm')->name('admin.register');
    Route::post('/register', 'RegisterController@register');
    Route::get('/{admin}/edit', 'RegisterController@edit')->name('admin.edit');
    Route::delete('/{admin}', 'RegisterController@destroy')->name('admin.delete');
    Route::patch('/{admin}', 'RegisterController@update')->name('admin.update');

    // Admin Lists
    Route::get('/show', 'AdminController@show')->name('admin.show');

    // Admin Roles
    Route::post('/{admin}/role/{role}', 'AdminRoleController@attach')->name('admin.attach.roles');
    Route::delete('/{admin}/role/{role}', 'AdminRoleController@detach');

    // Roles
    Route::get('/roles', 'RoleController@index')->name('admin.roles');
    Route::get('/role/create', 'RoleController@create')->name('admin.role.create');
    Route::post('/role/store', 'RoleController@store')->name('admin.role.store');
    Route::delete('/role/{role}', 'RoleController@destroy')->name('admin.role.delete');
    Route::get('/role/{role}/edit', 'RoleController@edit')->name('admin.role.edit');
    Route::patch('/role/{role}', 'RoleController@update')->name('admin.role.update');

});

Route::group([
  'namespace'  => 'App\Http\Controllers',
  'middleware' => 'web',
  'prefix'     => config('multiauth.prefix', 'admin'),
], function () {

    Route::get('/dashboard', 'Admin\DashboardController@index')->name('admin.dashboard');
    // SCHOOL ANNOUNCEMENTS
    Route::get('/announcements', 'Admin\AnnouncementController@index')->name('admin.announcements')->middleware('role:staff');
    Route::get('/announcement/{announcement}/edit', 'Admin\AnnouncementController@edit')->name('admin.announcement.edit')->middleware('role:staff');
    Route::get('/announcement/create', 'Admin\AnnouncementController@create')->name('admin.announcement.create')->middleware('role:staff');
    Route::post('/announcement/store', 'Admin\AnnouncementController@store')->name('admin.announcement.store')->middleware('role:staff');
    Route::patch('/announcement/{announcement}', 'Admin\AnnouncementController@update')->name('admin.announcement.update')->middleware('role:staff');;
    Route::delete('/announcement/{announcement}', 'Admin\AnnouncementController@destroy')->name('admin.announcement.delete')->middleware('role:staff');
    // SCHOOL EVENTS
    Route::resource('/events', 'Admin\EventsController')->middleware('role:super');
    // SCHOOL NEWS
    Route::resource('/news', 'Admin\NewsController')->middleware('role:staff');;
    // SCHOOL CAMPUSES
    Route::resource('/campuses', 'Admin\CampusesController')->middleware('role:super');
    // SCHOOL PROGRAMS
    Route::resource('/vocational', 'Admin\VocationalController')->middleware('role:super');
    Route::resource('/collegiate', 'Admin\CollegiateController')->middleware('role:super');
    Route::resource('/senior_high', 'Admin\SeniorhighController')->middleware('role:super');
    Route::resource('/shortcourse', 'Admin\ShortcourseController')->middleware('role:super');

    // Lagro Student Manage
    Route::resource('/lagro/students', 'Admin\Student\LagroController')->middleware('role:lagro');
    Route::post('/lagro/students/create', 'Admin\Student\LagroController@store')->middleware('role:lagro');
    Route::post('/lagro/students', 'Admin\Student\LagroController@update')->middleware('role:lagro');
    // Lagro 1st year 1st Sem
    Route::resource('/lagro/students/grades', 'Admin\Student\Grades\StudentGrade1Controller')->middleware('role:lagro');
    Route::get('/lagro/students/grades/{student}', 'Admin\Student\Grades\StudentGrade1Controller@show')->middleware('role:lagro');
    Route::get('/lagro/students/grades/{student}/create', 'Admin\Student\Grades\StudentGrade1Controller@create')->middleware('role:lagro');
    // Lagro 1st year 2nd Sem
    Route::resource('/lagro/students/grades2', 'Admin\Student\Grades\StudentGrade2Controller')->middleware('role:lagro');
    Route::get('/lagro/students/grades2/{student}', 'Admin\Student\Grades\StudentGrade2Controller@show')->middleware('role:lagro');
    Route::get('/lagro/students/grades2/{student}/create', 'Admin\Student\Grades\StudentGrade2Controller@create')->middleware('role:lagro');
    // Lagro 2nd year 1st Sem
    Route::resource('/lagro/students/grades3', 'Admin\Student\Grades\StudentGrade3Controller')->middleware('role:lagro');
    Route::get('/lagro/students/grades3/{student}', 'Admin\Student\Grades\StudentGrade3Controller@show')->middleware('role:lagro');
    Route::get('/lagro/students/grades3/{student}/create', 'Admin\Student\Grades\StudentGrade3Controller@create')->middleware('role:lagro');
    // Lagro 2nd year 2nd Sem
    Route::resource('/lagro/students/grades4', 'Admin\Student\Grades\StudentGrade4Controller')->middleware('role:lagro');
    Route::get('/lagro/students/grades4/{student}', 'Admin\Student\Grades\StudentGrade4Controller@show')->middleware('role:lagro');
    Route::get('/lagro/students/grades4/{student}/create', 'Admin\Student\Grades\StudentGrade4Controller@create')->middleware('role:lagro');
    // Lagro 3rd year 1st Sem
    Route::resource('/lagro/students/grades5', 'Admin\Student\Grades\StudentGrade5Controller')->middleware('role:lagro');
    Route::get('/lagro/students/grades5/{student}', 'Admin\Student\Grades\StudentGrade5Controller@show')->middleware('role:lagro');
    Route::get('/lagro/students/grades5/{student}/create', 'Admin\Student\Grades\StudentGrade5Controller@create')->middleware('role:lagro');
    // Lagro 3rd year 2nd Sem
    Route::resource('/lagro/students/grades6', 'Admin\Student\Grades\StudentGrade6Controller')->middleware('role:lagro');
    Route::get('/lagro/students/grades6/{student}', 'Admin\Student\Grades\StudentGrade6Controller@show')->middleware('role:lagro');
    Route::get('/lagro/students/grades6/{student}/create', 'Admin\Student\Grades\StudentGrade6Controller@create')->middleware('role:lagro');
    // Lagro 4th year 1st Sem
    Route::resource('/lagro/students/grades7', 'Admin\Student\Grades\StudentGrade7Controller')->middleware('role:lagro');
    Route::get('/lagro/students/grades7/{student}', 'Admin\Student\Grades\StudentGrade7Controller@show')->middleware('role:lagro');
    Route::get('/lagro/students/grades7/{student}/create', 'Admin\Student\Grades\StudentGrade7Controller@create')->middleware('role:lagro');
    // Lagro 4th year 2nd Sem
    Route::resource('/lagro/students/grades8', 'Admin\Student\Grades\StudentGrade8Controller')->middleware('role:lagro');
    Route::get('/lagro/students/grades8/{student}', 'Admin\Student\Grades\StudentGrade8Controller@show')->middleware('role:lagro');
    Route::get('/lagro/students/grades8/{student}/create', 'Admin\Student\Grades\StudentGrade8Controller@create')->middleware('role:lagro');

    Route::resource('/camarin/students', 'Admin\Student\CamarinController');
    Route::resource('/recto/students', 'Admin\Student\RectoController');

    Route::post('/student-register', 'Auth\RegisterController@register');
    Route::get('/student-register', 'Auth\RegisterController@showRegistrationForm')->name('register');

    Route::get('/{any}', function () {
        return abort(404);
    });
});
