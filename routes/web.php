<?php

// Static Pages
Route::get('/', 'PagesController@index');
Route::get('/discover_access', 'PagesController@discover')->name('pages.discover');
Route::get('/campuses', 'PagesController@campuses')->name('pages.campuses');
Route::get('/admission', 'PagesController@admission')->name('pages.admission');

// Dynamic Pages
// Announcements
Route::get('/announcements', 'PagesController@announce')->name('pages.announcement.index');
Route::get('/announcement/{category}', 'PagesController@category')->name('pages.category');
Route::get('/announcement/{announcement}/{category}', 'PagesController@show_announce')->name('pages.announcement.show');
// Events
Route::get('/events', 'PagesController@event');
Route::get('/events/{event}', 'PagesController@show_event');
// News
Route::get('/news', 'PagesController@news');
Route::get('/news/{new}', 'PagesController@show_news');
// Campuses
Route::get('/campuses', 'PagesController@campuses');
Route::get('/campus/{campus}', 'PagesController@show_campus');
// PROGRAMS
Route::get('/vocationals', 'PagesController@vocational');
Route::get('/vocational/{vocational}', 'PagesController@show_vocational');
Route::get('/collegiate', 'PagesController@collegiate');
Route::get('/collegiate/{collegiate}', 'PagesController@show_collegiate');
Route::get('/shortcourses', 'PagesController@shortcourses');
Route::get('/shortcourse/{shortcourse}', 'PagesController@show_shortcourses');
Route::get('/senior-high', 'PagesController@senior_high');
// Help Desk
Route::get('/contact-us', 'HelpDeskController@index');
Route::post('contact-us/send', 'HelpDeskController@send');


/*
|--------------------------------------------------------------------------
| STUDENT LOGIN BY CAMPUS
|--------------------------------------------------------------------------
*/
Route::get('student-login', function(){
  return view('pages.student-login');
});

Route::prefix('lagro')->group(function() {
  Route::get('/dashboard', 'LagroDashboardController@index')->name('student.lagro.dashboard');
  Route::get('/my-profile', 'LagroDashboardController@profile')->name('lagro.student.profile');
  Route::get('/personal-information', 'LagroDashboardController@personalInfo')->name('lagro.student.personal-info');
  Route::get('/check-list', 'LagroDashboardController@checkList')->name('lagro.student.check-list');
  Route::get('/login', 'Auth\LagroLoginController@lagroLoginForm')->name('login');
  Route::post('/login', 'Auth\LagroLoginController@login')->name('lagro.login.submit');
  Route::post('/logout', 'Auth\LagroLoginController@lagroLogout')->name('lagro.student.logout');
  // Change Password
  Route::get('/change-password', 'LagroDashboardController@showChangePasswordForm')->name('lagro.changePasswordForm');;
  Route::post('/change-password','LagroDashboardController@changePassword')->name('lagro.changePassword');
});

Route::prefix('balagtas')->group(function() {
  Route::get('/dashboard', 'BalagtasDashboardController@index')->name('student.balagtas.dashboard');
  Route::get('/my-profile', 'BalagtasDashboardController@profile')->name('balagtas.student.profile');
  Route::get('/personal-information', 'BalagtasDashboardController@personalInfo')->name('balagtas.student.personal-info');
  Route::get('/check-list', 'BalagtasDashboardController@checkList')->name('balagtas.student.check-list');
  Route::get('/login', 'Auth\BalagtasLoginController@balagtasLoginForm')->name('login');
  Route::post('/login', 'Auth\BalagtasLoginController@login')->name('balagtas.login.submit');
  Route::post('/logout', 'Auth\BalagtasLoginController@balagtasLogout')->name('balagtas.student.logout');
  // Change Password
  Route::get('/change-password', 'BalagtasDashboardController@showChangePasswordForm')->name('balagtas.changePasswordForm');;
  Route::post('/change-password','BalagtasDashboardController@changePassword')->name('balagtas.changePassword');
});

Route::prefix('camarin')->group(function() {
  Route::get('/dashboard', 'CamarinDashboardController@index')->name('student.camarin.dashboard');
  Route::get('/my-profile', 'CamarinDashboardController@profile')->name('camarin.student.profile');
  Route::get('/personal-information', 'CamarinDashboardController@personalInfo')->name('camarin.student.personal-info');
  Route::get('/check-list', 'CamarinDashboardController@checkList')->name('camarin.student.check-list');
  Route::get('/login', 'Auth\CamarinLoginController@camarinLoginForm')->name('login');
  Route::post('/login', 'Auth\CamarinLoginController@login')->name('camarin.login.submit');
  Route::post('/logout', 'Auth\CamarinLoginController@camarinLogout')->name('camarin.student.logout');
  // Change Password
  Route::get('/change-password', 'CamarinDashboardController@showChangePasswordForm')->name('camarin.changePasswordForm');;
  Route::post('/change-password','CamarinDashboardController@changePassword')->name('camarin.changePassword');
});

Route::prefix('carmen')->group(function() {
  Route::get('/dashboard', 'CarmenDashboardController@index')->name('student.carmen.dashboard');
  Route::get('/my-profile', 'CarmenDashboardController@profile')->name('carmen.student.profile');
  Route::get('/personal-information', 'CarmenDashboardController@personalInfo')->name('carmen.student.personal-info');
  Route::get('/check-list', 'CarmenDashboardController@checkList')->name('carmen.student.check-list');
  Route::get('/login', 'Auth\CarmenLoginController@carmenLoginForm')->name('login');
  Route::post('/login', 'Auth\CarmenLoginController@login')->name('carmen.login.submit');
  Route::post('/logout', 'Auth\CarmenLoginController@carmenLogout')->name('carmen.student.logout');
  // Change Password
  Route::get('/change-password', 'CarmenDashboardController@showChangePasswordForm')->name('carmen.changePasswordForm');;
  Route::post('/change-password','CarmenDashboardController@changePassword')->name('carmen.changePassword');
});

Route::prefix('cubao')->group(function() {
  Route::get('/dashboard', 'CubaoDashboardController@index')->name('student.cubao.dashboard');
  Route::get('/my-profile', 'CubaoDashboardController@profile')->name('cubao.student.profile');
  Route::get('/personal-information', 'CubaoDashboardController@personalInfo')->name('cubao.student.personal-info');
  Route::get('/check-list', 'CubaoDashboardController@checkList')->name('cubao.student.check-list');
  Route::get('/login', 'Auth\CubaoLoginController@cubaoLoginForm')->name('login');
  Route::post('/login', 'Auth\CubaoLoginController@login')->name('cubao.login.submit');
  Route::post('/logout', 'Auth\CubaoLoginController@cubaoLogout')->name('cubao.student.logout');
  // Change Password
  Route::get('/change-password', 'CubaoDashboardController@showChangePasswordForm')->name('cubao.changePasswordForm');;
  Route::post('/change-password','CubaoDashboardController@changePassword')->name('cubao.changePassword');
});

Route::prefix('marikina')->group(function() {
  Route::get('/dashboard', 'MarikinaDashboardController@index')->name('student.marikina.dashboard');
  Route::get('/my-profile', 'MarikinaDashboardController@profile')->name('marikina.student.profile');
  Route::get('/personal-information', 'MarikinaDashboardController@personalInfo')->name('marikina.student.personal-info');
  Route::get('/check-list', 'MarikinaDashboardController@checkList')->name('marikina.student.check-list');
  Route::get('/login', 'Auth\MarikinaLoginController@marikinaLoginForm')->name('login');
  Route::post('/login', 'Auth\MarikinaLoginController@login')->name('marikina.login.submit');
  Route::post('/logout', 'Auth\MarikinaLoginController@marikinaLogout')->name('marikina.student.logout');
  // Change Password
  Route::get('/change-password', 'MarikinaDashboardController@showChangePasswordForm')->name('marikina.changePasswordForm');;
  Route::post('/change-password','MarikinaDashboardController@changePassword')->name('marikina.changePassword');
});

Route::prefix('marilao')->group(function() {
  Route::get('/dashboard', 'MarilaoDashboardController@index')->name('student.marilao.dashboard');
  Route::get('/my-profile', 'MarilaoDashboardController@profile')->name('marilao.student.profile');
  Route::get('/personal-information', 'MarilaoDashboardController@personalInfo')->name('marilao.student.personal-info');
  Route::get('/check-list', 'MarilaoDashboardController@checkList')->name('marilao.student.check-list');
  Route::get('/login', 'Auth\MarilaoLoginController@marilaoLoginForm')->name('login');
  Route::post('/login', 'Auth\MarilaoLoginController@login')->name('marilao.login.submit');
  Route::post('/logout', 'Auth\MarilaoLoginController@marilaoLogout')->name('marilao.student.logout');
  // Change Password
  Route::get('/change-password', 'MarilaoDashboardController@showChangePasswordForm')->name('marilao.changePasswordForm');;
  Route::post('/change-password','MarilaoDashboardController@changePassword')->name('marilao.changePassword');
});

Route::prefix('meycauayan')->group(function() {
  Route::get('/dashboard', 'MeycauayanDashboardController@index')->name('student.meycauayan.dashboard');
  Route::get('/my-profile', 'MeycauayanDashboardController@profile')->name('meycauayan.student.profile');
  Route::get('/personal-information', 'MeycauayanDashboardController@personalInfo')->name('meycauayan.student.personal-info');
  Route::get('/check-list', 'MeycauayanDashboardController@checkList')->name('meycauayan.student.check-list');
  Route::get('/login', 'Auth\MeycauayanLoginController@meycauayanLoginForm')->name('login');
  Route::post('/login', 'Auth\MeycauayanLoginController@login')->name('meycauayan.login.submit');
  Route::post('/logout', 'Auth\MeycauayanLoginController@meycauayanLogout')->name('meycauayan.student.logout');
  // Change Password
  Route::get('/change-password', 'MeycauayanDashboardController@showChangePasswordForm')->name('meycauayan.changePasswordForm');;
  Route::post('/change-password','MeycauayanDashboardController@changePassword')->name('meycauayan.changePassword');
});

Route::prefix('monumento')->group(function() {
  Route::get('/dashboard', 'MonumentoDashboardController@index')->name('student.monumento.dashboard');
  Route::get('/my-profile', 'MonumentoDashboardController@profile')->name('monumento.student.profile');
  Route::get('/personal-information', 'MonumentoDashboardController@personalInfo')->name('monumento.student.personal-info');
  Route::get('/check-list', 'MonumentoDashboardController@checkList')->name('monumento.student.check-list');
  Route::get('/login', 'Auth\MonumentoLoginController@monumentoLoginForm')->name('login');
  Route::post('/login', 'Auth\MonumentoLoginController@login')->name('monumento.login.submit');
  Route::post('/logout', 'Auth\MonumentoLoginController@monumentoLogout')->name('monumento.student.logout');
  // Change Password
  Route::get('/change-password', 'MonumentoDashboardController@showChangePasswordForm')->name('monumento.changePasswordForm');;
  Route::post('/change-password','MonumentoDashboardController@changePassword')->name('monumento.changePassword');
});

Route::prefix('pasig')->group(function() {
  Route::get('/dashboard', 'PasigDashboardController@index')->name('student.pasig.dashboard');
  Route::get('/my-profile', 'PasigDashboardController@profile')->name('pasig.student.profile');
  Route::get('/personal-information', 'PasigDashboardController@personalInfo')->name('pasig.student.personal-info');
  Route::get('/check-list', 'PasigDashboardController@checkList')->name('pasig.student.check-list');
  Route::get('/login', 'Auth\PasigLoginController@pasigLoginForm')->name('login');
  Route::post('/login', 'Auth\PasigLoginController@login')->name('pasig.login.submit');
  Route::post('/logout', 'Auth\PasigLoginController@pasigLogout')->name('pasig.student.logout');
  // Change Password
  Route::get('/change-password', 'PasigDashboardController@showChangePasswordForm')->name('pasig.changePasswordForm');;
  Route::post('/change-password','PasigDashboardController@changePassword')->name('pasig.changePassword');
});

Route::prefix('pasig2')->group(function() {
  Route::get('/dashboard', 'Pasig2DashboardController@index')->name('student.pasig2.dashboard');
  Route::get('/my-profile', 'Pasig2DashboardController@profile')->name('pasig2.student.profile');
  Route::get('/personal-information', 'Pasig2DashboardController@personalInfo')->name('pasig2.student.personal-info');
  Route::get('/check-list', 'Pasig2DashboardController@checkList')->name('pasig2.student.check-list');
  Route::get('/login', 'Auth\Pasig2LoginController@pasig2LoginForm')->name('login');
  Route::post('/login', 'Auth\Pasig2LoginController@login')->name('pasig2.login.submit');
  Route::post('/logout', 'Auth\Pasig2LoginController@pasig2Logout')->name('pasig2.student.logout');
  // Change Password
  Route::get('/change-password', 'Pasig2DashboardController@showChangePasswordForm')->name('pasig2.changePasswordForm');;
  Route::post('/change-password','Pasig2DashboardController@changePassword')->name('pasig2.changePassword');
});

Route::prefix('recto')->group(function() {
  Route::get('/dashboard', 'RectoDashboardController@index')->name('student.recto.dashboard');
  Route::get('/my-profile', 'RectoDashboardController@profile')->name('recto.student.profile');
  Route::get('/personal-information', 'RectoDashboardController@personalInfo')->name('recto.student.personal-info');
  Route::get('/check-list', 'RectoDashboardController@checkList')->name('recto.student.check-list');
  Route::get('/login', 'Auth\RectoLoginController@rectoLoginForm')->name('login');
  Route::post('/login', 'Auth\RectoLoginController@login')->name('recto.login.submit');
  Route::post('/logout', 'Auth\RectoLoginController@rectoLogout')->name('recto.student.logout');
  // Change Password
  Route::get('/change-password', 'RectoDashboardController@showChangePasswordForm')->name('recto.changePasswordForm');;
  Route::post('/change-password','RectoDashboardController@changePassword')->name('recto.changePassword');
});

Route::prefix('villasis')->group(function() {
  Route::get('/dashboard', 'VillasisDashboardController@index')->name('student.villasis.dashboard');
  Route::get('/my-profile', 'VillasisDashboardController@profile')->name('villasis.student.profile');
  Route::get('/personal-information', 'VillasisDashboardController@personalInfo')->name('villasis.student.personal-info');
  Route::get('/check-list', 'VillasisDashboardController@checkList')->name('villasis.student.check-list');
  Route::get('/login', 'Auth\VillasisLoginController@villasisLoginForm')->name('login');
  Route::post('/login', 'Auth\VillasisLoginController@login')->name('villasis.login.submit');
  Route::post('/logout', 'Auth\VillasisLoginController@villasisLogout')->name('villasis.student.logout');
  // Change Password
  Route::get('/change-password', 'VillasisDashboardController@showChangePasswordForm')->name('villasis.changePasswordForm');;
  Route::post('/change-password','VillasisDashboardController@changePassword')->name('villasis.changePassword');
});

Route::prefix('zabarte')->group(function() {
  Route::get('/dashboard', 'ZabarteDashboardController@index')->name('student.zabarte.dashboard');
  Route::get('/my-profile', 'ZabarteDashboardController@profile')->name('zabarte.student.profile');
  Route::get('/personal-information', 'ZabarteDashboardController@personalInfo')->name('zabarte.student.personal-info');
  Route::get('/check-list', 'ZabarteDashboardController@checkList')->name('zabarte.student.check-list');
  Route::get('/login', 'Auth\ZabarteLoginController@zabarteLoginForm')->name('login');
  Route::post('/login', 'Auth\ZabarteLoginController@login')->name('zabarte.login.submit');
  Route::post('/logout', 'Auth\ZabarteLoginController@zabarteLogout')->name('zabarte.student.logout');
  // Change Password
  Route::get('/change-password', 'ZabarteDashboardController@showChangePasswordForm')->name('zabarte.changePasswordForm');;
  Route::post('/change-password','ZabarteDashboardController@changePassword')->name('zabarte.changePassword');
});
